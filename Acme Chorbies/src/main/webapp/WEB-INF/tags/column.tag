<%@tag language="java" body-content="empty" %>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<!-- Attributes -->

<%@ attribute name="code" required="false" %>
<%@ attribute name="property" required="false" %>
<%@ attribute name="sortable" required="false" %>
<%@ attribute name="value" required="false" %>
<%@ attribute name="url" required="false" %>
<%@ attribute name="url_code" required="false" %>

<!-- Body -->

<spring:message code="${code}" var="title" />
<display:column property="${property}" title="${title}" sortable="${sortable}"> 
	<jstl:choose>
		<jstl:when test="${url==null}">
			<jstl:out value="${value}" />
		</jstl:when>
		<jstl:when test="${url!=null}"> 
			<a href="${url}"><spring:message code="${url_code}"/></a>
		</jstl:when>
	</jstl:choose>
</display:column>