<%--
 * textbox.tag
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@ tag language="java" body-content="empty"%>

<%-- Taglibs --%>

<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%-- Attributes --%>

<%@ attribute name="coordinate" required="true" type="domain.Coordinate"%>

<%-- Definition --%>

<jstl:if test="${coordinate.country != null}">
	<p><spring:message code="coordinate.country" />:<jstl:out value="${coordinate.country}" /></p>
</jstl:if>
<jstl:if test="${coordinate.state != null}">
	<p><spring:message code="coordinate.state" />:<jstl:out value="${coordinate.state}" /></p>
</jstl:if>
<jstl:if test="${coordinate.province != null}">
	<p><spring:message code="coordinate.province" />:<jstl:out value="${coordinate.province}" /></p>
</jstl:if>
<p><spring:message code="coordinate.city" />:<jstl:out value="${coordinate.city}" /></p>