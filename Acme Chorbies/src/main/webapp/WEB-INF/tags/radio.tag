<%--
 * textbox.tag
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@ tag language="java" body-content="empty" %>

<%-- Taglibs --%>

<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%-- Attributes --%> 
 
<%@ attribute name="path" required="true" %>
<%@ attribute name="code" required="true" %>
<%@ attribute name="code1" required="true" %>
<%@ attribute name="code2" required="true" %>
<%@ attribute name="code3" required="false" %>
<%@ attribute name="value1" required="true" %>
<%@ attribute name="value2" required="true" %>
<%@ attribute name="value3" required="false" %>
<%@ attribute name="codenull" required="false" %>

<%-- Definition --%>

<div>
	<form:label path="${path}">
		<spring:message code="${code}" />:
	</form:label>
	<p>
	<form:radiobutton path="${path}" value="${value1}"/><spring:message code="${code1}" />
	<form:radiobutton path="${path}" value="${value2}"/><spring:message code="${code2}" />
	<jstl:if test="${value3!=null and code3!=null}">
		<form:radiobutton path="${path}" value="${value3}"/><spring:message code="${code3}" />	
	</jstl:if>
	<jstl:if test="${codenull != null}">
		<form:radiobutton path="${path}" value="${null}"/><spring:message code="${codenull}" />
	</jstl:if>
	<form:errors path="${path}" cssClass="error" />
	</p>
</div>	
