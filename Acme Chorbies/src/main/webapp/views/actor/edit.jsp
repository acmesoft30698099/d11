<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="actor/ua/edit.do" id="userAccount" modelAttribute="userAccount">
	
	<fieldset>
        <legend><spring:message code="userAccount.login_details" /></legend>
		<acme:textbox code="userAccount.username" path="username"/>
		<acme:password code="userAccount.password" path="password"/>
		<acme:password code="userAccount.confirmPassword" path="confirmPassword"/>
	</fieldset>
	
	<acme:save code="actor.save"/>
	<acme:delete confirm="actor.delete.confirm" code="actor.delete" id="${actorId}"/>
</form:form>

<acme:cancel url="actor/profile.do" code="actor.cancel"/>
	