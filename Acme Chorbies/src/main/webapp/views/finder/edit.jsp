<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form name="finder" action="finder/edit.do" id="finder"
	modelAttribute="finder">
	<form:hidden path="id" />

	<acme:radio codenull="finder.null" value3="activities"
		code3="finder.activities" value2="friendship"
		code2="finder.friendship" value1="love" code1="finder.love"
		code="finder.relation" path="relation" />
	<acme:textbox code="finder.age" path="age" />
	<acme:radio codenull="finder.null" value2="woman" code2="finder.woman"
		value1="man" code1="finder.man" code="finder.gender" path="gender" />

	<fieldset>
		<legend>
			<spring:message code="finder.coordinates" />
		</legend>
		<jstl:forEach items="${finder.coordinates}" var="coordinate" varStatus="status">
			<acme:textbox code="coordinate.country" path="coordinates[${status.index}].country" value="${coordinate.country}" />
			<acme:textbox code="coordinate.state" path="coordinates[${status.index}].state" value="${coordinate.state}" />
			<acme:textbox code="coordinate.province" path="coordinates[${status.index}].province" />
			<acme:textbox code="coordinate.city" path="coordinates[${status.index}].city" />
			
			<button type="submit" name="removeCoordinates"
				formaction="finder/removeCoordinates.do?coordinatesIndex=${status.index}"
				class="btn btn-primary">
				<spring:message code="coordinate.remove" />
			</button>

			<hr />
		</jstl:forEach>
		
		<button type="submit" name="addCoordinates"
			formaction="finder/addCoordinates.do" class="btn btn-primary">
			<spring:message code="coordinate.add" />
		</button>
	</fieldset>

	<acme:textbox code="finder.keyword" path="keyword" />

	<acme:save code="finder.save" />

</form:form>

<acme:cancel url="finder/profile.do" code="finder.cancel" />