<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="profileFinder">
	<acme:edit code="finder.edit" path="finder/edit.do?finderId=${finder.id}" edit="${edit}"/>
	
	<p> <spring:message code="finder.relation"/>: <jstl:choose><jstl:when test="${finder.relation =='activities'}"><spring:message code="finder.activities" /></jstl:when>
	<jstl:when test="${finder.relation =='friendship'}"><spring:message code="finder.friendship" /></jstl:when><jstl:when test="${finder.relation =='love'}">
	<spring:message code="finder.love" /></jstl:when><jstl:otherwise><spring:message code="finder.null" /></jstl:otherwise>
	</jstl:choose></p>
	<p><spring:message code="finder.age"/>: <jstl:choose><jstl:when test="${finder.age!=null}"><jstl:out value="${finder.age}" /></jstl:when>
	<jstl:otherwise><spring:message code="finder.null" /></jstl:otherwise></jstl:choose></p>
	<p> <spring:message code="finder.gender"/>: <jstl:choose><jstl:when test="${finder.gender =='man'}"><spring:message code="finder.man" /></jstl:when>
	<jstl:when test="${finder.gender =='woman'}"><spring:message code="finder.woman" /></jstl:when>
	<jstl:otherwise><spring:message code="finder.null" /></jstl:otherwise>
	</jstl:choose></p>
	<p><spring:message code="finder.keyword"/>: <jstl:choose><jstl:when test="${finder.keyword!=null}"><jstl:out value="${finder.keyword}" /></jstl:when>
	<jstl:otherwise><spring:message code="finder.null" /></jstl:otherwise></jstl:choose></p>
	<fieldset>
		<legend><spring:message code="finder.coordinates" /></legend>
		<jstl:forEach items="${finder.coordinates}" var="coordinate" >
			<jstl:if test="${coordinate.country!=null}"><p><spring:message code="coordinate.country"/>: <jstl:out value="${coordinate.country}" /></p></jstl:if>
			<jstl:if test="${coordinate.state!=null}"><p><spring:message code="coordinate.state"/>: <jstl:out value="${coordinate.state}" /></p></jstl:if>
			<jstl:if test="${coordinate.province!=null}"><p><spring:message code="coordinate.province"/>: <jstl:out value="${coordinate.province}" /></p></jstl:if>
			<p><spring:message code="coordinate.city"/>: <jstl:out value="${coordinate.city}" /></p>
		</jstl:forEach>
	</fieldset>
	
	<fieldset>
		<legend><spring:message code="finder.results" /></legend>
		<jstl:choose>
			<jstl:when test="${invalid!=true}">
				<display:table sort="list" requestURI="finder/profile.do" name="finderResults" id="finderResult" pagesize="5"  class="displayTag">
					<spring:message code="finder.results.image" var="imageTitle"/>
					<display:column title="${imageTitle}">
						<img src="${finderResult.image}" class="chorbiImage" />
					</display:column>
					
					<spring:message code="finder.results.name" var="nameTitle"/>
					<display:column property="name" title="${nameTitle}" sortable="true" />
			
					<spring:message code="finder.results.surname" var="surnameTitle"/>
					<display:column property="surname" title="${surnameTitle}" sortable="true" />
					
					<spring:message code="finder.results.relation" var="relationTitle"/>
					<display:column title="${relationTitle}" sortable="true">
						<jstl:choose>
							<jstl:when test="${finderResult.relation=='activities'}">
								<spring:message code="finder.activities" />
							</jstl:when>
							<jstl:when test="${finderResult.relation=='friendship'}">
								<spring:message code="finder.friendship" />
							</jstl:when>
							<jstl:otherwise>
								<spring:message code="finder.love" />
							</jstl:otherwise>
						</jstl:choose>
					</display:column>
					
					<spring:message code="finder.results.gender" var="genderTitle"/>
					<display:column title="${genderTitle}">
						<jstl:choose>
							<jstl:when test="${finderResult.gender=='man'}">
								<spring:message code="finder.man" />
							</jstl:when>
							<jstl:otherwise>
								<spring:message code="finder.woman" />
							</jstl:otherwise>
						</jstl:choose>
					</display:column>
					
					<spring:message code="finder.results.age" var="ageTitle"/>
					<display:column property="age" title="${ageTitle}" sortable="true" />

					<spring:message code="finder.results.profile.title" var="profileTitle" />
					<display:column title="${profileTitle}" >
						<a href="actor/profile.do?actorId=${finderResult.id}"><spring:message code="finder.results.profile" /></a>
					</display:column>
				</display:table>
			</jstl:when>
			<jstl:otherwise>
				<h1 class="error"><spring:message code="finder.invalidCreditCard" /></h1>
			</jstl:otherwise>
		</jstl:choose>
	</fieldset>
	
</div>
