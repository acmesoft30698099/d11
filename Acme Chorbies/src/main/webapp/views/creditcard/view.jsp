<%--
 * view.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:choose>

<jstl:when test="${creditCard != null}">

<fieldset>
	<legend><spring:message code="creditcard" /></legend>
	<span><spring:message code="creditcard.holder" /> : <jstl:out value="${creditCard.holder}" /></span>
	<br>
	<span><spring:message code="creditcard.brand" /> : <jstl:out value="${creditCard.brand}" /></span>
	<br>
	<span><spring:message code="creditcard.number" /> : <jstl:out value="${creditCard.number}" /></span>
	<br>
	<span><spring:message code="creditcard.expirationMonth" /> : <jstl:out value="${creditCard.expirationMonth}" /></span>
	<br>
	<span><spring:message code="creditcard.expirationYear" /> : <jstl:out value="${creditCard.expirationYear}" /></span>
	<br>
	<span><spring:message code="creditcard.cvv" /> : <jstl:out value="${creditCard.cvv}" /></span>
	<br>
	<jstl:choose>
		<jstl:when test="${creditCard.valid == true}">
			<spring:message code="creditcard.valid" />
		</jstl:when>
		<jstl:otherwise>
			<spring:message code="creditcard.notvalid" />
		</jstl:otherwise>
	</jstl:choose>
</fieldset>

<a href="creditcard/edit.do" ><spring:message code="creditcard.edit" /></a>
<br>
<security:authorize access="hasRole('CHORBI')">
	<a href="creditcard/delete.do" ><spring:message code="creditcard.delete" /></a>
</security:authorize>
</jstl:when>

<jstl:otherwise>
	
<p><spring:message code="creditcard.null" />

<a href="creditcard/create.do"><spring:message code="creditcard.create" /></a></p>

</jstl:otherwise>

</jstl:choose>