<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="forms.EventForm"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form id="form" modelAttribute="event" action="event/edit.do">
	<form:hidden path="id" />
	
	<acme:textbox code="event.title" path="title" />
	<acme:textbox code="event.description" path="description" />
	<acme:textbox code="event.image" path="image" />
	<acme:textbox code="event.seats" path="numberSeats" />
	<%
	
		EventForm obj_model = (EventForm) request.getAttribute("event");
		Date date = obj_model.getDate();
		if(date == null){
			date = new Date();
		}
		DateFormat date_formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		application.setAttribute("date", date_formatter.format(date));
	
	%>	
	
	<acme:textbox code="event.date" path="date" value="${date}" />
	
	<spring:message code="event.form.confirm" var="confirmMsg" />
	
	<acme:submit name="save" code="event.save" confirm="${id == 0}" confirmMsg="${confirmMsg}"/>
	
	<jstl:if test="${event.id > 0}">
		<acme:submit name="delete" code="event.delete" />
	</jstl:if>
</form:form>

<acme:cancel url="event/listOwns.do" code="event.cancel"/>