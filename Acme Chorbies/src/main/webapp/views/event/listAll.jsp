<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@page import="domain.Event" %>
<%@page import="java.util.Calendar"%>
<%@page import="controllers.utils.EventTableDecorator"%>

<display:table sort="list" requestURI="event/list.do" name="${events}"
	id="id" pagesize="5" class="displayTag" decorator="decorator">
	
	
	
	<spring:message code="event.title" var="titleTitle" />
	<display:column property="title" title="${titleTitle}" sortable="true" />
	
	<spring:message code="event.image" var="imageTitle" />
	<display:column title="${imageTitle}">
		<img src="<jstl:out value="${id.image}" />" class="chorbiImage" />
	</display:column>
	
	<spring:message code="event.date" var="dateTitle" />
	<display:column property="date" title="${dateTitle}" format="{0,date,dd/MM/yyyy HH:mm}" />
	
	<spring:message code="event.seats" var="seatsTitle" />
	<display:column property="numberSeats" title="${seatsTitle}" />
	
	<spring:message code="event.availableSeats" var="availableSeatsTitle" />
	<display:column property="availableSeats" title="${availableSeatsTitle}" sortable="true" />
	
	<display:column>
		<a href="event/profile.do?eventId=${id.id}"><spring:message code="event.profile" /></a>
	</display:column>
	
</display:table>