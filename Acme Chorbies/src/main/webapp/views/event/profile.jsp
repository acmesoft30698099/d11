<%--
 * profile.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<acme:edit code="event.edit" path="event/edit.do?eventId=${event.id}"
	edit="${canEdit}" />
	
<acme:edit code="event.bulk" path="chirp/bulk.do?eventId=${event.id}"
	edit="${canEdit}" />

<h3>
	<jstl:out value="${event.title}" />
</h3>
<img src="<jstl:out value="${event.image}" />"
	alt="<spring:message code="event.image.alt" />" class="chorbiImage" /><br/>
<spring:message code="event.description" />
<span><jstl:out value="${event.description}" /></span>
<security:authorize access="isAuthenticated()">
	<p><spring:message code="event.manager" />: <a href="actor/profile.do?actorId=${event.manager.id}"><jstl:out value="${event.manager.name}" /></a>
</security:authorize>

<p><spring:message code="event.date" />: <fmt:formatDate value="${event.date}" pattern="dd/MM/yyyy HH:mm" /></p>
<p><spring:message code="event.seats" />: <jstl:out value="${event.numberSeats}" /></p>
<p><spring:message code="event.availableSeats" />: <jstl:out value="${event.availableSeats}" /></p>

<jstl:if test="${canRegister or canUnRegister}" >
	<jstl:if test="${event.availableSeats > 0}">
		<p><spring:message code="event.hasAvailableSeats" /></p>
	</jstl:if>
	
	<acme:gotoConfirm access="${canRegister}" code="event.register" url="event/register.do?eventId=${event.id}" text="event.register.confirm" />
	<acme:edit code="event.unregister" path="event/unregister.do?eventId=${event.id}" edit="${canUnRegister}"/>
</jstl:if>
