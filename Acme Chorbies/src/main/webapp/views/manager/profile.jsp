<%--
 * profile.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<fieldset>
	<legend><spring:message code="manager.profile" /></legend>
	<acme:edit code="manager.edit" path="actor/profile/edit.do" edit="${edit}" />
	<acme:edit code="manager.edit.ua" path="actor/ua/edit.do" edit="${edit}" />
	
	<p><spring:message code="manager.name" />:<jstl:out value="${manager.name}" /></p>
	<p><spring:message code="manager.surname" />:<jstl:out value="${manager.surname}" /></p>
	<p><spring:message code="manager.email" />:<jstl:out value="${manager.email}" /></p>
	<p><spring:message code="manager.phone" />:<jstl:out value="${manager.phone}" /></p>
	<p><spring:message code="manager.companyName" />:<jstl:out value="${manager.companyName}" /></p>
	<p><spring:message code="manager.VATNumber" />:<jstl:out value="${manager.VATNumber}" /></p>
	
	<jstl:if test="${edit}">
		<p><a href="event/listOwns.do"><spring:message code="manager.events" /></a></p>
		
		<p><spring:message code="manager.fee" />: <jstl:out value="${manager.fee}" /></p>
	</jstl:if>
	
	<security:authorize access="hasRole('ADMIN')" >
		<p><spring:message code="manager.fee" />: <jstl:out value="${manager.fee}" /></p>
	</security:authorize>
	
</fieldset>

