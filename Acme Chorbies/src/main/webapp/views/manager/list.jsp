<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table sort="list" requestURI="_manager/list.do" name="${managers}"
	id="id" pagesize="5" class="displayTag">

	<spring:message code="manager.name" var="nameTitle" />
	<display:column property="name" title="${nameTitle}" sortable="true" />

	<spring:message code="manager.surname" var="surnameTitle" />
	<display:column property="surname" title="${surnameTitle}"
		sortable="true" />
		
	<spring:message code="manager.companyName" var="companyNameTitle" />
	<display:column property="companyName" title="${companyNameTitle}" sortable="true" />

	<display:column>
		<a href="actor/profile.do?actorId=${id.id}"><spring:message
				code="manager.profile" /></a>
	</display:column>
</display:table>