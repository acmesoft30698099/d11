<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="forms.ActorForm"%>
<%@page import="forms.ChorbiForm"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form modelAttribute="manager" action="_manager/edit.do">
	<form:hidden path="id" />
	
	<jstl:if test="${manager.id<=0}">
		<fieldset>
	        <legend><spring:message code="userAccount.login_details" /></legend>
	        <acme:textbox code="userAccount.username" path="userAccount.username"/>
			<acme:password code="userAccount.password" path="userAccount.password"/>
			<acme:password code="userAccount.confirmPassword" path="userAccount.confirmPassword"/>
	    </fieldset>
	</jstl:if>
	
	<acme:textbox code="manager.name" path="name" />
	<acme:textbox code="manager.surname" path="surname" />
	<acme:textbox code="manager.email" path="email" />
	<acme:textbox code="manager.phone" path="phone" />
	
	<acme:textbox code="manager.companyName" path="companyName" />
	<acme:textbox code="manager.VATNumber" path="VATNumber" />
	
	<jstl:choose>
		<jstl:when test="${manager.id == 0}">
			<fieldset>
				<legend><spring:message code="creditCard.login_details" /></legend>
			    <form:hidden path="creditCard.id" />
			    <acme:textbox path="creditCard.holder" code="creditcard.holder" />
				<acme:textbox path="creditCard.brand" code="creditcard.brand" />
				<acme:textbox path="creditCard.number" code="creditcard.number" />
				<acme:textbox path="creditCard.expirationMonth" code="creditcard.expirationMonth" />
				<acme:textbox path="creditCard.expirationYear" code="creditcard.expirationYear" />
				<acme:textbox path="creditCard.cvv" code="creditcard.cvv" />
			</fieldset>
		</jstl:when>
		<jstl:otherwise>
			<form:hidden path="creditCard.id" />
		</jstl:otherwise>
	</jstl:choose>
	<jstl:if test="${manager.id == 0}">
		<acme:termAndconditions /><br/>
	</jstl:if>
	
	<acme:submit name="save" code="manager.save" />
</form:form>

<jstl:if test="${manager.id > 0}">
	<acme:cancel url="_manager/profile.do" code="manager.cancel" />
</jstl:if>
