<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="profileAdministrador">
	<acme:edit code="actor.edit" path="actor/profile/edit.do" edit="${edit}"/>
	<acme:edit code="actor.edit_ua" path="actor/ua/edit.do" edit="${edit}"/>
	
	<fieldset>
		<legend><spring:message code="actor.profile.info" /></legend>
		<p> <spring:message code="actor.name"/>: <jstl:out value="${administrator.name}" /></p>
		<p> <spring:message code="actor.surname"/>: <jstl:out value="${administrator.surname}" /></p>
		<p> <spring:message code="actor.email"/>: <jstl:out value="${administrator.email}" /></p>
		<p> <spring:message code="actor.phone"/>: <jstl:out value="${administrator.phone}" /></p>
	</fieldset>

</div>