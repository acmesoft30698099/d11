<%--
 * index.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div class="members">
	<fieldset>
		<h3><spring:message code="contact.member_1" /></h3>
		<p><spring:message code="contact.name"/>: Ignacio Jos� Al�s L�pez. </p>
		<p><spring:message code="contact.email"/>: ignalelop@alum.us.es </p>
		<p><spring:message code="contact.id" />: 30698099E </p>
		<h3><spring:message code="contact.member_2" /></h3>
		<p><spring:message code="contact.name"/>: Javier Rom�n Bayar. </p>
		<p><spring:message code="contact.email"/>: javrombay@alum.us.es </p>
		<p><spring:message code="contact.id" />: 29504834C </p>
		<h3><spring:message code="contact.member_3" /></h3>
		<p><spring:message code="contact.name"/>: Juan Manuel Lineros Fern�ndez. </p>
		<p><spring:message code="contact.email"/>: jualinfer1@alum.us.es </p>
		<p><spring:message code="contact.id" />: 77814569A </p>
		<h3><spring:message code="contact.member_4" /></h3>
		<p><spring:message code="contact.name"/>: Pablo Pino Jim�nez. </p>
		<p><spring:message code="contact.email"/>: pabpinjim@alum.us.es </p>
		<p><spring:message code="contact.id" />: 29500962N </p>
	</fieldset>
</div>