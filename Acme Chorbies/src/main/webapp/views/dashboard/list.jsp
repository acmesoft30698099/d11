<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="divDashboard">

	<p><spring:message code="dashboard.number.chorbies.country" /></p>
	<table>
		<jstl:forEach items="${getNumberOfChorbiesByTheirCountryAndCity}" var="content">
			<tr>
				<td>
					<jstl:choose>
						<jstl:when test="${content.key==null}">
							<spring:message code="dashboard.nothing" />
						</jstl:when>
						<jstl:otherwise>
							<jstl:out value="${content.key}" />
						</jstl:otherwise>
					</jstl:choose>
				</td>
				<td>
					<jstl:out value="${content.value}" />
				</td>
		</jstl:forEach>
	</table>
	
	<p><spring:message code="dashboard.chorbies.ordered.number.received.likes" /></p>
	<ul>
		<jstl:forEach items="${getChorbiesOrderedByNumberOfReceivedLikes}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a><jstl:out value=" -> ${fn:length(attribute.receivedLikes)}" /></li>
		</jstl:forEach>
	</ul>
	
	<p><spring:message code="dashboard.chorbi.received.chirps" /></p>
	<ul>
		<jstl:forEach items="${getChorbiesWithTheMostReceivedChirps}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a></li>
		</jstl:forEach>
	</ul>
	
	<p><spring:message code="dashboard.chorbi.sent.chirps" /></p>
	<ul>
		<jstl:forEach items="${getChorbiesWithTheMostSentChirps}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a></li>
		</jstl:forEach>
	</ul>
	
	
	<p><spring:message code="dashboard.manager.events" /></p>
	<ul>
		<jstl:forEach items="${getManagersOrderedByNumberOfEvents}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a><jstl:out value=" -> ${fn:length(attribute.events)}" /></li>
		</jstl:forEach>
	</ul>
	
	<p><spring:message code="dashboard.manager.fee" /></p>
	<ul>
		<jstl:forEach items="${getAllManager}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a><jstl:out value=" -> ${attribute.fee} euros" /></li>
		</jstl:forEach>
	</ul>
	
	<p><spring:message code="dashboard.chorbi.events" /></p>
	<ul>
		<jstl:forEach items="${getChorbiesOrderedByNumberOfEvents}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a><jstl:out value=" -> ${fn:length(attribute.events)}" /></li>
		</jstl:forEach>
	</ul>
	
	<p><spring:message code="dashboard.chorbi.fee" /></p>
	<ul>
		<jstl:forEach items="${getAllChorbies}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a><jstl:out value=" -> ${attribute.fee} euros" /></li>
		</jstl:forEach>
	</ul>
	
	<p><spring:message code="dashboard.chorbi.stars" /></p>
	<ul>
		<jstl:forEach items="${getChorbiesOrderedByAverageLikes}" var="attribute">
			<li><a href="actor/profile.do?actorId=${attribute.id}"><jstl:out value="${attribute.name} ${attribute.surname}" /></a></li>
		</jstl:forEach>
	</ul>
	
	<p><spring:message code="dashboard.max.age.chorbies" />: <jstl:out value="${getMaxAgeOfChorbies}" /></p>
	<p><spring:message code="dashboard.min.age.chorbies" />: <jstl:out value="${getMinAgeOfChorbies}" /></p>
	<p><spring:message code="dashboard.average.age.chorbies" />: <jstl:out value="${getAvgAgeOfChorbies}" /></p>
	<p><spring:message code="dashboard.chorbies.invalid.creditcard" />: <jstl:out value="${getAmountOfChorbieswithInvalidOrNullCreditCard}" /></p>
	<p><spring:message code="dashboard.chorbies.searching.activities" />: <jstl:out value="${getAmountOfChorbiesSearchingForActivities}" /></p>
	<p><spring:message code="dashboard.chorbies.searching.friendship" />: <jstl:out value="${getAmountOfChorbiesSearchingForFriendship}" /></p>
	<p><spring:message code="dashboard.chorbies.searching.love" />: <jstl:out value="${getAmountOfChorbiesSearchingForLove}" /></p>
	<p><spring:message code="dashboard.min.likes" />: <jstl:out value="${getMinNumberOfLikesPerChorbi}" /></p>
	<p><spring:message code="dashboard.max.likes" />: <jstl:out value="${getMaxNumberOfLikesPerChorbi}" /></p>
	<p><spring:message code="dashboard.average.likes" />: <jstl:out value="${getAvgNumberOfLikesPerChorbi}" /></p>
	<p><spring:message code="dashboard.min.received.chirps" />: <jstl:out value="${getMinNumberOfReceivedChirpsPerChorbi}" /></p>
	<p><spring:message code="dashboard.max.received.chirps" />: <jstl:out value="${getMaxNumberOfReceivedChirpsPerChorbi}" /></p>
	<p><spring:message code="dashboard.average.received.chirps" />: <jstl:out value="${getAvgNumberOfReceivedChirpsPerChorbi}" /></p>
	<p><spring:message code="dashboard.min.sent.chirps" />: <jstl:out value="${getMinNumberOfSentChirpsPerChorbi}" /></p>
	<p><spring:message code="dashboard.max.sent.chirps" />: <jstl:out value="${getMaxNumberOfSentChirpsPerChorbi}" /></p>
	<p><spring:message code="dashboard.average.sent.chirps" />: <jstl:out value="${getAvgNumberOfSentChirpsPerChorbi}" /></p>
	
	<p><spring:message code="dashboard.max.stars" />: <jstl:out value="${getMaxNumberOfStars}" /></p>
	<p><spring:message code="dashboard.min.stars" />: <jstl:out value="${getMinNumberOfStars}" /></p>
	<p><spring:message code="dashboard.average.stars" />: <jstl:out value="${getAvgNumberOfStars}" /></p>
	
</div>
