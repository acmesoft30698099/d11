<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Sample Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<li><a class="fNiv"><spring:message code="master.page.contact" /></a>
			<ul>
				<li class="arrow" />
				<li><a href="contact/show.do"><spring:message code="master.page.contact.show" /></a></li>
				<li><a href="mailto:ignalelop@alum.us.es"><spring:message code="master.page.contact.send" /></a></li>
			</ul>
		</li>
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="!hasRole('BANNED_CHORBI')">
			<li><a class="fNiv"><spring:message code="master.page.events" /></a>
				<ul>
					<li class="arrow" />
					<li><a href="event/list.do"><spring:message code="master.page.events.all" /></a></li>
					<li><a href="event/listAvailable.do" ><spring:message code="master.page.events.available" /></a></li>
				</ul>
			</li>
		</security:authorize>
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="dashboard/list.do"><spring:message code="master.page.administrator.dashboard" /></a></li>
					<li><a href="configuration/profile.do"><spring:message code="master.page.administrator.configuration" /></a></li>
					<li><a href="chorbi/list.do"><spring:message code="master.page.chorbi.list" /></a></li>
					<li><a href="configuration/showIncome.do"><spring:message code="master.page.showIncome" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('MANAGER')">
			<li><a class="fNiv"><spring:message	code="master.page.manager" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="event/listOwns.do" ><spring:message code="master.page.manager.list.owns" /></a></li>
					<li><a href="event/create.do" ><spring:message code="master.page.manager.create.event" /></a></li>
					<li><a href="_manager/list.do"><spring:message code="master.page.manager.list" /></a></li>
					<li><a href="creditcard/view.do"><spring:message code="master.page.chorbi.creditcard" /></a></li>		
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('CHORBI')">
			<li><a class="fNiv"><spring:message	code="master.page.chorbi" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="finder/profile.do"><spring:message code="master.page.chorbi.finder" /></a></li>
					<li><a href="chirp/listReceived.do"><spring:message code="master.page.chorbi.receivedChirps" /></a></li>	
					<li><a href="chirp/listSent.do"><spring:message code="master.page.chorbi.sentChirps" /></a></li>		
					<li><a href="chorbi/list.do"><spring:message code="master.page.chorbi.list" /></a></li>
					<li><a href="event/listRegistered.do"><spring:message code="master.page.chorbi.list.registered" /></a></li>
					<li><a href="creditcard/view.do"><spring:message code="master.page.chorbi.creditcard" /></a></li>
					<li><a href="chorbi/likedList.do"><spring:message code="master.page.chorbi.list.liked" /></a></li>		
				</ul>
			</li>
		</security:authorize>
			
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a>
				<ul>
					<li class="arrow"/>
					<li><a href="chorbi/create.do"><spring:message code="master.page.chorbi.create" /></a></li>
					<li><a href="_manager/create.do"><spring:message code="master.page.manager.create" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasAnyRole('ADMIN', 'CHORBI', 'MANAGER')">
						<li><a href="actor/profile.do"><spring:message code="master.page.actor.profile" /></a></li>
					</security:authorize>		
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

