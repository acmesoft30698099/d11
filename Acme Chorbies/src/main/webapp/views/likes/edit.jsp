<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="likes/edit.do" modelAttribute="likes"
	method="post" name="likes" id="likes" >
	<form:hidden path="receiver" />

	<acme:textbox path="comments" code="likes.comments" />
	<acme:textbox path="stars" code="likes.stars" />

	<input type="submit" name="save" value="<spring:message code="likes.save" />" />
	
</form:form>

<acme:cancel url="${returnURL}" code="likes.cancel" />