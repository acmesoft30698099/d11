<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table sort="list" name="chorbies" id="chorbi" requestURI="configuration/showIncome.do" pagesize="5">
	
	<spring:message code="configuration.chorbi" var="chorbiTitle" />
	<display:column title="${chorbiTitle}"><jstl:out value="${chorbi.name}" /> <jstl:out value="${chorbi.surname}" /></display:column>
	<spring:message code="configuration.chorbiFee" var="feeTitle" />
	<display:column title="${feeTitle}" property="fee" />
	
</display:table>

<spring:message code="configuration.income" /> <jstl:out value="${monthlyIncome}" />