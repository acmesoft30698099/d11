<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form name="configuration" action="configuration/edit.do" id="configuration"
	modelAttribute="configuration">
	
	<fieldset>
		<legend><spring:message code="configuration.banners" /></legend>
		<jstl:forEach items="${configuration.banners}" var="banner" varStatus="index">
			<p>
			<acme:textbox code="configuration.url" path="banners[${index.index}].url" />
			<button type="submit" name="removeBanner" formaction="configuration/removeBanner.do?bannerIndex=${index.index}"><spring:message code="configuration.remove.banner" /></button></p> 
		</jstl:forEach>
		<p><button type="submit" name="addBanner" formaction="configuration/addBanner.do"><spring:message code="configuration.add.banner" /></button></p>
	</fieldset>
	
	<acme:textbox code="configuration.timer" path="cacheTimer" />
	<acme:textbox code="configuration.managerFee" path="managerFee" />
	<acme:textbox code="configuration.chorbiFee" path="chorbiFee" />
	
	<acme:submit name="save" code="configuration.save" /> 
</form:form>

<acme:cancel url="configuration/profile.do" code="configuration.cancel" />
