<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="profileConfiguration">
	<fieldset>
		<legend><spring:message code="configuration.title" /></legend>
		<acme:edit code="configuration.edit" path="configuration/edit.do" edit="${true}" />
		<display:table sort="list" name="configuration.banners" id="banner" requestURI="${requestURI}" pagesize="5">
			<spring:message code="configuration.banners" var="bannerTitle" />
			<display:column title="${bannerTitle}">
				<a href=<jstl:out value="${banner.url}"/> >
					<img class="banner" src=<jstl:out value="${banner.url}" /> />
				</a>
			</display:column>
			<spring:message code="configuration.url" var="urlTitle" />
			<display:column title="${urlTitle}">
				<a href=<jstl:out value="${banner.url}"/> ><jstl:out value="${banner.url}"/></a>
			</display:column>
		</display:table>
		<p><spring:message code="configuration.timer" />: <fmt:formatDate value="${configuration.cacheTimer}" pattern="HH:mm:ss"/></p>
		<p><spring:message code="configuration.managerFee" />: <jstl:out value="${configuration.managerFee}" /></p>
		<p><spring:message code="configuration.chorbiFee" />: <jstl:out value="${configuration.chorbiFee}" /></p>
	</fieldset>
</div>
