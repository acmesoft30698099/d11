<%--
 * profile.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<fieldset>
	<legend><spring:message code="chorbi.profile" /></legend>
	<acme:edit code="chorbi.edit" path="actor/profile/edit.do" edit="${edit}" />
	<acme:edit code="chorbi.edit.ua" path="actor/ua/edit.do" edit="${edit}" />
	
	<jstl:choose>
		<jstl:when test="${not isBanned}">
			<security:authorize access="hasRole('ADMIN')">
				<acme:edit code="chorbi.ban" path="chorbi/banChorbi.do?chorbiId=${chorbi.id}" edit="${true}" />	
			</security:authorize>
		</jstl:when>
		<jstl:otherwise>
			<security:authorize access="hasRole('ADMIN')">
				<acme:edit code="chorbi.unban" path="chorbi/unbanChorbi.do?chorbiId=${chorbi.id}" edit="${true}" />
			</security:authorize>
			<h2 class="error"><spring:message code="chorbi.banned" /></h2>
		</jstl:otherwise>
	</jstl:choose>
	
	<jstl:if test="${ban_message}">
		<p class="error"><spring:message code="commit.error" /></p>
	</jstl:if>
	
	<img src="<jstl:out value="${chorbi.image}" />" class="chorbiImage"/>
	<p><spring:message code="chorbi.name" />:<jstl:out value="${chorbi.name}" /></p>
	<p><spring:message code="chorbi.surname" />:<jstl:out value="${chorbi.surname}" /></p>
	<p><spring:message code="chorbi.email" />:<jstl:out value="${chorbi.email}" /></p>
	<p><spring:message code="chorbi.phone" />:<jstl:out value="${chorbi.phone}" /></p>
	<span><spring:message code="chorbi.description" />:<jstl:out value="${description}" /></span>
	<p><spring:message code="chorbi.gender"/>:
		<acme:show_gender chorbi="${chorbi}"/>
	</p>
	<p><spring:message code="chorbi.birthDate"/>:<fmt:formatDate value="${chorbi.birthDate}" pattern="dd/MM/yyyy"/> (<spring:message code="chorbi.age"/> <jstl:out value="${chorbi.age}" />)</p>

	<p><spring:message code="chorbi.relation" />:
		<acme:show_relation chorbi="${chorbi}"/>	
	</p>
	<fieldset>
		<legend><spring:message code="chorbi.coordinate" /></legend>
		<acme:show_coordinate coordinate="${chorbi.coordinate}"/>
	</fieldset>
	
	<fieldset>
		<legend><spring:message code="chorbi.likes" /></legend>
		<jstl:forEach items="${chorbi.receivedLikes}" var="like" varStatus="status">
			<jstl:if test="${status.index > 0}">
				<hr/>
			</jstl:if>
			<p><spring:message code="likes.sentDate" />: <fmt:formatDate value="${like.sentDate}" pattern="dd/MM/yyyy HH:mm"/></p>
			<p><spring:message code="likes.sender" />: <a href="chorbi/profile.do?chorbiId=${like.sender.id}"><jstl:out value="${like.sender.name}" /></a></p>
			<jstl:if test="${like.comments != null}" >
				<p><spring:message code="likes.comments" />: <jstl:out value="${like.comments}" /></p>
			</jstl:if>
			<p><spring:message code="likes.stars" />: <jstl:out value="${like.stars}" /></p>
			<jstl:if test="${like.sender == registeredUser}">
				<acme:goto url="likes/remove.do?likesId=${like.id}" code="likes.remove" /> 
			</jstl:if>
		</jstl:forEach>
		<security:authorize access="hasRole('CHORBI')">
			<jstl:if test="${canLike == true}">
				<acme:edit code="chorbi.send.like" path="likes/create.do?chorbiId=${chorbi.id}" edit="${not edit}" />
				<jstl:if test="${likeRemoveError}">
					<p class="error"><spring:message code="commit.error" /></p>
				</jstl:if>
			</jstl:if>
		</security:authorize>
	</fieldset>
</fieldset>

