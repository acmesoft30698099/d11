<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table sort="list" requestURI="chorbi/list.do" name="${chobies}"
	id="id" pagesize="5" class="displayTag">
	
	<spring:message code="chorbi.image" var="imageTitle" />
	<display:column title="${imageTitle}">
		<img src="${id.image}" class="chorbiImage" />
	</display:column>

	<spring:message code="chorbi.name" var="nameTitle" />
	<display:column property="name" title="${nameTitle}" sortable="true" />

	<spring:message code="chorbi.surname" var="surnameTitle" />
	<display:column property="surname" title="${surnameTitle}"
		sortable="true" />

	<spring:message code="chorbi.relation" var="relationTitle" />
	<display:column title="${relationTitle}" sortable="true">
		<jstl:choose>
			<jstl:when test="${id.relation=='activities'}">
				<spring:message code="chorbi.relation.activities" />
			</jstl:when>
			<jstl:when test="${id.relation=='friendship'}">
				<spring:message code="chorbi.relation.friendship" />
			</jstl:when>
			<jstl:otherwise>
				<spring:message code="chorbi.relation.love" />
			</jstl:otherwise>
		</jstl:choose>
	</display:column>

	<spring:message code="chorbi.gender" var="genderTitle" />
	<display:column title="${genderTitle}">
		<acme:show_gender chorbi="${id}"/>
	</display:column>

	<spring:message code="chorbi.age" var="ageTitle" />
	<display:column property="age" title="${ageTitle}" sortable="true" />

	<display:column>
		<a href="actor/profile.do?actorId=${id.id}"><spring:message
				code="chorbi.profile" /></a>
	</display:column>
</display:table>
