<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="forms.ActorForm"%>
<%@page import="forms.ChorbiForm"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form modelAttribute="chorbi" action="chorbi/edit.do">
	<form:hidden path="id" />
	
	<jstl:if test="${chorbi.id<=0}">
		<fieldset>
	        <legend><spring:message code="userAccount.login_details" /></legend>
	        <acme:textbox code="userAccount.username" path="userAccount.username"/>
			<acme:password code="userAccount.password" path="userAccount.password"/>
			<acme:password code="userAccount.confirmPassword" path="userAccount.confirmPassword"/>
	    </fieldset>
	</jstl:if>
	
	<acme:textbox code="chorbi.name" path="name" />
	<acme:textbox code="chorbi.surname" path="surname" />
	<acme:textbox code="chorbi.email" path="email" />
	<acme:textbox code="chorbi.phone" path="phone" />
	
	<acme:textbox code="chorbi.image" path="image" />
	<acme:textbox code="chorbi.description" path="description" />
	<acme:radio value1="love" code1="chorbi.relation.love" value2="friendship" code2="chorbi.relation.friendship"
		value3="activities" code3="chorbi.relation.activities" code="chorbi.relation" path="relation" />
	<%
	
	ChorbiForm obj_model = (ChorbiForm) request.getAttribute("chorbi");
	Date date = obj_model.getBirthDate();
	if(date == null){
		date = new Date();
	}
	DateFormat date_formatter = new SimpleDateFormat("dd/MM/yyyy");
	application.setAttribute("date", date_formatter.format(date));
	
	%>	
	<jstl:choose>
		<jstl:when test="${chorbi.id == 0}">
			<acme:textbox code="chorbi.birthDate" path="birthDate" value="${date}" />
			<acme:radio value1="man" code1="chorbi.man" value2="woman" code2="chorbi.woman"
			 	code="chorbi.gender" path="gender" />
		</jstl:when>
		<jstl:otherwise>
			<form:hidden path="birthDate" value="${date}" />
			<form:hidden path="gender" />
		</jstl:otherwise>
	</jstl:choose>
	
	<acme:textbox code="coordinate.country" path="coordinate.country" />
	<acme:textbox code="coordinate.state" path="coordinate.state" /> 
	<acme:textbox code="coordinate.province" path="coordinate.province" />
	<acme:textbox code="coordinate.city" path="coordinate.city" />
	
	<jstl:if test="${chorbi.id == 0}">
		<acme:termAndconditions /><br/>
	</jstl:if>
	
	<acme:submit name="save" code="chorbi.save" />
</form:form>

<jstl:if test="${chorbi.id > 0}">
	<acme:cancel url="chorbi/profile.do" code="chorbi.cancel" />
</jstl:if>
