<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="domain.Manager"%>
<%@page import="domain.Chirp"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${sent}">
	<p>
		<acme:goto code="chirp.create" url="chirp/create.do" />
	</p>
</jstl:if>
<display:table sort="list" requestURI="${requestUri}" name="chirps"
	id="chirp" pagesize="5">

	<spring:message code="chirp.subject" var="subjectTitle" />
	<display:column property="subject" title="${subjectTitle}" />

	<spring:message code="chirp.text" var="textTitle" />
	<display:column property="text" title="${textTitle}" />

	<spring:message code="chirp.sentDate" var="sentDateTitle" />
	<display:column property="sentDate" title="${sentDateTitle}"
		format="{0,date,dd/MM/yyyy HH:mm}" />

	<jstl:if test="${sent}">
		<spring:message code="chirp.receiver" var="receiverTitle" />
		<display:column title="${receiverTitle}">
			<a href="actor/profile.do?actorId=${chirp.receiver.id}"><jstl:out
					value="${chirp.receiver.name}" /></a>
		</display:column>
	</jstl:if>
	
	<%  
		if(chirp != null) {
			request.setAttribute("noReply",!(((Chirp) chirp).getSender() instanceof Manager));
		}
	%>

	<jstl:if test="${received}">
		<spring:message code="chirp.sender" var="senderTitle" />
		<display:column title="${senderTitle}">
			<a href="actor/profile.do?actorId=${chirp.sender.id}"><jstl:out
					value="${chirp.sender.name}" /></a>
		</display:column>
		
			<display:column>
				<jstl:if test="${noReply}">
					<acme:goto url="chirp/reply.do?chirpId=${chirp.id}"
						code="chirp.reply" />
				</jstl:if>
			</display:column>
			
	</jstl:if>

		<display:column>
			<jstl:if test="${noReply}">	
				<acme:goto url="chirp/forward.do?chirpId=${chirp.id}"
					code="chirp.forward" />
			</jstl:if>
		</display:column>
	
	<display:column>
		<acme:goto url="chirp/profile.do?chirpId=${chirp.id}"
			code="chirp.profile" />
	</display:column>

</display:table>
