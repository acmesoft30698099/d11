<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:choose>
	<jstl:when test="${not forward}">
		<form:form modelAttribute="chirpForm" method="post" action="chirp/edit.do">
			<jstl:choose>
				<jstl:when test="${not reply}">
					<acme:textbox code="chirp.subject" path="subject" />
					<acme:textarea code="chirp.text" path="text"/>
					<jstl:if test="${bulk}">
						<form:hidden path="eventId" />
					</jstl:if>
				</jstl:when>
				<jstl:otherwise>
					<form:hidden path="subject" />
					<form:hidden path="receiver" />
					<form:hidden path="text" />
					<p><spring:message code="chirp.subject" />: <jstl:out value="${chirpForm.subject}" /></p>
					<p><spring:message code="chirp.text" />:${'>'}<jstl:out value="${chirpForm.text}" />${'<'}</p>
					<p><spring:message code="chirp.text" />
					<textarea name="texto" id="texto" ><jstl:out value="${texto}" /></textarea></p>
				</jstl:otherwise>
			</jstl:choose>

			<fieldset>
				<legend>
					<spring:message code="chirp.attachments" />
				</legend>
				<jstl:forEach items="${chirpForm.attachments}" var="attachment" varStatus="status">
					<acme:textbox code="chirp.attachment" path="attachments[${status.index}].url" />
						
					<jstl:choose>
						<jstl:when test="${reply}">
							<button type="submit" name="removeAttachment"
								formaction="chirp/removeAttachmentReply.do?attachmentIndex=${status.index}"
								class="btn btn-primary">
								<spring:message code="attachment.remove" />
							</button>
						</jstl:when>
						<jstl:when test="${bulk}">
							<button type="submit" name="removeAttachment"
								formaction="chirp/removeAttachmentBulk.do?attachmentIndex=${status.index}"
								class="btn btn-primary">
								<spring:message code="attachment.remove" />
							</button>
						</jstl:when>
						<jstl:otherwise>
							<button type="submit" name="removeAttachment"
								formaction="chirp/removeAttachmentStandard.do?attachmentIndex=${status.index}"
								class="btn btn-primary">
								<spring:message code="attachment.remove" />
							</button>
						</jstl:otherwise>
					</jstl:choose>
					
					<hr />
				</jstl:forEach>
				<jstl:choose>
					<jstl:when test="${reply}">
						<button type="submit" name="addAttachment"
							formaction="chirp/addAttachmentReply.do"
							class="btn btn-primary">
							<spring:message code="attachment.add" />
						</button>
					</jstl:when>
					<jstl:when test="${bulk}">
						<button type="submit" name="addAttachment"
							formaction="chirp/addAttachmentBulk.do"
							class="btn btn-primary">
							<spring:message code="attachment.add" />
						</button>
					</jstl:when>
					<jstl:otherwise>
						<button type="submit" name="addAttachment"
							formaction="chirp/addAttachmentStandard.do"
							class="btn btn-primary">
							<spring:message code="attachment.add" />
						</button>
					</jstl:otherwise>
				</jstl:choose>
			</fieldset>
			
			<jstl:choose>
				<jstl:when test="${reply}">
					<br/><acme:submit name="saveReply" code="chirp.save" />
				</jstl:when>
				<jstl:when test="${bulk}">
					<br/><acme:submit name="saveBulk" code="chirp.save" />
				</jstl:when>
				<jstl:otherwise>
					<form:select path="receiver">
						<form:options items="${actors}" itemLabel="name" itemValue="id"/>
					</form:select>
				
					<br/><acme:submit name="saveNormal" code="chirp.save" />
				</jstl:otherwise>
			</jstl:choose>
		</form:form>
	</jstl:when>
	<jstl:otherwise>
		<form action="chirp/edit.do" method="post"> 
			<input name="chirpId" id="chirpId" type="hidden" value="${_chirp.id}" />
		
			<p><spring:message code="chirp.subject" />: <jstl:out value="${_chirp.subject}" /></p>
			<p><spring:message code="chirp.text" />: <jstl:out value="${_chirp.text}" /></p>
			
			<fieldset>
				<legend><spring:message code="chirp.attachments" /></legend>
				<jstl:forEach items="${_chirp.attachments}" var="attachment">
					<p><jstl:out value="${attachment.url}" /></p>
				</jstl:forEach>
			</fieldset>
			<select name="actorId" id="actorId" >
				<jstl:forEach items="${actors}" var="actor">
					<jstl:choose>
						<jstl:when test="${actorId == actor.id}">
							<option value="${actor.id}" selected="selected"><jstl:out value="${actor.name}" /></option>
						</jstl:when>
						<jstl:otherwise>
							<option value="${actor.id}"><jstl:out value="${actor.name}" /></option>
						</jstl:otherwise>
					</jstl:choose>
				</jstl:forEach>
			</select>
			
			<br/><acme:submit name="saveForward" code="chirp.save" />
		</form>
	</jstl:otherwise>
</jstl:choose>

<acme:cancel code="chirp.cancel" url="chirp/listSent.do" />