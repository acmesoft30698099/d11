<%--
 * profile.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="domain.Manager"%>
<%@page import="domain.Chirp"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<h3><jstl:out value="${_chirp.subject}" /></h3>

<p><spring:message code="chirp.sender" />: 
	<a href="actor/profile.do?actorId=${_chirp.sender.id}" >
		<jstl:out value="${_chirp.sender.name}" />
	</a>
</p>

<p><spring:message code="chirp.receiver" />: 
	<a href="actor/profile.do?actorId=${_chirp.receiver.id}" >
		<jstl:out value="${_chirp.receiver.name}" />
	</a>
</p>

<fieldset>
	<legend><spring:message code="chirp.text" /></legend>
	<p><jstl:out value="${_chirp.text}" /></p>
</fieldset>
	
<fieldset>
	<legend><spring:message code="chirp.attachments" /></legend>
	<display:table name="_chirp.attachments" id="attachment" requestURI="${requestURI}" pagesize="5">
		<spring:message code="chirp.attachments" var="attachmentsTitle" />
		<display:column title="${attachmentsTitle}">
			<a href="${attachment.url}"><jstl:out value="${attachment.url}" /></a>
		</display:column>
	</display:table>
</fieldset>
<br />

<fieldset>
	<legend><spring:message code="chirp.options" /></legend>
	<%
		Chirp chirp = (Chirp) request.getAttribute("_chirp");
		request.setAttribute("noReply", ((Chirp) chirp).getSender() instanceof Manager);
	%>
	<jstl:if test="${!noReply}">
		<jstl:if test="${received}">
			<acme:goto url="chirp/reply.do?chirpId=${_chirp.id}" code="chirp.reply"/>
		</jstl:if>
		<acme:goto url="chirp/forward.do?chirpId=${_chirp.id}" code="chirp.forward"/>
	</jstl:if>
	<acme:gotoConfirm url="chirp/delete.do?chirpId=${_chirp.id}" code="chirp.delete" text="chirp.delete.confirm"/>
</fieldset>