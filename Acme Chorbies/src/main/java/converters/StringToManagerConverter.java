package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.ManagerRepository;
import domain.Manager;

@Component
@Transactional
public class StringToManagerConverter extends StringToGenericConverter<Manager, ManagerRepository> {

}
