package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.ChirpRepository;
import domain.Chirp;

@Component
@Transactional
public class StringToChirpConverter extends StringToGenericConverter<Chirp, ChirpRepository> {

}
