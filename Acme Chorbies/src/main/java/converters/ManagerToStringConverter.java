package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Manager;

@Component
@Transactional
public class ManagerToStringConverter extends GenericToStringConverter<Manager> {

}
