package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Configuration;

@Component
@Transactional
public class ConfigurationToStringConverter extends GenericToStringConverter<Configuration>{

}
