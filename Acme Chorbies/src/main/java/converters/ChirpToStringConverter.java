package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Chirp;

@Component
@Transactional
public class ChirpToStringConverter extends GenericToStringConverter<Chirp>{

}
