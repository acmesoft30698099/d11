package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.AdministratorRepository;
import domain.Administrator;

@Component
@Transactional
public class StringToAdministratorConverter extends StringToGenericConverter<Administrator, AdministratorRepository>{

}
