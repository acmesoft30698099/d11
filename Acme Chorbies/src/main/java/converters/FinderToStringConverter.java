package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Finder;

@Component
@Transactional
public class FinderToStringConverter extends GenericToStringConverter<Finder> {

}
