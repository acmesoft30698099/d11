package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.CreditCard;

@Component
@Transactional
public class CreditCardToStringConverter extends GenericToStringConverter<CreditCard> {

}
