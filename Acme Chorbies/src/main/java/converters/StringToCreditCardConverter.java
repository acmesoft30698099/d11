package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.CreditCard;
import repositories.CreditCardRepository;

@Component
@Transactional
public class StringToCreditCardConverter extends StringToGenericConverter<CreditCard, CreditCardRepository> {

}
