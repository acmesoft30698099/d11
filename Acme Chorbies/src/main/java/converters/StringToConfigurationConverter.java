package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Configuration;
import repositories.ConfigurationRepository;

@Component
@Transactional
public class StringToConfigurationConverter extends StringToGenericConverter<Configuration, ConfigurationRepository>{

}
