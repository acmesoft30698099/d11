package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Administrator;

@Component
@Transactional
public class AdministratorToStringConverter extends GenericToStringConverter<Administrator>{

}
