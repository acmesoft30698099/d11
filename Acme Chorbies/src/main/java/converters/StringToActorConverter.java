package converters;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import domain.Actor;
import repositories.ActorRepository;

@Component
@Transactional
public class StringToActorConverter implements Converter<String, Actor> {
	
	@Autowired
	private ActorRepository repository;

	@Override
	public Actor convert(String source) {
		Actor result;
		int id;
		
		try {
			if(StringUtils.isEmpty(source)) {
				result = null;
			} else {
				id = Integer.valueOf(source);
				result = repository.findById(id);
			}
		} catch(Throwable excp) {
			throw new IllegalArgumentException();
		}
		
		return result;
	}

}
