/* WelcomeController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 * 
 */

package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import security.Authority;
import services.ConfigurationService;
import services.utils.ServicesUtils;
import domain.Chorbi;
import domain.Url;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {

	// Constructors -----------------------------------------------------------
	
	public WelcomeController() {
		super();
	}
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ConfigurationService configurationService;
		
	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/index")
	public ModelAndView index() {
		ModelAndView result;
		
		result = new ModelAndView("welcome/index");
		
		Url banner = configurationService.getRandomBanner();
		result.addObject("banner",banner);
		
		try {
			Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
			Authority auth = (Authority) chorbi.getUserAccount().getAuthorities().toArray()[0];
			boolean isBanned = auth.getAuthority().equals(Authority.BANNED_CHORBI);
			result.addObject("banned",isBanned);
		} catch (Throwable excp) {
			// Usuario no registrado, no hacemos nada
		}

		return result;
	}
}