/* managerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 * 
 */

package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ManagerService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Manager;
import forms.ManagerForm;

@Controller
@RequestMapping("/_manager")
public class ManagerController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private ManagerService managerService;

	@Autowired
	private ServicesUtils servicesUtils;

	// Constructors -----------------------------------------------------------

	public ManagerController() {
		super();
	}

	// List
	// ---------------------------------------------------------------------

	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Manager> managers;

		managers = managerService.findAll();

		result = new ModelAndView("manager/list");
		result.addObject("managers", managers);

		return result;
	}

	// Profile -----------------------------------------------------------------

	@RequestMapping("profile")
	public ModelAndView profile(
			@RequestParam(required = false) Integer managerId) {
		ModelAndView result;
		Manager manager;

		result = new ModelAndView("manager/profile");

		try {
			if (managerId == null) {
				manager = servicesUtils.checkRegistered(Manager.class,
						servicesUtils.checkUser());
				result.addObject("manager", manager);
				result.addObject("edit", true);
			} else {
				manager = managerService.findOne(managerId);
				result.addObject("manager", manager);
			}
		} catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Creation
	// -----------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		ManagerForm manager = null;

		try {
			Actor actor = servicesUtils.checkUser();
			result = new ModelAndView("redirect:/actor/profile.do?actorId="+actor.getId());
		} catch (Throwable th) {
			try {
				manager = managerService.construct(managerService.create());

				result = createEditModelAndView(manager);
			} catch (Throwable t) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		}

		return result;
	}

	// Edit
	// ---------------------------------------------------------------------

	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = true) Integer managerId) {
		ModelAndView result;
		Manager manager;
		ManagerForm managerForm;

		try {
			manager = managerService.findOneForEdit(managerId);
			managerForm = managerService.construct(manager);

			result = createEditModelAndView(managerForm);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value = "manager") ManagerForm managerForm, BindingResult binding) {
		ModelAndView result;

		try {
			Manager manager = managerService.reconstruct(managerForm, binding);

			if (binding.hasErrors()) {
				if (manager.getId() <= 0
						&& !managerForm
								.getUserAccount()
								.getPassword()
								.equals(managerForm.getUserAccount()
										.getConfirmPassword())) {
					result = createEditModelAndView(managerForm,
							"password.dontmatch");
				} else {
					result = createEditModelAndView(managerForm);
				}
			} else {
				try {
					if (manager.getId() <= 0
							&& !managerForm
									.getUserAccount()
									.getPassword()
									.equals(managerForm.getUserAccount()
											.getConfirmPassword())) {
						result = createEditModelAndView(managerForm,
								"password.dontmatch");
					} else {
						int managerId = manager.getId();
						manager = managerService.save(manager);
						if (managerId <= 0) {
							result = new ModelAndView(
									"redirect:/security/login.do");
						} else {
							result = new ModelAndView(
									"redirect:/_manager/profile.do");
						}
					}
				} catch (DataIntegrityViolationException excp) {
					// Este caso es un caso especial que s�lo ocurrira cuando el
					// UserAccount que ha puesto el usuario tiene el mismo
					// username que uno ya existente, entonces se le informar�
					// de esto
					result = createEditModelAndView(managerForm,
							"userAccount.already.used");
				} catch (Throwable t) {
					if (t instanceof IllegalArgumentException
							&& t.getMessage().contains("businessRule")) {
						result = createEditModelAndView(managerForm,
								t.getMessage());
					} else {
						result = createEditModelAndView(managerForm,
								"commit.error");
					}
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Ancillary Methods
	// ----------------------------------------------------------

	private ModelAndView createEditModelAndView(ManagerForm manager) {
		ModelAndView result;

		result = createEditModelAndView(manager, null);

		return result;
	}

	private ModelAndView createEditModelAndView(ManagerForm manager,
			String message) {
		ModelAndView result;

		result = new ModelAndView("manager/edit");
		result.addObject("manager", manager);
		result.addObject("message", message);

		return result;
	}

}
