package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.CreditCardService;
import services.ManagerService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Chorbi;
import domain.CreditCard;
import domain.Manager;
import forms.CreditCardForm;

@Controller
@RequestMapping("creditcard")
public class CreditCardController extends AbstractController {

	@Autowired
	private CreditCardService creditCardService;
	
	@Autowired
	private ManagerService managerService;
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@RequestMapping(value="view", method=RequestMethod.GET)
	public ModelAndView view() {
		ModelAndView res = new ModelAndView("creditcard/view");
		try{
			Actor registered = servicesUtils.checkUser();
			CreditCard creditCard = null;
			if(registered instanceof Manager){
				Manager manager = (Manager) registered;
				if(manager.getCreditCard() != null) {
					creditCard = creditCardService.findOne(manager.getCreditCard().getId());
				}
			}else{
				Chorbi chorbi = (Chorbi) registered;
				if(chorbi.getCreditCard() != null) {
					creditCard = creditCardService.findOne(chorbi.getCreditCard().getId());
				}
			}
			if(creditCard != null) {
				creditCard.setValid(creditCardService.validCreditCard(creditCard));
			}
			res.addObject("creditCard", creditCard);
		}catch(Throwable t){
			res = new ModelAndView("redirect:/misc/403.do");
		}
		return res;
	}
	
	@RequestMapping(value="create", method=RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res = null;
		CreditCard creditCard = new CreditCard();
		CreditCardForm form = creditCardService.construct(creditCard);
		res = createEditModelAndView(form);
		return res;
	}
	
	@RequestMapping(value="edit", method=RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res = null;
		try{
			Actor registered = servicesUtils.checkUser();
			CreditCard creditCard = null;
			if(registered instanceof Manager){
				Manager manager = (Manager) registered;
				creditCard = creditCardService.findOneForEdit(manager.getCreditCard().getId());
			}else{
				Chorbi chorbi = (Chorbi) registered;
				creditCard = creditCardService.findOneForEdit(chorbi.getCreditCard().getId());
			}
			CreditCardForm form = creditCardService.construct(creditCard);
			res = createEditModelAndView(form);
		}catch(Throwable t){
			res = new ModelAndView("redirect:/misc/403.do");
		}
		return res;
	}
	
	@RequestMapping(value="edit", method=RequestMethod.POST, params="save")
	public ModelAndView edit(@ModelAttribute("creditCardForm") CreditCardForm form, BindingResult binding) {
		ModelAndView res = null;
		try{
			CreditCard creditCard = creditCardService.reconstruct(form, binding);
			checkCreditCardForm(creditCard,binding);
			if(binding.hasErrors()) {
				res = createEditModelAndView(form);
			}
			else {
				try {
					int creditCardId = creditCard.getId();
					creditCard = creditCardService.save(creditCard);
					if(creditCardId == 0) {
						try {
							Actor actor = servicesUtils.checkUser();
							if(actor instanceof Manager) {
								Manager manager = managerService.findOne(actor.getId());
								manager.setCreditCard(creditCard);
								managerService.save(manager);
							} else {
								Chorbi chorbi = chorbiService.findOne(actor.getId());
								chorbi.setCreditCard(creditCard);
								chorbiService.save(chorbi);
							}
						} catch (Throwable e) {
							
						}
					}
					res = new ModelAndView("redirect:/creditcard/view.do");
				} catch(Throwable t) {
					if(t instanceof IllegalArgumentException && t.getMessage().contains("businessRule")) {
						res = createEditModelAndView(form, t.getMessage());
					} else {
						res = createEditModelAndView(form, "commit.error");
					}
				}
			}
		}catch(Throwable t){
			res = new ModelAndView("redirect:/misc/403.do");
		}
		return res;
	}
	
	@RequestMapping(value="delete", method=RequestMethod.GET)
	public ModelAndView delete() {
		ModelAndView res = new ModelAndView("redirect:/chorbi/profile.do");
		try {
			Actor registered = servicesUtils.checkUser();
			CreditCard creditCard = null;
			if(registered instanceof Manager){
				Manager m = (Manager) registered;
				creditCard = creditCardService.findOneForEdit(m.getCreditCard().getId());
			}else{
				Chorbi c = (Chorbi) registered;
				creditCard = creditCardService.findOneForEdit(c.getCreditCard().getId());
			}
			creditCardService.delete(creditCard);
		} catch(Throwable t) {
			res = new ModelAndView("redirect:/misc/403.do");
		}
		return res;
	}
	
	// Ancillary methods
	
	private void checkCreditCardForm(CreditCard creditCard, BindingResult binding) {
		
	}
	
	private ModelAndView createEditModelAndView(CreditCardForm form) {
		return createEditModelAndView(form, null);
	}
	
	private ModelAndView createEditModelAndView(CreditCardForm form, String message) {
		ModelAndView res = new ModelAndView("creditcard/edit");
		res.addObject("creditCardForm", form);
		res.addObject("message", message);
		return res;
	}
	
}
