
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.LikesService;
import services.utils.ServicesUtils;
import domain.Chorbi;
import domain.Likes;
import forms.LikesForm;

@Controller
@RequestMapping("/likes")
public class LikesController extends AbstractController {

	// Constructor ----------------------------------

	public LikesController() {
		super();
	}


	// Services -------------------------------------

	@Autowired
	private LikesService	likesService;

	@Autowired
	private ChorbiService	chorbiService;

	@Autowired
	private ServicesUtils	servicesUtils;


	// Create ---------------------------------------

	@RequestMapping("create")
	public ModelAndView create(@RequestParam(required = true) Integer chorbiId) {
		ModelAndView result;
		LikesForm likesForm;
		Likes likes;
		Chorbi chorbi;

		try {
			likes = likesService.create();

			chorbi = chorbiService.findOne(chorbiId);
			likes.setReceiver(chorbi);

			likesForm = likesService.construct(likes);

			result = createEditModelAndView(likesForm);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Save -----------------------------------------

	@RequestMapping(value = "edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value = "likes") LikesForm likesForm, BindingResult binding) {
		ModelAndView result = null;
		Likes likes;

		try {
			likes = likesService.reconstruct(likesForm, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(likesForm);
			} else {
				try {
					likes = likesService.save(likes);
					result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId=" + likes.getReceiver().getId());
				} catch (Throwable excp) {
					result = createEditModelAndView(likesForm, "commit.error");
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}
	// Remove ---------------------------------------

	@RequestMapping("/remove")
	public ModelAndView remove(@RequestParam(required = true) Integer likesId) {
		ModelAndView result;
		Likes likes;

		likes = likesService.findOne(likesId);

		try {
			result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId=" + likes.getReceiver().getId());
			likesService.delete(likes);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId=" + likes.getReceiver().getId());
			result.addObject("likeRemoveError", "commit.error");
		}

		return result;
	}

	// Ancillary methods ----------------------------

	private ModelAndView createEditModelAndView(LikesForm likesForm) {
		return createEditModelAndView(likesForm, null);
	}

	private ModelAndView createEditModelAndView(LikesForm likesForm, String message) {
		Collection<Chorbi> chorbies = chorbiService.findAll();
		Chorbi currentUser = servicesUtils.getRegisteredUser(Chorbi.class);
		chorbies.remove(currentUser);
		ModelAndView result;

		result = new ModelAndView("likes/edit");

		result.addObject("likes", likesForm);
		result.addObject("chorbies", chorbies);
		result.addObject("message", message);
		result.addObject("returnURL","chorbi/profile.do?chorbiId="+likesForm.getReceiver().getId());

		return result;
	}
}
