/* chorbiController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 * 
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.Authority;
import security.UserAccount;
import security.UserAccountService;
import services.ChorbiService;
import services.CreditCardService;
import services.LikesService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Chorbi;
import forms.ChorbiForm;

@Controller
@RequestMapping("/chorbi")
public class ChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private UserAccountService userAccountService;

	@Autowired
	private LikesService likesService;
	
	@Autowired
	private CreditCardService creditCardService;
	
	// Constructors -----------------------------------------------------------

	public ChorbiController() {
		super();
	}
	
	// List --------------------------------------------------------------------
	
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Chorbi> chorbies;
		
		chorbies = chorbiService.findAll();
		
		result = new ModelAndView("chorbi/list");
		result.addObject("chobies",chorbies);
		
		return result;
	}
	
	// Profile -----------------------------------------------------------------

	@RequestMapping("profile")
	public ModelAndView profile(@RequestParam(required = false) Integer chorbiId) {
		ModelAndView result;
		Chorbi chorbi;

		result = new ModelAndView("chorbi/profile");
		
		try {
			if (chorbiId == null) {
				chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
				// Para evitar la b�squeda perezosa
				chorbi.getReceivedLikes().size();
				result.addObject("chorbi", chorbi);
				result.addObject("description",servicesUtils.removePersonalData(chorbi.getDescription()));
				result.addObject("edit", true);
			} else {
				chorbi = chorbiService.findOne(chorbiId);
				// Para evitar la b�squeda perezosa
				chorbi.getReceivedLikes().size();
				
				Authority auth = (Authority) chorbi.getUserAccount().getAuthorities().toArray()[0];
				boolean isBanned = auth.getAuthority().equals(Authority.BANNED_CHORBI);
				result.addObject("isBanned",isBanned);
				result.addObject("chorbi", chorbi);
				result.addObject("description",servicesUtils.removePersonalData(chorbi.getDescription()));
				try {
					Chorbi chorbiReg = servicesUtils.checkRegistered(Chorbi.class,
							servicesUtils.checkUser());
					if (chorbiReg.getId() == chorbiId) {
						result.addObject("edit", true);
					}
					
					result.addObject("canLike",!likesService.hasLikedTheUser(chorbiReg, chorbi));
					result.addObject("registeredUser",chorbiReg);
				}
				catch (Throwable t) {
					// Usuario no registrado
				}
			}
			result.addObject("description",servicesUtils.removePersonalData(chorbi.getDescription()));
		}
		catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Creation
	// -----------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		ChorbiForm chorbi = null;
		
		try {
			Actor actor = servicesUtils.checkUser();
			result = new ModelAndView("redirect:/actor/profile.do?actorId="+actor.getId());
		}
		catch (Throwable th) {
			try {
				chorbi = chorbiService.construct(chorbiService.create());

				result = createEditModelAndView(chorbi);
			}
			catch (Throwable t) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		}

		return result;
	}
	
	// Edit
	// ---------------------------------------------------------------------
	
	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = true) Integer chorbiId) {
		ModelAndView result;
		Chorbi chorbi;
		ChorbiForm chorbiForm;

		try {
			chorbi = chorbiService.findOneForEdit(chorbiId);
			chorbiForm = chorbiService.construct(chorbi);

			result = createEditModelAndView(chorbiForm);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value="chorbi") ChorbiForm chorbiForm, BindingResult binding) {
		ModelAndView result;
		
		try {
			Chorbi chorbi = chorbiService.reconstruct(chorbiForm, binding);
			
			if (binding.hasErrors()) {
				if (chorbi.getId() <= 0 && !chorbiForm.getUserAccount().getPassword()
						.equals(chorbiForm.getUserAccount().getConfirmPassword())) {
					result = createEditModelAndView(chorbiForm, "password.dontmatch");
				}
				else {
					result = createEditModelAndView(chorbiForm);
				}
			}
			else {		
				try {
					if (chorbi.getId() <= 0 && !chorbiForm.getUserAccount().getPassword()
							.equals(chorbiForm.getUserAccount().getConfirmPassword())) {
						result = createEditModelAndView(chorbiForm, "password.dontmatch");
					}
					else {
						int chorbiId = chorbi.getId();
						chorbi = chorbiService.save(chorbi);
						if (chorbiId <= 0) {
							result = new ModelAndView("redirect:/security/login.do");
						}
						else {
							result = new ModelAndView("redirect:/chorbi/profile.do");
						}
					}
				}
				catch (DataIntegrityViolationException excp) {
					// Este caso es un caso especial que s�lo ocurrira cuando el UserAccount que ha puesto el usuario tiene el mismo 
					// username que uno ya existente, entonces se le informar� de esto
					result = createEditModelAndView(chorbiForm,"userAccount.already.used");
				}
				catch (Throwable t) {
					if(t instanceof IllegalArgumentException && t.getMessage().contains("bussinessRule")) {
						result = createEditModelAndView(chorbiForm, t.getMessage());
					} else {
						result = createEditModelAndView(chorbiForm, "commit.error");
					}
				}
			}
		}
		catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Ban management
	// ----------------------------------------------------------
	
	@RequestMapping("/banChorbi")
	public ModelAndView banChorbi(@RequestParam(required=true) Integer chorbiId) {
		ModelAndView result;
		Chorbi chorbi;
		
		chorbi = chorbiService.findOne(chorbiId);
		
		try {
			UserAccount userAccount = chorbi.getUserAccount();
			
			Collection<Authority> auths = new ArrayList<Authority>();
			Authority banned = new Authority();
			banned.setAuthority(Authority.BANNED_CHORBI);
			auths.add(banned);
			userAccount.setAuthorities(auths);
			
			userAccountService.changeAuthorities(userAccount);
			
			result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId="+chorbi.getId());
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId="+chorbi.getId());
			result.addObject("ban_message","commit.error");
		}
		
		return result;
	}
	
	@RequestMapping("/unbanChorbi")
	public ModelAndView unbanChorbi(@RequestParam(required=true) Integer chorbiId) {
		ModelAndView result;
		Chorbi chorbi;
		
		chorbi = chorbiService.findOne(chorbiId);
		
		try {
			UserAccount userAccount = chorbi.getUserAccount();
			
			Collection<Authority> auths = new ArrayList<Authority>();
			Authority normal = new Authority();
			normal.setAuthority(Authority.CHORBI);
			auths.add(normal);
			userAccount.setAuthorities(auths);
			
			userAccountService.changeAuthorities(userAccount);
			
			result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId="+chorbi.getId());
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId="+chorbi.getId());
			result.addObject("ban_message","commit.error");
		}
		
		return result;
	}
	
	
	// List liked -----------------------------------------------
	
	@RequestMapping("/likedList")
	public ModelAndView likedList() {
		ModelAndView result;
		Chorbi chorbi;
		
		try {
			chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
			result = new ModelAndView("chorbi/list");
			if(chorbi.getCreditCard() == null || 
					!creditCardService.validCreditCard(chorbi.getCreditCard())) {
				result.addObject("message","businessRule.credit.card.invalid.list");
			} else {
				result.addObject("chobies",likesService.getChorbiesThatLiked(chorbi));
			}
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}

	// Ancillary Methods
	// ----------------------------------------------------------

	private ModelAndView createEditModelAndView(ChorbiForm chorbi) {
		ModelAndView result;

		result = createEditModelAndView(chorbi, null);

		return result;
	}

	private ModelAndView createEditModelAndView(ChorbiForm chorbi, String message) {
		ModelAndView result;

		result = new ModelAndView("chorbi/edit");
		result.addObject("chorbi", chorbi);
		result.addObject("message", message);

		return result;
	}
	
}
