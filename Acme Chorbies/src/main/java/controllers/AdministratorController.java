/* AdministratorController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 * 
 */

package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import services.utils.ServicesUtils;
import domain.Administrator;
import forms.AdministratorForm;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private ServicesUtils servicesUtils;

	// Constructors -----------------------------------------------------------
	
	public AdministratorController() {
		super();
	}
	
	// Profile -----------------------------------------------------------------

	@RequestMapping("profile")
	public ModelAndView profile(@RequestParam(required = false) Integer administratorId) {
		ModelAndView result;
		Administrator administrator;
		//Collection<Comment> comments;

		result = new ModelAndView("administrator/profile");
		
		try {
			if (administratorId == null) {
				administrator = servicesUtils.checkRegistered(Administrator.class, servicesUtils.checkUser());
				result.addObject("administrator", administrator);
				result.addObject("edit", true);
			}
			else {
				administrator = administratorService.findOne(administratorId);
				result.addObject("administrator", administrator);
				try {
					administrator = servicesUtils.checkRegistered(Administrator.class,
							servicesUtils.checkUser());
					if (administrator.getId() == administratorId) {
						result.addObject("edit", true);
					}
				}
				catch (Throwable t) {
					// Usuario no registrado
				}
			}
		}
		catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}

	// Edit
	// ---------------------------------------------------------------------

	@RequestMapping(value="/edit", method=RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res = null;
		Administrator admin = servicesUtils.getRegisteredUser(Administrator.class);
		Assert.notNull(admin);
		AdministratorForm form = administratorService.construct(admin);
		res = createEditModelAndView(form);
		return res;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView edit(@ModelAttribute(value="administrator") AdministratorForm administratorForm, BindingResult binding) {
		ModelAndView result;
		try {
			Administrator administrator = administratorService.reconstruct(administratorForm, binding);
			
			if (binding.hasErrors()) {
				result = createEditModelAndView(administratorForm);
			}
			else {
				administrator = administratorService.save(administrator);
				result = new ModelAndView("redirect:/administrator/profile.do?administratorId="+ administrator.getId());
			}
		}
		catch (Throwable excp) {
			result = createEditModelAndView(administratorForm, "commit.error");
		}
		
		return result;
	}

	// Ancillary Methods
	// ----------------------------------------------------------

	private ModelAndView createEditModelAndView(AdministratorForm administrator) {
		ModelAndView result;

		result = createEditModelAndView(administrator, null);

		return result;
	}

	private ModelAndView createEditModelAndView(AdministratorForm administrator, String message) {
		ModelAndView result;

		result = new ModelAndView("administrator/edit");
		result.addObject("administrator", administrator);
		result.addObject("message", message);

		return result;
	}
	
}