package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.displaytag.decorator.TableDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import services.EventService;
import services.utils.ServicesUtils;
import controllers.utils.EventTableDecorator;
import domain.Chirp;
import domain.Chorbi;
import domain.Event;
import domain.Manager;
import domain.Url;
import forms.EventForm;

@Controller
@RequestMapping("/event")
public class EventController extends AbstractController {
	
	// Constructor -----------------------------------------
	
	public EventController() {
		super();
	}
	
	// Services --------------------------------------------
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Profile ---------------------------------------------
	
	@RequestMapping("/profile")
	public ModelAndView profile(@RequestParam(required=true) Integer eventId) {
		ModelAndView result;
		
		try {
			Event event = eventService.findOne(eventId);
			
			result = new ModelAndView("event/profile");
			// Permisos de edici�n
			try {
				Manager manager = servicesUtils.getRegisteredUser(Manager.class);
				Assert.isTrue(manager.getId() == event.getManager().getId());
				// El evento tiene que ser futuro para poder cambiarse
				Assert.isTrue(event.getDate().after(new Date()));
				result.addObject("canEdit",true);
			} catch(Throwable excp) {
				// El usuario actual no es el poseedor del Event, no hacemos nada
			}
			// Permisos de registro de chorbi
			try {
				Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
				// Comprobamos la fecha del evento
				Assert.isTrue(event.getDate().after(new Date()));
				// Comprobamos que se haya registrado
				Collection<Chorbi> registered = eventService.getRegisteredChorbiesFromEvent(event);
				if(registered.contains(chorbi)) {
					result.addObject("canUnRegister",true);
				} else {
					// Si no hay espacios disponibles, no podr� registrarse
					Assert.isTrue(event.getAvailableSeats() > 0);
					result.addObject("canRegister",true);
				}
			} catch(Throwable excp) {
				// El usuario actual no es Chorbi, no hacemos nada
				excp.getMessage();
			}
			
			result.addObject("event",event);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// List -------------------------------------------------
	
	@RequestMapping("/list")
	public ModelAndView listAll() {
		ModelAndView result;
		Collection<Event> events;
		// Este objeto decorar� las filas necesarias de los eventos determinados.
		TableDecorator decorator;
		
		events = eventService.findAll();
		decorator = new EventTableDecorator();
		
		result = new ModelAndView("event/listAll");
		result.addObject("events",events);
		result.addObject("decorator",decorator);
		
		return result;
	}
	
	// List available ---------------------------------------
	
	@RequestMapping("/listAvailable")
	public ModelAndView listAvailable() {
		ModelAndView result;
		Collection<Event> events;
		
		events = eventService.getAvailableEventsOfOneMonth();
		
		result = new ModelAndView("event/list");
		result.addObject("events",events);
		result.addObject("requestUri","event/listAvailable.do");
		
		return result;
	}
	
	// List registered --------------------------------------
	
	@RequestMapping("/listRegistered")
	public ModelAndView listRegistered() {
		ModelAndView result;
		Collection<Event> events;
		Chorbi chorbi;
		
		chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		events = chorbi.getEvents();
		
		result = new ModelAndView("event/list");
		result.addObject("events",events);
		result.addObject("requestUri","event/listRegistered.do");
		
		return result;
	}
	
	// List owns --------------------------------------------
	
	@RequestMapping("/listOwns")
	public ModelAndView listOwns() {
		ModelAndView result;
		Manager manager;
		Collection<Event> events;
		
		manager = servicesUtils.getRegisteredUser(Manager.class);
		events = manager.getEvents();
		
		result = new ModelAndView("event/list");
		result.addObject("events",events);
		result.addObject("requestUri","event/listOwns.do");
		result.addObject("canCreate",true);
		
		return result;
	}
	
	// Create ------------------------------------------------
	
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Event event;
		EventForm eventForm;
		
		event = eventService.create();
		eventForm = eventService.construct(event);
		
		result = new ModelAndView("event/edit");
		result.addObject("event",eventForm);
		
		return result;
	}
	
	// Edit ---------------------------------------------------
	
	@RequestMapping(value = "/edit", method=RequestMethod.GET)
	public ModelAndView edit(@RequestParam(required = true) Integer eventId) {
		ModelAndView result;
		Event event;
		EventForm eventForm;
		
		event = eventService.findOne(eventId);
		
		if(event.getDate().before(new Date())) {
			result = new ModelAndView("redirect:/misc/403.do");
		} else {
			eventForm = eventService.construct(event);
		
			result = createEditModelAndView(eventForm);
		}
		
		return result;
	}
	
	// Save ---------------------------------------------------
	
	@RequestMapping(value = "edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(
			@ModelAttribute(value = "event") EventForm eventForm,
			BindingResult binding) {
		ModelAndView result;
		Event event;

		try {
			event = eventService.reconstruct(eventForm, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(eventForm);
			} else {
				try {
					int eventId = event.getId();
					event = eventService.save(event);
					if(eventId > 0) {
						// No necesita todos los datos, creamos el Chirp desde dominio
						Chirp template = new Chirp();
						template.setSubject(event.getTitle()+" changed");
						template.setText("The event "+event.getTitle()+" in which you are registered has changed, to see the changes, follow the link in the attachments.");
						template.setAttachments(new ArrayList<Url>());
						Url urlEvento = new Url();
						urlEvento.setUrl("http://www.acme.com/event/profile.do?eventId="+event.getId());
						template.getAttachments().add(urlEvento);
						servicesUtils.sendBulkChirpsForEvent(event, template);
					}
					result = new ModelAndView("redirect:profile.do?eventId="+event.getId());
				} catch (Throwable excp) {
					if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
						result = createEditModelAndView(eventForm, excp.getMessage());
					} else {
						result = createEditModelAndView(eventForm,
								"commit.error");
					}
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}
	
	// Delete -------------------------------------------------
	
	@RequestMapping(value="edit",method=RequestMethod.POST,params="delete")
	public ModelAndView delete(@ModelAttribute(value="event") EventForm eventForm,
			BindingResult binding) {
		ModelAndView result;
		Event event;
		
		try {
			event = eventService.findOneForEdit(eventForm.getId());
			try {
				Chirp template = new Chirp();
				template.setSubject("Event " + event.getTitle() + " delete");
				template.setText("The event with title "+event.getTitle()+" in which you are registered has been deleted.");
				servicesUtils.sendBulkChirpsForEvent(event, template);
				eventService.delete(event);
				result = new ModelAndView("redirect:/event/listOwns.do");
			} catch (Throwable excp) {
				result = createEditModelAndView(eventForm, "commit.error");
			}
		} catch(Throwable excp) {
			result = new ModelAndView();
		}
		
		return result;
	}
	
	// Register -----------------------------------------------
	
	@RequestMapping("/register")
	public ModelAndView register(@RequestParam(required=true) Integer eventId, RedirectAttributes attr) {
		ModelAndView result;
		Chorbi chorbi;
		
		chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		result = new ModelAndView("redirect:/event/profile.do?eventId="+eventId);
		
		try {
			eventService.registerToEvent(chorbi.getId(), eventId);
		} catch(Throwable excp) {
			if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
				attr.addFlashAttribute("message",excp.getMessage());
			} else {
				attr.addFlashAttribute("message","commit.error");
			}
		}
		
		return result;
	}
	
	// Unregister ---------------------------------------------
	
	@RequestMapping("/unregister")
	public ModelAndView unregister(@RequestParam(required=true) Integer eventId, RedirectAttributes attr) {
		ModelAndView result;
		Chorbi chorbi;
		
		chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		result = new ModelAndView("redirect:/event/profile.do?eventId="+eventId);
		
		try {
			Event event = eventService.findOne(eventId);
			Assert.isTrue(event.getDate().after(new Date()));
			eventService.unregisterToEvent(chorbi.getId(), eventId);
		} catch(Throwable excp) {
			if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
				attr.addFlashAttribute("message",excp.getMessage());
			} else {
				attr.addFlashAttribute("message","commit.error");
			}
		}
		
		return result;
	}
	
	// Ancillary methods --------------------------------------
	
	private ModelAndView createEditModelAndView(EventForm eventForm) {
		return createEditModelAndView(eventForm,null);
	}
	
	private ModelAndView createEditModelAndView(EventForm eventForm, String message) {
		ModelAndView result;
		
		result = new ModelAndView("event/edit");
		result.addObject("event",eventForm);
		result.addObject("message",message);
		
		return result;
	}

}
