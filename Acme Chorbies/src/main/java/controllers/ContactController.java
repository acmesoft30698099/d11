package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("contact")
public class ContactController extends AbstractController {
	
	// Show
	
	@RequestMapping("show")
	public ModelAndView show() {
		ModelAndView result;
		
		result = new ModelAndView("contact/show");
		
		return result;
	}

}
