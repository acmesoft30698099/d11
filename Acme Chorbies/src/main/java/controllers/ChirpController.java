package controllers;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChirpService;
import services.ChorbiService;
import services.EventService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Chirp;
import domain.Chorbi;
import domain.Event;
import domain.Url;
import forms.ChirpForm;
import forms.ChirpFormEvent;

@Controller
@RequestMapping("/chirp")
public class ChirpController extends AbstractController {

	// Constructor ----------------------

	public ChirpController() {
		super();
	}

	// Services -------------------------

	@Autowired
	private ChirpService chirpService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private EventService eventService;

	// List -----------------------------

	@RequestMapping("/listReceived")
	public ModelAndView listReceived() {
		ModelAndView result;
		Collection<Chirp> chirps;

		try {
			Chorbi chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
			
			chirps = chorbi.getReceivedChirps();

			result = new ModelAndView("chirp/list");

			result.addObject("chirps", chirps);
			result.addObject("received", true);
			result.addObject("requestUri","chirp/listReceived.do");
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	@RequestMapping("/listSent")
	public ModelAndView listSent() {
		ModelAndView result;
		Collection<Chirp> chirps;

		try {
			Chorbi chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
			
			chirps = chorbi.getSentChirps();

			result = new ModelAndView("chirp/list");

			result.addObject("chirps", chirps);
			result.addObject("sent", true);
			result.addObject("requestUri","chirp/listSent.do");
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Create normal ---------------------------

	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Chirp chirp;
		ChirpForm chirpForm;

		chirp = chirpService.create();
		chirpForm = chirpService.construct(chirp);

		result = createEditModelAndView(chirpForm);

		return result;
	}

	// Create reply ----------------------------

	@RequestMapping("/reply")
	public ModelAndView replyCreate(
			@RequestParam(required = true) Integer chirpId) {
		ModelAndView result;
		Chirp chirp;
		ChirpForm chirpForm;

		try {
			chirp = chirpService.findOne(chirpId);
			
			chirpForm = chirpService.constructReply(chirp);
			chirpForm.setReceiver((Chorbi) chirp.getSender()); 
			chirpForm.setAttachments(new ArrayList<Url>());

			result = createEditModelAndView(chirpForm);
			result.addObject("reply",true);
			result.addObject("texto",null);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Create forward ----------------------------

	@RequestMapping("/forward")
	public ModelAndView forwardCreate(
			@RequestParam(required = true) Integer chirpId) {
		ModelAndView result;

		try {
			// Comprobamos que exista el mensaje
			chirpService.findOne(chirpId);

			result = createForwardModelAndView(chirpId,null,"");
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Edit ------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "saveNormal")
	public ModelAndView save(
			@ModelAttribute(value = "chirpForm") ChirpForm chirpForm,
			BindingResult binding) {
		ModelAndView result;
		Chirp chirp;

		try {
			chirp = chirpService.reconstruct(chirpForm, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(chirpForm);
			} else {
				try {
					chirp = chirpService.save(chirp);
					result = new ModelAndView("redirect:profile.do?chirpId="
							+ chirp.getId());
				} catch (Throwable excp) {
					result = createEditModelAndView(chirpForm,"commit.error");
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "saveReply")
	public ModelAndView saveReply(
			@ModelAttribute(value = "chirpForm") ChirpForm chirpForm,
			BindingResult binding,
			@RequestParam(required=true) String texto) {
		ModelAndView result;
		Chirp chirp;

		try {
			chirp = chirpService.reconstructReply(chirpForm,binding, texto);
			if (binding.hasErrors()) {
				result = createEditModelAndView(chirpForm);
				result.addObject("reply",true);
				result.addObject("texto",texto.trim());
			} else {
				try {
					chirp = chirpService.save(chirp);
					result = new ModelAndView("redirect:profile.do?chirpId="
							+ chirp.getId());
				} catch (Throwable excp) {
					result = createEditModelAndView(chirpForm,
							"commit.error");
					result.addObject("reply",true);
					result.addObject("texto",texto.trim());
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "saveForward")
	public ModelAndView saveForward(
			@RequestParam(value="actorId")@ModelAttribute int actorId,
			@RequestParam(value="chirpId")@ModelAttribute int chirpId) {
		ModelAndView result;
		Chirp chirp;

		try {
			chirp = chirpService.reconstructForward(chirpId, actorId);
			try {
				chirp = chirpService.save(chirp);
				result = new ModelAndView("redirect:profile.do?chirpId="
						+ chirp.getId());
			} catch (Throwable excp) {
				result = createForwardModelAndView(actorId, chirpId, "commit.error");
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}
	
	// Delete ----------------------------------
	
	@RequestMapping("delete")
	public ModelAndView delete(@RequestParam(required=true) Integer chirpId) {
		ModelAndView result;
		Chirp chirp;
		Actor actor;
		
		actor = servicesUtils.checkUser();
		chirp = chirpService.findOne(chirpId);
		
		if(chirp == null || (chirp.getReceiver().getId() != actor.getId() &&
				chirp.getSender().getId() != actor.getId())) {
			result = new ModelAndView("redirect:/misc/403.do");
		} else {
			chirpService.delete(chirp);
			result = new ModelAndView("redirect:/chirp/listSent.do");
		}
		
		return result;
	}

	// Attachments handling --------------------

	@RequestMapping(value = "removeAttachmentStandard", method = RequestMethod.POST)
	public ModelAndView removeAttachmentStandard(
			@ModelAttribute(value = "chirpForm") ChirpForm chirpForm,
			BindingResult binding,
			@RequestParam(required=true) int attachmentIndex,
			@RequestParam(required = false) String texto) {
		ModelAndView result;
		
		chirpForm.getAttachments().remove(attachmentIndex);

		result = createEditModelAndView(chirpForm);
		if(texto!=null){
			result.addObject("texto",texto.trim());
		}

		return result;
	}
	
	@RequestMapping(value = "addAttachmentStandard", method = RequestMethod.POST)
	public ModelAndView addAttachmentStandard(
			@ModelAttribute(value = "chirpForm") ChirpForm chirpForm,
			BindingResult binding,
			@RequestParam(required = false) String texto) {
		ModelAndView result;
		Url attachment = new Url();

		if (chirpForm.getAttachments() == null) {
			chirpForm.setAttachments(new ArrayList<Url>());
		}

		chirpForm.getAttachments().add(attachment);

		result = createEditModelAndView(chirpForm);
		if(texto!=null){
			result.addObject("texto",texto.trim());
		}

		return result;
	}
	
	@RequestMapping(value = "removeAttachmentReply", method = RequestMethod.POST)
	public ModelAndView removeAttachmentReply(
			@ModelAttribute(value = "chirpForm") ChirpForm chirpForm,
			BindingResult binding,
			@RequestParam(required=true) int attachmentIndex,
			@RequestParam(required = false) String texto) {
		ModelAndView result;

		chirpForm.getAttachments().remove(attachmentIndex);

		result = createEditModelAndView(chirpForm);
		
		result.addObject("reply",true);
		if(texto!=null){
			result.addObject("texto",texto.trim());
		}

		return result;
	}
	
	@RequestMapping(value = "addAttachmentReply", method = RequestMethod.POST)
	public ModelAndView addAttachmentReply(
			@ModelAttribute(value = "chirpForm") ChirpForm chirpForm,
			BindingResult binding,
			@RequestParam(required = false) String texto) {
		ModelAndView result;
		Url attachment = new Url();

		if (chirpForm.getAttachments() == null) {
			chirpForm.setAttachments(new ArrayList<Url>());
		}

		chirpForm.getAttachments().add(attachment);

		result = createEditModelAndView(chirpForm);
		
		result.addObject("reply",true);
		if(texto!=null){
			result.addObject("texto",texto.trim());
		}

		return result;
	}

	// Profile ---------------------------------

	@RequestMapping("/profile")
	public ModelAndView profile(@RequestParam(required = true) Integer chirpId) {
		ModelAndView result;
		Chirp chirp;
		Chorbi chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());

		chirp = chirpService.findOne(chirpId);
		if (chorbi.getId() != chirp.getSender().getId()
				&& chorbi.getId() != chirp.getReceiver().getId()) {
			result = new ModelAndView("redirect:/misc/403.do");
		} else {
			result = new ModelAndView("chirp/profile");
			result.addObject("_chirp",chirp);
			if(chorbi.getId() == chirp.getReceiver().getId()){
				result.addObject("received",true);
			}
		}
		result.addObject("requestURI", "chirp/profile.do");
		
		return result;
	}
	
	// Bulk message ----------------------------
	
	@RequestMapping(value="/bulk", method=RequestMethod.GET)
	public ModelAndView createBulk(@RequestParam(required = true) Integer eventId) {
		ModelAndView result;
		ChirpFormEvent chirpForm;
		
		try {
			// Comprobar los permisos de edici�n
			eventService.findOneForEdit(eventId);
			chirpForm = chirpService.constructBulk(eventId);
			
			result = createEditBulkModelAndView(chirpForm);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Bulk message save -----------------------
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "saveBulk")
	public ModelAndView saveBulk(
			@ModelAttribute(value = "chirpForm")@Valid ChirpFormEvent chirpForm,
			BindingResult binding) {
		ModelAndView result;
		Chirp chirp;
		Event event;

		try {
			chirp = chirpService.reconstructBulk(chirpForm);
			event = eventService.findOneForEdit(chirpForm.getEventId());
			if (binding.hasErrors()) {
				result = createEditBulkModelAndView(chirpForm);
			} else {
				try {
					servicesUtils.sendBulkChirpsForEvent(event, chirp);
					result = new ModelAndView("redirect:/event/profile.do?eventId="+chirpForm.getEventId());
				} catch (Throwable excp) {
					result = createEditBulkModelAndView(chirpForm,
							"commit.error");
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}
	
	// Bulk message add ------------------------
	
	@RequestMapping(value = "removeAttachmentBulk", method = RequestMethod.POST)
	public ModelAndView removeAttachmentBulk(
			@ModelAttribute(value = "chirpForm") ChirpFormEvent chirpForm,
			BindingResult binding,
			@RequestParam(required=true) int attachmentIndex) {
		ModelAndView result;

		chirpForm.getAttachments().remove(attachmentIndex);

		result = createEditBulkModelAndView(chirpForm);

		return result;
	}
	
	@RequestMapping(value = "addAttachmentBulk", method = RequestMethod.POST)
	public ModelAndView addAttachmentBulk(
			@ModelAttribute(value = "chirpForm") ChirpFormEvent chirpForm,
			BindingResult binding) {
		ModelAndView result;
		Url attachment = new Url();

		if (chirpForm.getAttachments() == null) {
			chirpForm.setAttachments(new ArrayList<Url>());
		}

		chirpForm.getAttachments().add(attachment);
		
		result = createEditBulkModelAndView(chirpForm);

		return result;
	}	

	// Ancillary methods -----------------------

	public ModelAndView createEditModelAndView(ChirpForm chirpForm) {
		return createEditModelAndView(chirpForm, null);
	}
	
	public ModelAndView createForwardModelAndView(Integer chirpId, Integer actorId){
		return createForwardModelAndView(chirpId,actorId,null);
	}

	public ModelAndView createEditModelAndView(ChirpForm chirpForm,
			String chirp) {
		ModelAndView result = new ModelAndView("chirp/edit");
		Collection<Chorbi> actors = chorbiService.findAll();
		Actor actor = servicesUtils.checkUser();
		
		actors.remove(actor);

		result.addObject("chirpForm", chirpForm);
		result.addObject("chirp", chirp);
		result.addObject("actors", actors);

		return result;
	}

	public ModelAndView createForwardModelAndView(Integer chirpId, Integer actorId, String chirp) {
		ModelAndView result = new ModelAndView("chirp/edit");
		Collection<Chorbi> actors = chorbiService.findAll();
		Chirp _chirp = chirpService.findOne(chirpId);
		Actor actor = servicesUtils.checkUser();
		
		actors.remove(actor);
		
		result.addObject("_chirp",_chirp);
		result.addObject("forward",true);
		result.addObject("chirp",chirp);
		result.addObject("actorId",actorId);
		result.addObject("actors",actors);
		
		return result;
	}
	
	private ModelAndView createEditBulkModelAndView(ChirpFormEvent chirpForm) {
		return createEditBulkModelAndView(chirpForm,null);
	}
	
	private ModelAndView createEditBulkModelAndView(ChirpFormEvent chirpForm, String message) {
		ModelAndView result;
		
		result = new ModelAndView("chirp/edit");
		
		result.addObject("chirpForm",chirpForm);
		result.addObject("bulk",true);
		result.addObject("message",message);
		
		return result;
	}
}
