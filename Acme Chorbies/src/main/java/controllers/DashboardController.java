/* AdministratorController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 * 
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.LikesService;
import services.ManagerService;
import domain.Chorbi;
import domain.Manager;

@Controller
@RequestMapping("/dashboard")
public class DashboardController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private ManagerService managerService;
	
	@Autowired
	private LikesService likesService;

	// Constructors -----------------------------------------------------------

	public DashboardController() {
		super();
	}
	
	// Dashboard
	// ----------------------------------------------------------------
	@RequestMapping("list")
	public ModelAndView dashboard(){
		ModelAndView result;
		Collection<Object[]> getNumberOfChorbiesByTheirCountryAndCityObject = chorbiService.getNumberOfChorbiesByTheirCountryAndCity();
		Integer getMaxAgeOfChorbies = chorbiService.getMaxAgeOfChorbies();
		Integer getMinAgeOfChorbies = chorbiService.getMinAgeOfChorbies();
		Double getAvgAgeOfChorbies = chorbiService.getAvgAgeOfChorbies();
		Long getAmountOfChorbieswithInvalidOrNullCreditCard = chorbiService.getAmountOfChorbieswithInvalidOrNullCreditCard();
		Long getAmountOfChorbiesSearchingForActivities = chorbiService.getAmountOfChorbiesSearchingFor("activities");
		Long getAmountOfChorbiesSearchingForFriendship = chorbiService.getAmountOfChorbiesSearchingFor("friendship");
		Long getAmountOfChorbiesSearchingForLove = chorbiService.getAmountOfChorbiesSearchingFor("love");
		Collection<Chorbi> getChorbiesOrderedByNumberOfReceivedLikes = new ArrayList<Chorbi>();
		getChorbiesOrderedByNumberOfReceivedLikes = chorbiService.getChorbiesOrderedByNumberOfReceivedLikes();
		Integer getMinNumberOfLikesPerChorbi = chorbiService.getMinNumberOfLikesPerChorbi();
		Integer getMaxNumberOfLikesPerChorbi = chorbiService.getMaxNumberOfLikesPerChorbi();
		Double getAvgNumberOfLikesPerChorbi = chorbiService.getAvgNumberOfLikesPerChorbi();
		Integer getMinNumberOfReceivedChirpsPerChorbi = chorbiService.getMinNumberOfReceivedChirpsPerChorbi();
		Integer getMaxNumberOfReceivedChirpsPerChorbi = chorbiService.getMaxNumberOfReceivedChirpsPerChorbi();
		Double getAvgNumberOfReceivedChirpsPerChorbi = chorbiService.getAvgNumberOfReceivedChirpsPerChorbi();
		Integer getMinNumberOfSentChirpsPerChorbi = chorbiService.getMinNumberOfSentChirpsPerChorbi();
		Integer getMaxNumberOfSentChirpsPerChorbi = chorbiService.getMaxNumberOfSentChirpsPerChorbi();
		Double getAvgNumberOfSentChirpsPerChorbi = chorbiService.getAvgNumberOfSentChirpsPerChorbi();
		Collection<Chorbi> getChorbiesWithTheMostReceivedChirps = new ArrayList<Chorbi>();
		getChorbiesWithTheMostReceivedChirps = chorbiService.getChorbiesWithTheMostReceivedChirps();
		Collection<Chorbi> getChorbiesWithTheMostSentChirps = new ArrayList<Chorbi>();
		getChorbiesWithTheMostSentChirps = chorbiService.getChorbiesWithTheMostSentChirps();
		
		Collection<Manager> getManagersOrderedByNumberOfEvents = new ArrayList<Manager>();
		getManagersOrderedByNumberOfEvents = managerService.getManagersOrderedByNumberOfEvents();
		Collection<Manager> getAllManager = new ArrayList<Manager>();
		getAllManager = managerService.findAll();
		Collection<Chorbi> getChorbiesOrderedByNumberOfEvents = new ArrayList<Chorbi>();
		getChorbiesOrderedByNumberOfEvents = chorbiService.getChorbiesOrderedByNumberOfEvents();
		Collection<Chorbi> getAllChorbies = new ArrayList<Chorbi>();
		getAllChorbies = chorbiService.findAll();
		Long getMinNumberOfStars = likesService.getMinNumberOfLikesPerChorbi();
		Long getMaxNumberOfStars = likesService.getMaxNumberOfLikesPerChorbi();
		Double getAvgNumberOfStars = likesService.getAvgNumberOfLikesPerChorbi();
		Collection<Chorbi> getChorbiesOrderedByAverageLikes = new ArrayList<Chorbi>();
		getChorbiesOrderedByAverageLikes = likesService.getChorbiesOrderedByAverageLikes();
		
		Map<String, Long> getNumberOfChorbiesByTheirCountryAndCity = new HashMap<String, Long>();
		for(Object[] array:getNumberOfChorbiesByTheirCountryAndCityObject){
			String key = (String) array[0];
			Long value = (Long) array[1];
			if(key!=null && key.startsWith(",")){
				key = key.substring(1,key.length());
			}
			getNumberOfChorbiesByTheirCountryAndCity.put(key,value); 
		}
		
		result = new ModelAndView("dashboard/list");
		result.addObject("getNumberOfChorbiesByTheirCountryAndCity",getNumberOfChorbiesByTheirCountryAndCity);
		result.addObject("getMaxAgeOfChorbies",getMaxAgeOfChorbies);
		result.addObject("getMinAgeOfChorbies",getMinAgeOfChorbies);
		result.addObject("getAvgAgeOfChorbies",getAvgAgeOfChorbies);
		result.addObject("getAmountOfChorbieswithInvalidOrNullCreditCard",getAmountOfChorbieswithInvalidOrNullCreditCard);
		result.addObject("getAmountOfChorbiesSearchingForActivities",getAmountOfChorbiesSearchingForActivities);
		result.addObject("getAmountOfChorbiesSearchingForFriendship",getAmountOfChorbiesSearchingForFriendship);
		result.addObject("getAmountOfChorbiesSearchingForLove",getAmountOfChorbiesSearchingForLove);
		result.addObject("getChorbiesOrderedByNumberOfReceivedLikes",getChorbiesOrderedByNumberOfReceivedLikes);
		result.addObject("getMinNumberOfLikesPerChorbi",getMinNumberOfLikesPerChorbi);
		result.addObject("getMaxNumberOfLikesPerChorbi",getMaxNumberOfLikesPerChorbi);
		result.addObject("getAvgNumberOfLikesPerChorbi",getAvgNumberOfLikesPerChorbi);
		result.addObject("getMinNumberOfReceivedChirpsPerChorbi",getMinNumberOfReceivedChirpsPerChorbi);
		result.addObject("getMaxNumberOfReceivedChirpsPerChorbi",getMaxNumberOfReceivedChirpsPerChorbi);
		result.addObject("getAvgNumberOfReceivedChirpsPerChorbi",getAvgNumberOfReceivedChirpsPerChorbi);
		result.addObject("getMinNumberOfSentChirpsPerChorbi", getMinNumberOfSentChirpsPerChorbi);
		result.addObject("getMaxNumberOfSentChirpsPerChorbi",getMaxNumberOfSentChirpsPerChorbi);
		result.addObject("getAvgNumberOfSentChirpsPerChorbi",getAvgNumberOfSentChirpsPerChorbi);
		result.addObject("getChorbiesWithTheMostReceivedChirps",getChorbiesWithTheMostReceivedChirps);
		result.addObject("getChorbiesWithTheMostSentChirps",getChorbiesWithTheMostSentChirps);
		
		result.addObject("getManagersOrderedByNumberOfEvents",getManagersOrderedByNumberOfEvents);
		result.addObject("getAllManager",getAllManager);
		result.addObject("getChorbiesOrderedByNumberOfEvents",getChorbiesOrderedByNumberOfEvents);
		result.addObject("getAllChorbies",getAllChorbies);
		result.addObject("getMinNumberOfStars",getMinNumberOfStars);
		result.addObject("getMaxNumberOfStars",getMaxNumberOfStars);
		result.addObject("getAvgNumberOfStars",getAvgNumberOfStars);
		result.addObject("getChorbiesOrderedByAverageLikes",getChorbiesOrderedByAverageLikes);
		
		return result;
	}
	
}