package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import security.UserAccountService;
import services.ActorService;
import services.AdministratorService;
import services.ChorbiService;
import services.ManagerService;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Administrator;
import domain.Chorbi;
import domain.Manager;
import forms.UserAccountForm;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {
	
	// Constructor --------------------------------------
	
	public ActorController() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private UserAccountService userAccountService;
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private ManagerService managerService;
	
	// Edit ---------------------------------------------
	
	@RequestMapping(value = "/ua/edit", method = RequestMethod.GET)
	public ModelAndView userAccountEdit() {
		UserAccount userAccount = LoginService.getPrincipal();
		UserAccountForm uaForm = userAccountService.construct(userAccount);
		ModelAndView result = new ModelAndView("actor/ua/edit");
		result.addObject("userAccount", uaForm);
		result.addObject("actorId", actorService.findActorByUserAccount(userAccount).getId());
		return result;
	}
	
	@RequestMapping(value = "/ua/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView userAccountSave(@ModelAttribute(value = "userAccount")UserAccountForm uaForm,
			BindingResult binding) {
		ModelAndView result;
		
		UserAccount userAccount = userAccountService.reconstruct(uaForm, binding);
		if (binding.hasErrors()) {
			if (!uaForm.getPassword().equals(uaForm.getConfirmPassword())) {
				result = createEditModelAndView(uaForm, "password.dontmatch");
			}
			else {
				result = createEditModelAndView(uaForm);
			}
		}
		else {
			try {
				if (!uaForm.getPassword().equals(uaForm.getConfirmPassword())) {
					result = createEditModelAndView(uaForm, "password.dontmatch");
				}
				else {
					userAccountService.save(userAccount);
					result = profile(null);
				}
			}
			catch (DataIntegrityViolationException excp) {
				// Este caso es un caso especial que s�lo ocurrira cuando el UserAccount que ha puesto el usuario tiene el mismo 
				// username que uno ya existente, entonces se le informar� de esto
				result = createEditModelAndView(uaForm,"userAccount.already.used");
			}
			catch (Throwable oops) {
				result = createEditModelAndView(uaForm, "commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/ua/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@ModelAttribute(value = "userAccount")UserAccountForm uaForm, BindingResult binding) {
		ModelAndView result;
		
		Actor actor = servicesUtils.checkUser();
		try {
			if (actor instanceof Chorbi) {
				chorbiService.delete((Chorbi) actor);
			}
			else if (actor instanceof Administrator) {
				administratorService.delete((Administrator) actor);
			}
			else if (actor instanceof Manager) {
				managerService.delete((Manager) actor);
			}
			result = new ModelAndView("redirect:/j_spring_security_logout");
		}
		catch (Throwable oops) {
			result = createEditModelAndView(uaForm, "commit.error");
		}

		return result;
	}
	
	//Profile ---------------------------------------------------------------

	@RequestMapping(value="/profile", method=RequestMethod.GET)
	public ModelAndView profile(@RequestParam(required = false) Integer actorId) {
		ModelAndView result;
		Actor actor;
		try {
			if (actorId == null) {
				actor = servicesUtils.checkUser();
				if (actor instanceof Chorbi) {
					result = new ModelAndView("redirect:/chorbi/profile.do");
				}
				else if(actor instanceof Administrator) {
					result = new ModelAndView("redirect:/administrator/profile.do");
				}
				else if(actor instanceof Manager) {
					result = new ModelAndView("redirect:/_manager/profile.do");
				}
				else {
					result = new ModelAndView("redirect:/misc/403.do");
				}
			}
			else {
				actor = actorService.findById(actorId);
				if (actor instanceof Chorbi) {
					result = new ModelAndView("redirect:/chorbi/profile.do?chorbiId="+actorId);
				}
				else if (actor instanceof Administrator) {
					result = new ModelAndView("redirect:/administrator/profile.do?administratorId="+actorId);
				}
				else if (actor instanceof Manager) {
					result = new ModelAndView("redirect:/_manager/profile.do?managerId="+actorId);
				}
				else {
					result = new ModelAndView("redirect:/misc/403.do");
				}
			}
		}
		catch (Throwable t) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		return result;
	}
	
	@RequestMapping(value = "/profile/edit", method = RequestMethod.GET)
	public ModelAndView profileEdit() {
		ModelAndView result;
		Actor actor;

		actor = servicesUtils.checkUser();

		if (actor instanceof Chorbi) {
			result = new ModelAndView("chorbi/edit");
			result.addObject("chorbi", chorbiService.construct((Chorbi) actor));
		}
		else if (actor instanceof Administrator) {
			result = new ModelAndView("administrator/edit");
			result.addObject("administrator", administratorService.construct((Administrator)actor));
		}
		else if (actor instanceof Manager) {
			result = new ModelAndView("manager/edit");
			result.addObject("manager", managerService.construct((Manager)actor));
		}
		else {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}
	
	// Ancillary Methods
	// ----------------------------------------------------------
	
	private ModelAndView createEditModelAndView(UserAccountForm uaForm) {
		return createEditModelAndView(uaForm, null);
	}
	
	private ModelAndView createEditModelAndView(UserAccountForm uaForm, String message) {
		ModelAndView result = new ModelAndView("actor/ua/edit");
		result.addObject("userAccount", uaForm);
		result.addObject("actorId", servicesUtils.checkUser().getId());
		result.addObject("message", message);
		return result;
	}
	

}
