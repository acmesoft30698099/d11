
package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.ConfigurationService;
import domain.Configuration;
import domain.Url;
import forms.ConfigurationForm;

@Controller
@RequestMapping("/configuration")
public class ConfigurationController extends AbstractController {

	// Constructor -------------------------------------

	public ConfigurationController() {
		super();
	}


	// Services ----------------------------------------

	@Autowired
	private ConfigurationService	configurationService;

	@Autowired
	private ChorbiService			chorbiService;


	// Profile -----------------------------------------

	@RequestMapping("/profile")
	public ModelAndView profile(@RequestParam(required = false) boolean showIncome) {
		ModelAndView result;
		Configuration configuration;

		configuration = configurationService.findConfiguration();

		result = new ModelAndView("configuration/profile");

		result.addObject("configuration", configuration);
		result.addObject("requestURI", "configuration/profile.do");

		return result;
	}

	// Edit ---------------------------------------------

	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		Configuration configuration;
		ConfigurationForm configurationForm;

		try {
			configuration = configurationService.findConfiguration();
			configurationForm = configurationService.construct(configuration);

			result = createEditModelAndView(configurationForm);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Save ----------------------------------------------

	@RequestMapping(value = "edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value = "configuration") ConfigurationForm configurationForm, BindingResult binding) {
		ModelAndView result;
		Configuration configuration;

		try {
			configuration = configurationService.reconstruct(configurationForm, binding);
			if (binding.hasErrors()) {
				result = createEditModelAndView(configurationForm);
			} else {
				try {
					configuration = configurationService.save(configuration);
					result = new ModelAndView("redirect:/configuration/profile.do");
				} catch (Throwable excp) {
					result = createEditModelAndView(configurationForm, "commit.error");
				}
			}
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}

		return result;
	}

	// Add banner ----------------------------------------

	@RequestMapping(value = "/addBanner", method = RequestMethod.POST)
	public ModelAndView addBanner(@ModelAttribute(value = "configuration") ConfigurationForm configurationForm, BindingResult binding) {
		ModelAndView result;

		configurationForm.getBanners().add(new Url());

		result = createEditModelAndView(configurationForm);

		return result;
	}

	// Remove banner -------------------------------------

	@RequestMapping(value = "/removeBanner", method = RequestMethod.POST)
	public ModelAndView removeBanner(@ModelAttribute(value = "configuration") ConfigurationForm configurationForm, @RequestParam(required = true) int bannerIndex, BindingResult binding) {
		ModelAndView result;

		configurationForm.getBanners().remove(bannerIndex);

		result = createEditModelAndView(configurationForm);

		return result;
	}

	// Show monthly income

	@RequestMapping(value = "showIncome", method = RequestMethod.GET)
	public ModelAndView showIncome() {
		ModelAndView result = new ModelAndView("configuration/income");

		result.addObject("monthlyIncome", configurationService.getMensualIncomeFromChorbisFee());
		result.addObject("requestURI", "configuration/showIncome");
		result.addObject("chorbies", chorbiService.findAll());

		return result;
	}

	// Ancillary methods ---------------------------------

	private ModelAndView createEditModelAndView(ConfigurationForm configurationForm) {
		return createEditModelAndView(configurationForm, null);
	}

	private ModelAndView createEditModelAndView(ConfigurationForm configurationForm, String message) {
		ModelAndView result;

		result = new ModelAndView("configuration/edit");

		result.addObject("configuration", configurationForm);
		result.addObject("message", message);

		return result;
	}
}
