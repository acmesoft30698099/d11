package controllers.utils;

import java.util.Calendar;
import java.util.Date;

import org.displaytag.decorator.TableDecorator;

import domain.Event;

/**
 * Esta clase cambiar� el estilo de la tabla de eventos a los necesarios para resaltarlos
 * @author Student
 *
 */
public class EventTableDecorator extends TableDecorator {
	
	// Constructor -----------------------------------
	
	public EventTableDecorator() {
		super();
	}
	
	// Methods ---------------------------------------
	
	public String addRowClass() {
		Event event = (Event) getCurrentRowObject();
		Calendar currentDate = Calendar.getInstance();
		String className;
		if(event.getDate().before(currentDate.getTime())) {
			className = "pastEvent";
		} else {
			currentDate.setTime(new Date());
			currentDate.add(Calendar.MONTH, 1);
			if(event.getAvailableSeats() > 0 && event.getDate().before(currentDate.getTime())) {
				className = "recentEvent";
			} else {
				className = "normalEvent";
			}
		}
		return className;
	}

}
