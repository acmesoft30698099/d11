package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.FinderService;
import services.utils.ServicesUtils;
import domain.Chorbi;
import domain.Coordinate;
import domain.Finder;
import forms.FinderForm;

@Controller
@RequestMapping("/finder")
public class FinderController extends AbstractController {
	
	// Services --------------------------------------
	
	@Autowired
	private FinderService finderService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Constructor -----------------------------------
	
	public FinderController() {
		super();
	}
	
	// Show finder -----------------------------------
	
	@RequestMapping(value="/profile", method=RequestMethod.GET)
	public ModelAndView profile() {
		ModelAndView result;
		
		try {
			Finder finder;
			Collection<Chorbi> finderResults = new ArrayList<Chorbi>();
			Chorbi user = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
			
			finder = finderService.findOneForEdit(user.getFinder().getId());
			
			result = new ModelAndView("finder/profile");
			
			try{
				finderResults = finderService.getResultsOfFinder(finder);
			}catch (IllegalArgumentException e){
				result.addObject("invalid", true);
			}
			
			result.addObject("edit",true); // El usuario registrado siempre ser� el propietario
			result.addObject("finder",finder);
			result.addObject("finderResults",finderResults);
		} catch (Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Edit GET --------------------------------------
	
	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		
		try {
			FinderForm finder;
			Chorbi user = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
			finder = finderService.construct(finderService.findOneForEdit(user.getFinder().getId()));
			
			result = createEditModelAndView(finder);
		} catch(Throwable excp) {
			result = new ModelAndView("redirect:/misc/403.do");
		}
		
		return result;
	}
	
	// Edit save -------------------------------------
	
	@RequestMapping(value="/edit",method=RequestMethod.POST,params="save")
	public ModelAndView save(@ModelAttribute(value="finder") FinderForm finder, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()) {
			result = createEditModelAndView(finder);
		} else {
			try {
				Finder f = finderService.reconstruct(finder, binding);
				if(binding.hasErrors()) {
					result = createEditModelAndView(finder);
				} else {
					try {
						finderService.save(f);
						result = new ModelAndView("redirect:profile.do");
					} catch(Throwable excp) {
						if(excp instanceof IllegalArgumentException && excp.getMessage().contains("businessRule")) {
							result = createEditModelAndView(finder, excp.getMessage());
						} else {
							result = createEditModelAndView(finder, "commit.error");
						}
					}
				}
			} catch (Throwable excp) {
				result = new ModelAndView("redirect:/misc/403.do");
			}
		}
		
		return result;
	}
	
	// Coordinates handling --------------------------------------
	
	@RequestMapping(value = "removeCoordinates", method = RequestMethod.POST)
	public ModelAndView removeCoordinates(
			@ModelAttribute(value = "finderForm") FinderForm finderForm,
			BindingResult binding,
			@RequestParam(required=true) int coordinatesIndex) {
		ModelAndView result;

		finderForm.getCoordinates().remove(coordinatesIndex);

		result = createEditModelAndView(finderForm);

		return result;
	}
	
	@RequestMapping(value = "addCoordinates", method = RequestMethod.POST)
	public ModelAndView addCoordinates(
			@ModelAttribute(value = "finderForm") FinderForm finderForm,
			BindingResult binding) {
		ModelAndView result;
		Coordinate coordinate = new Coordinate();

		if (finderForm.getCoordinates() == null) {
			finderForm.setCoordinates(new ArrayList<Coordinate>());
		}

		finderForm.getCoordinates().add(coordinate);

		result = createEditModelAndView(finderForm);

		return result;
	}
	
	// Ancillary methods -----------------------------
	
	private ModelAndView createEditModelAndView(FinderForm finder) {
		ModelAndView result;
		
		result = createEditModelAndView(finder,null);
		
		return result;
	}
	
	private ModelAndView createEditModelAndView(FinderForm finder, String message) {
		ModelAndView result;
		
		result = new ModelAndView("finder/edit");
		result.addObject("finder",finder);
		result.addObject("message",message);
		
		return result;
	}

}
