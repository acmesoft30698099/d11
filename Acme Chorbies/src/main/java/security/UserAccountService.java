package security;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import services.utils.ServicesUtils;

import domain.Actor;
import domain.Administrator;
import forms.UserAccountForm;

@Service
@Transactional
public class UserAccountService {
	
	// Managed repository -----------------------------------------------------
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	// Supporting services ----------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Constructors -----------------------------------------------------------
	
	public UserAccountService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public UserAccount findByActor(final Actor actor) {
		Assert.notNull(actor);
		
		UserAccount result;
		
		result = this.userAccountRepository.findByActorId(actor.getId());
		
		return result;
	}
	
	public UserAccount create() {
		return new UserAccount();
	}
	
	public void changeAuthorities(UserAccount userAccount) {
		// Comprobamos que el usuario actual es Administrator
		servicesUtils.getRegisteredUser(Administrator.class);
		// Se realiza el guardado
		userAccountRepository.save(userAccount);
	}
	
	public UserAccount save(UserAccount ua) {
		UserAccount saved;
		Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
		Assert.notNull(ua);
		// Dividimos el flujo entre edici�n y creaci�n
		if (ua.getId() > 0) {
			// Comprobamos que existe en BD
			UserAccount original = userAccountRepository.findOne(ua.getId());
			Assert.notNull(original);
			// Encriptamos la contrase�a
			ua.setPassword(md5PasswordEncoder.encodePassword(ua.getPassword(), null));
			// Guardamos los cambios
			saved = userAccountRepository.save(ua);
			Assert.notNull(saved);
		}
		else {
			// Asignamos valores por defecto
			ua.setId(0);
			ua.setVersion(0);
			// Encriptamos la contrase�a
			ua.setPassword(md5PasswordEncoder.encodePassword(ua.getPassword(), null));
			// Guardamos el objeto
			saved = userAccountRepository.save(ua);
			Assert.notNull(saved);
		}
		// Retornamos el objeto
		return saved;
	}
	
	// Other business methods -------------------------------------------------
	
	public UserAccount reconstruct(UserAccountForm uaForm, BindingResult binding) {
		UserAccount newUserAccount = new UserAccount();
		UserAccount original = userAccountRepository.findOne(LoginService.getPrincipal().getId());
		
		newUserAccount.setAuthorities(original.getAuthorities());
		newUserAccount.setId(original.getId());
		newUserAccount.setVersion(original.getVersion());
		newUserAccount.setUsername(uaForm.getUsername());
		newUserAccount.setPassword(uaForm.getPassword());
		
		validator.validate(newUserAccount, binding);
		
		return newUserAccount;
	}
	
	public UserAccountForm construct(UserAccount userAccount) {
		UserAccountForm uaForm = new UserAccountForm();
		
		uaForm.setUsername(userAccount.getUsername());
		
		return uaForm;
	}
	
}
