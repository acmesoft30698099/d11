package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chorbi;
import domain.Likes;

@Repository
public interface LikesRepository extends JpaRepository<Likes, Integer> {
	
	@Query("select likes from Likes likes where likes.sender.id = ?1 and likes.receiver.id = ?2")
	Likes hasLikedTheUser(Integer senderId, Integer receiverId);
	
	@Query("select sum(likes.stars) from Chorbi chorbi join chorbi.receivedLikes likes group by chorbi order by sum(likes.stars)")
	Collection<Long> getMinNumberOfLikesPerChorbi();
	
	@Query("select sum(likes.stars) from Chorbi chorbi join chorbi.receivedLikes likes group by chorbi order by sum(likes.stars) desc")
	Collection<Long> getMaxNumberOfLikesPerChorbi();
	
	@Query("select avg(likes.stars) from Likes likes")
	Double getAvgNumberOfLikesPerChorbi();
	
	@Query(value="select id from chorbi as c order by (select avg(stars) from Likes where receiver_id=c.id)",nativeQuery=true)
	Collection<Integer> getChorbiesOrderedByAverageLikes();
	
	@Query("select chorbi from Chorbi chorbi join chorbi.sentLikes likes where likes.receiver.id = ?1")
	Collection<Chorbi> getChorbiesThatLiked(Integer chorbiId);
}
