package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {

	@Query("select manager from Manager manager order by manager.events.size")
	Collection<Manager> getManagersOrderedByNumberOfEvents();
	
	//Se puede obtener ya que fee es un atributo de manager
	
}