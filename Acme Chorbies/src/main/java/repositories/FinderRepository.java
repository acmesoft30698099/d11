package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chorbi;
import domain.Finder;

@Repository
public interface FinderRepository extends JpaRepository<Finder, Integer> {
	
	// TODO Cambiar a array
	@Query("select chorbi from Chorbi chorbi join chorbi.coordinate coordinate where chorbi.gender like ?1 and chorbi.relation like ?2 " +
			"and chorbi.description like concat('%',?3,'%') and (chorbi.age between (?4 - 5) and (?4 + 5))")
	Collection<Chorbi> getResultsOfFinderWithAge(String gender, String relation, String keyword, Integer age);
	
	@Query("select chorbi from Chorbi chorbi join chorbi.coordinate coordinate where chorbi.gender like ?1 and chorbi.relation like ?2 " +
			"and chorbi.description like concat('%',?3,'%')")
	Collection<Chorbi> getResultsOfFinderWithoutAge(String gender, String relation, String keyword);
	
	@Query("select finder from Finder finder join finder.cachedChorbies chorbi where chorbi.id = ?1")
	Collection<Finder> getFindersThatHasCachedThisChorbi(Integer chorbiId);

}