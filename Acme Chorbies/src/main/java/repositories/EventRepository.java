package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chorbi;
import domain.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer>{
	
	@Query("select event from Event event where date(event.date) < ?1 and date(event.date) > CURRENT_DATE and event.availableSeats > 0")
	Collection<Event> getEventsBeforeDateAndWithAvailableSeats(Date date);
	
	@Query("select event from Event event where date(event.date) < CURRENT_DATE")
	Collection<Event> getPastEvents();
	
	@Query("select event.chorbies from Event event where event.id = ?1")
	Collection<Chorbi> getRegisteredChorbiesFromEvent(Integer id);
	
}
