package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ChorbiRepository;
import security.Authority;
import security.UserAccount;
import security.UserAccountService;
import services.utils.ServicesUtils;
import domain.Administrator;
import domain.Chirp;
import domain.Chorbi;
import domain.Event;
import domain.Finder;
import domain.Likes;
import forms.ChorbiForm;
import forms.UserAccountForm;

@Service
@Transactional
public class ChorbiService extends GenericService<ChorbiRepository, Chorbi> {
	
	// Supporting services ----------------------------------------------------

	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private UserAccountService userAccountService;
	
	@Autowired
	private FinderService finderService;
	
	@Autowired
	private EventService eventService;
	
	// Constructors -----------------------------------------------------------

	public ChorbiService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Chorbi create() {
		Chorbi created = new Chorbi();
		created.setReceivedChirps(new ArrayList<Chirp>());
		created.setReceivedLikes(new ArrayList<Likes>());
		created.setSentChirps(new ArrayList<Chirp>());
		created.setSentLikes(new ArrayList<Likes>());
		created.setUserAccount(new UserAccount());
		return created;
	}
	
	@Override
	public Chorbi reglaNegocioSave(Chorbi chorbi) {
		Assert.notNull(chorbi);
		Assert.isTrue(chorbi.getAge() >= 18, "bussinessRule.IncorrectAge");
		// Guardamos la UserAccount si se trata de un registro de nuevo usuario
		if (chorbi.getId() <= 0) {
			Assert.notNull(chorbi.getUserAccount());
			UserAccount ua = userAccountService.save(chorbi.getUserAccount());
			chorbi.setUserAccount(ua);
		}
		return chorbi;
	}
	
	@Override
	public Chorbi reglaNegocioDelete(Chorbi chorbi) {
		Chorbi registered;
		Assert.notNull(chorbi);
		Assert.isTrue(chorbi.getId() > 0);
		// Obtenemos el original y comprobamos que el usuario registrado sea el
		// propietario de la cuenta
		chorbi = repository.findOne(chorbi.getId());
		Assert.notNull(chorbi);
		registered = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
		Assert.notNull(registered);
		Assert.isTrue(registered.getId() == chorbi.getId());
		// De registro de los eventos registrados
		for(Event event : chorbi.getEvents()) {
			eventService.unregisterToEvent(chorbi.getId(), event.getId());
			eventService.flush();
		}
		// Eliminaci�n de la cach� de los finders.
		Collection<Finder> finders = finderService.getFindersThatHasCachedThisChorbi(chorbi);
		for(Finder finder : finders) {
			finder.getCachedChorbies().remove(chorbi);
		}
		return registered;
	}
	
	@Override
	public Chorbi reglaNegocioEdit(Chorbi chorbi){
		Assert.notNull(chorbi);
		Chorbi registered = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
		Assert.notNull(registered);
		Assert.isTrue(registered.getId() == chorbi.getId());
		return chorbi;
	}
	
	// Other business methods -------------------------------------------------
	
	public Collection<Object[]> getNumberOfChorbiesByTheirCountryAndCity(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getNumberOfChorbiesByTheirCountryAndCity();
	}
	
	public Integer getMaxAgeOfChorbies(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMaxAgeOfChorbies();
	}
	
	public Integer getMinAgeOfChorbies(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMinAgeOfChorbies();
	}
	
	public Double getAvgAgeOfChorbies(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getAvgAgeOfChorbies();
	}
	
	public Long getAmountOfChorbieswithInvalidOrNullCreditCard(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getAmountOfChorbieswithInvalidOrNullCreditCard();
	}
	
	public Long getAmountOfChorbiesSearchingFor(String relation){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getAmountOfChorbiesSearchingFor(relation);
	}
	
	public Collection<Chorbi> getChorbiesOrderedByNumberOfReceivedLikes(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getChorbiesOrderedByNumberOfReceivedLikes();
	}
	
	public Integer getMinNumberOfReceivedChirpsPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMinNumberOfReceivedChirpsPerChorbi();
	}
	
	public Integer getMaxNumberOfReceivedChirpsPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMaxNumberOfReceivedChirpsPerChorbi();
	}
	
	public Double getAvgNumberOfReceivedChirpsPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getAvgNumberOfReceivedChirpsPerChorbi();
	}
	
	public Integer getMinNumberOfSentChirpsPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMinNumberOfReceivedChirpsPerChorbi();
	}
	
	public Integer getMaxNumberOfSentChirpsPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMaxNumberOfReceivedChirpsPerChorbi();
	}
	
	public Double getAvgNumberOfSentChirpsPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getAvgNumberOfReceivedChirpsPerChorbi();
	}
	
	public Collection<Chorbi> getChorbiesWithTheMostReceivedChirps(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getChorbiesWithTheMostReceivedChirps();
	}
	
	public Collection<Chorbi> getChorbiesWithTheMostSentChirps(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getChorbiesWithTheMostSentChirps();
	}
	
	public Integer getMinNumberOfLikesPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMinAgeOfChorbies();
	}
	
	public Integer getMaxNumberOfLikesPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getMaxAgeOfChorbies();
	}
	
	public Double getAvgNumberOfLikesPerChorbi(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getAvgAgeOfChorbies();
	}
	
	public Chorbi reconstruct(ChorbiForm actorForm, BindingResult binding) {
		Chorbi newChorbi = create();
		if (actorForm.getId() > 0) {
			Chorbi original = findOneForEdit(actorForm.getId());
			
			newChorbi.setId(original.getId());
			newChorbi.setVersion(original.getVersion());
			
			newChorbi.setGender(original.getGender());
			newChorbi.setBirthDate(original.getBirthDate());
			newChorbi.setFinder(original.getFinder());
			newChorbi.setSentChirps(original.getReceivedChirps());
			newChorbi.setReceivedChirps(original.getReceivedChirps());
			newChorbi.setSentLikes(original.getReceivedLikes());
			newChorbi.setReceivedLikes(original.getReceivedLikes());
			newChorbi.setUserAccount(original.getUserAccount());
		}
		else {
			UserAccount ua = userAccountService.create();
			Collection<Authority> authorities = new ArrayList<Authority>();
			Authority authority = new Authority();
			authority.setAuthority(Authority.CHORBI);
			authorities.add(authority);
			ua.setAuthorities(authorities);
			
			ua.setUsername(actorForm.getUserAccount().getUsername());
			ua.setPassword(actorForm.getUserAccount().getPassword());
			newChorbi.setUserAccount(ua);
			
			newChorbi.setGender(actorForm.getGender());
			newChorbi.setBirthDate(actorForm.getBirthDate());
			
			if(newChorbi.getAge() < 18) {
				binding.rejectValue("birthDate", "bussinessRule.IncorrectAge");
			}
			
			Finder finder = finderService.create();
			finder.setChorbi(newChorbi);
			newChorbi.setFinder(finder);
		}

		newChorbi.setName(actorForm.getName());
		newChorbi.setSurname(actorForm.getSurname());
		newChorbi.setPhone(actorForm.getPhone());
		newChorbi.setEmail(actorForm.getEmail());
		newChorbi.setImage(actorForm.getImage());
		newChorbi.setRelation(actorForm.getRelation());
		newChorbi.setDescription(actorForm.getDescription());
		newChorbi.setCoordinate(actorForm.getCoordinate());

		validator.validate(newChorbi, binding);

		return newChorbi;
	}
	
	public ChorbiForm construct(Chorbi chorbi) {
		ChorbiForm actorForm = new ChorbiForm();

		actorForm.setId(chorbi.getId());

		actorForm.setName(chorbi.getName());
		actorForm.setSurname(chorbi.getSurname());
		actorForm.setPhone(chorbi.getPhone());
		actorForm.setEmail(chorbi.getEmail());
		actorForm.setImage(chorbi.getImage());
		actorForm.setRelation(chorbi.getRelation());
		actorForm.setDescription(chorbi.getDescription());
		actorForm.setGender(chorbi.getGender());
		actorForm.setBirthDate(chorbi.getBirthDate());
		actorForm.setCoordinate(chorbi.getCoordinate());
		
		actorForm.setUserAccount(new UserAccountForm());

		return actorForm;
	}
	
	//QUERIES
	
	public Collection<Chorbi> getChorbiesOrderedByNumberOfEvents(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getChorbiesOrderedByNumberOfEvents();
	}
	
}