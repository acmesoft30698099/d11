package services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import repositories.ActorRepository;
import security.UserAccount;
import services.utils.ServicesUtils;
import domain.Actor;

@Service
@Transactional
public class ActorService extends GenericService<ActorRepository, Actor> {

	// Supporting services ----------------------------------------------------

	@Autowired
	private ServicesUtils servicesUtils;

	// Constructors -----------------------------------------------------------

	public ActorService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------

	// Other business methods -------------------------------------------------

	/**
	 * Encuentra el usuario a trav�s del UserAccount aportado.
	 * @param user El UserAccount del usuario
	 * @return El Actor correspondiente.
	 */
	public Actor findActorByUserAccount(UserAccount user){
		return repository.findByUserAccount(user.getId());
	}
	
	/**
	 * M�todo alternativo para encontrar Actor ya que el m�todo findOne puede dar problemas al tratar
	 * con clases abstractas
	 * @param id
	 * @return
	 */
	public Actor findById(Integer id) {
		servicesUtils.checkId(id);
		return repository.findById(id);
	}
	
}
