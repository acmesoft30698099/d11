package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import domain.Chorbi;
import domain.Coordinate;
import domain.CreditCard;
import domain.Finder;
import forms.FinderForm;
import repositories.FinderRepository;
import services.utils.ServicesUtils;

@Service
@Transactional
public class FinderService extends GenericService<FinderRepository, Finder>{
	
	// Constructor ---------------------------------
	
	public FinderService() {
		super();
	}
	
	// Services ------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private CreditCardService creditCardService;

	// CRUD Methods --------------------------------
	
	public Finder create() {
		Finder finder = new Finder();
		finder.setCachedChorbies(new ArrayList<Chorbi>());
		finder.setCoordinates(new ArrayList<Coordinate>());
		finder.setCachedDate(new Date());
		
		return finder;
	}
	
	public Finder reglaNegocioEdit(Finder finder) {
		Chorbi user = servicesUtils.getRegisteredUser(Chorbi.class);
		// Comprobamos que el usuario actual es el poseedor del Finder
		Assert.isTrue(finder.getChorbi().getId() == user.getId());
		
		return finder;
	}
	
	public Finder reglaNegocioSave(Finder finder) {
		// Volvemos a comprobar los permisos del usuario actual
		Chorbi user = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(finder.getChorbi().getId() == user.getId());
		// Cambiamos la fecha de cache del Finder, de forma que siempre se actulice
		finder.setCachedDate(new Date(0));
		// Actualizamos la cach� del Finder a guardar.
		// Siempre que est� validad la tarjeta de cr�dito:
		// Actualizamos el estado de la tarjeta
		CreditCard creditCard = user.getCreditCard();
		if(creditCard != null) {
			creditCard = creditCardService.save(creditCard);
			if(creditCardService.validCreditCard(creditCard)) {
				getResultsOfFinder(finder);
			}
		}
		return finder;
	}
	
	public Finder reglaNegocioDelete(Finder finder) {
		// No deber�a ocurrir nunca, ya que todos los usuarios tienen un Finder y no es eliminable.
		
		return finder;
	}
	
	// Other methods ---------------------------------
	
	public Collection<Chorbi> getResultsOfFinder(Finder finder) {
		Collection<Chorbi> result;
		// Comprobamos la cach�
		Calendar currentTime = Calendar.getInstance();
		Calendar finderCachedDate = Calendar.getInstance();
		finderCachedDate.setTime(finder.getCachedDate());
		// Obtenemos la fecha de la configuraci�n
		Calendar configurationCacheTime = Calendar.getInstance();
		configurationCacheTime.setTime(configurationService.findConfiguration().getCacheTimer());
		// Sumamos las horas, minutos y segundos
		finderCachedDate.add(Calendar.HOUR_OF_DAY, configurationCacheTime.get(Calendar.HOUR_OF_DAY));
		finderCachedDate.add(Calendar.MINUTE, configurationCacheTime.get(Calendar.MINUTE));
		finderCachedDate.add(Calendar.SECOND, configurationCacheTime.get(Calendar.SECOND));
		// Comprobamos si la fecha es posterior
		if(currentTime.after(finderCachedDate)) {
			// Si es el caso, debemos realizar el cacheado.
			finder.setCachedChorbies(searchWithFinder(finder));
			// Fijamos la fecha de cacheado a la actual
			finder.setCachedDate(new Date());
			result = finder.getCachedChorbies();
		}
		// Finalmente, obtenemos los datos cacheados
		result = new ArrayList<Chorbi>(finder.getCachedChorbies());
		return result;
	}
	
	public Collection<Finder> getFindersThatHasCachedThisChorbi(Chorbi chorbi) {
		return repository.getFindersThatHasCachedThisChorbi(chorbi.getId());
	}
	
	public Collection<Chorbi> searchWithFinder(Finder finder) {
		Collection<Chorbi> chorbies;
		// Comprobamos la tarjeta de cr�dito, para comprobar que tiene los permisos.
		Chorbi user = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(creditCardService.validCreditCard(user.getCreditCard()),"businessRule.credit.card.invalid");
		if(finder.getAge() == null) {
			String gender = finder.getGender()==null||finder.getGender().isEmpty()?"%%":finder.getGender();
			String relation = finder.getRelation()==null||finder.getRelation().isEmpty()?"%%":finder.getRelation();
			String keyword = finder.getKeyword()==null?"%":finder.getKeyword();
			chorbies = repository.getResultsOfFinderWithoutAge(gender, relation, keyword);
		} else {
			String gender = finder.getGender()==null||finder.getGender().isEmpty()?"%%":finder.getGender();
			String relation = finder.getRelation()==null||finder.getRelation().isEmpty()?"%%":finder.getRelation();
			String keyword = finder.getKeyword()==null?"%%":finder.getKeyword();
			chorbies = repository.getResultsOfFinderWithAge(gender, relation, keyword, finder.getAge());
		}
		Collection<Chorbi> unfilteredChorbies = new ArrayList<Chorbi>(chorbies);
		if(finder.getCoordinates() != null) {
			for(Chorbi chorbi : unfilteredChorbies) {
				for(Coordinate coordinate : finder.getCoordinates()) {
					if(!(chorbi.getCoordinate().getCity().equals(coordinate.getCity()) &&
							((chorbi.getCoordinate().getCountry() == null || coordinate.getCountry() == null) || chorbi.getCoordinate().getCountry().equals(coordinate.getCountry())) &&
							((chorbi.getCoordinate().getProvince() == null  || coordinate.getProvince() == null) || chorbi.getCoordinate().getProvince().equals(coordinate.getProvince())) &&
							((chorbi.getCoordinate().getState() == null || coordinate.getState() == null) || chorbi.getCoordinate().getState().equals(coordinate.getState())))) {
						chorbies.remove(chorbi);
					}
				}
			}
		}
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		chorbies.remove(chorbi);
		return chorbies;
	}
	
	public FinderForm construct(Finder finder) {
		FinderForm finderForm = new FinderForm();
		
		finderForm.setId(finder.getId());
		finderForm.setAge(finder.getAge());
		finderForm.setCoordinates(new ArrayList<Coordinate>(finder.getCoordinates()));
		finderForm.setGender(finder.getGender());
		finderForm.setKeyword(finder.getKeyword());
		finderForm.setRelation(finder.getRelation());
		
		return finderForm;
	}
	
	public Finder reconstruct(FinderForm finderForm, BindingResult binding) {
		Finder finder = create();
		// Nunca se crear� un Finder con un formulario, por lo tanto siempre obtenemos el Finder de la BD.
		Finder original = findOne(finderForm.getId());
		
		// Asignamos los valores no mutables
		finder.setId(original.getId());
		finder.setVersion(original.getVersion());
		finder.setCachedChorbies(original.getCachedChorbies());
		finder.setCachedDate(original.getCachedDate());
		finder.setChorbi(original.getChorbi());
		
		// Asignamos los valores mutables, del formulario
		finder.setAge(finderForm.getAge());
		if(finderForm.getCoordinates() != null) {
			for(Coordinate coordinate : finderForm.getCoordinates()) {
				if(coordinate.getCountry().isEmpty()) {
					coordinate.setCountry(null);
				}
				if(coordinate.getProvince().isEmpty()) {
					coordinate.setProvince(null);
				}
				if(coordinate.getState().isEmpty()) {
					coordinate.setState(null);
				}
			}
		}
		finder.setCoordinates(finderForm.getCoordinates());
		finder.setGender(finderForm.getGender());
		if(finderForm.getKeyword() != ""){
			finder.setKeyword(finderForm.getKeyword());
		} else {
			finder.setKeyword(null);
		}
		finder.setRelation(finderForm.getRelation());
		
		return finder;
	}
}
