package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ManagerRepository;
import security.Authority;
import security.UserAccount;
import security.UserAccountService;
import services.utils.ServicesUtils;
import domain.Administrator;
import domain.CreditCard;
import domain.Event;
import domain.Manager;
import forms.CreditCardForm;
import forms.ManagerForm;
import forms.UserAccountForm;

@Service
@Transactional
public class ManagerService extends GenericService<ManagerRepository, Manager> {
	
	// Supporting services ----------------------------------------------------

	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private UserAccountService userAccountService;
	
	@Autowired
	private CreditCardService creditCardService;
	
	// Constructors -----------------------------------------------------------

	public ManagerService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Manager create() {
		Manager created = new Manager();
		created.setUserAccount(new UserAccount());
		created.setEvents(new ArrayList<Event>());
		created.setCreditCard(creditCardService.create());
		return created;
	}
	
	@Override
	public Manager reglaNegocioSave(Manager manager) {
		Assert.notNull(manager);
		Assert.notNull(manager.getCreditCard());
		Assert.isTrue(creditCardService.validCreditCard(manager.getCreditCard()),"businessRule.invalid.creditCard");
		CreditCard cc = creditCardService.save(manager.getCreditCard());
		manager.setCreditCard(cc);
		// Guardamos la UserAccount si se trata de un registro de nuevo usuario
		if (manager.getId() <= 0) {
			Assert.notNull(manager.getUserAccount());
			UserAccount ua = userAccountService.save(manager.getUserAccount());
			manager.setUserAccount(ua);
		}
		return manager;
	}
	
	@Override
	public Manager reglaNegocioDelete(Manager manager) {
		Manager registered;
		Assert.notNull(manager);
		Assert.isTrue(manager.getId() > 0);
		// Obtenemos el original y comprobamos que el usuario registrado sea el
		// propietario de la cuenta
		manager = repository.findOne(manager.getId());
		Assert.notNull(manager);
		registered = servicesUtils.checkRegistered(Manager.class, servicesUtils.checkUser());
		Assert.notNull(registered);
		Assert.isTrue(registered.getId() == manager.getId());

		return registered;
	}
	
	@Override
	public Manager reglaNegocioEdit(Manager manager){
		Assert.notNull(manager);
		Manager registered = servicesUtils.checkRegistered(Manager.class, servicesUtils.checkUser());
		Assert.notNull(registered);
		Assert.isTrue(registered.getId() == manager.getId());
		return manager;
	}
	
	public Manager reconstruct(ManagerForm actorForm, BindingResult binding) {
		Manager newManager = create();
		if (actorForm.getId() > 0) {
			Manager original = findOneForEdit(actorForm.getId());
			
			newManager.setId(original.getId());
			newManager.setVersion(original.getVersion());
			
			newManager.setCreditCard(original.getCreditCard());
			newManager.setEvents(original.getEvents());
			newManager.setFee(original.getFee());
			newManager.setUserAccount(original.getUserAccount());
		}
		else {
			UserAccount ua = userAccountService.create();
			Collection<Authority> authorities = new ArrayList<Authority>();
			Authority authority = new Authority();
			authority.setAuthority(Authority.MANAGER);
			authorities.add(authority);
			ua.setAuthorities(authorities);
			
			ua.setUsername(actorForm.getUserAccount().getUsername());
			ua.setPassword(actorForm.getUserAccount().getPassword());
			newManager.setUserAccount(ua);
			
			CreditCard creditCard = creditCardService.reconstruct(actorForm.getCreditCard(), binding);
			newManager.setCreditCard(creditCard);
		}

		newManager.setName(actorForm.getName());
		newManager.setSurname(actorForm.getSurname());
		newManager.setPhone(actorForm.getPhone());
		newManager.setEmail(actorForm.getEmail());
		newManager.setCompanyName(actorForm.getCompanyName());
		newManager.setVATNumber(actorForm.getVATNumber());

		validator.validate(newManager, binding);

		return newManager;
	}
	
	public ManagerForm construct(Manager manager) {
		ManagerForm actorForm = new ManagerForm();

		actorForm.setId(manager.getId());

		actorForm.setName(manager.getName());
		actorForm.setSurname(manager.getSurname());
		actorForm.setPhone(manager.getPhone());
		actorForm.setEmail(manager.getEmail());
		actorForm.setCompanyName(manager.getCompanyName());
		actorForm.setVATNumber(manager.getVATNumber());
		
		CreditCardForm cred = creditCardService.construct(manager.getCreditCard());
		actorForm.setCreditCard(cred);
		
		actorForm.setUserAccount(new UserAccountForm());

		return actorForm;
	}
	
	//QUERIES
	
	public Collection<Manager> getManagersOrderedByNumberOfEvents(){
		servicesUtils.getRegisteredUser(Administrator.class);
		return repository.getManagersOrderedByNumberOfEvents();
	}
	
}