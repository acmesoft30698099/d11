package services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import domain.Administrator;
import forms.AdministratorForm;
import repositories.AdministratorRepository;
import security.UserAccount;
import services.utils.ServicesUtils;

@Service
@Transactional
public class AdministratorService extends GenericService<AdministratorRepository, Administrator> {
	
	// Supporting services ----------------------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	// Constructors -----------------------------------------------------------

	public AdministratorService() {
		super();
	}
	
	// Simple CRUD methods ----------------------------------------------------
	
	public Administrator create() {
		Administrator administrator = new Administrator();
		administrator.setUserAccount(new UserAccount());
		
		return administrator;
	}
	
	public Administrator reglaNegocioSave(Administrator administrator) {
		// Comprobamos los permisos actuales.
		Administrator registered = servicesUtils.getRegisteredUser(Administrator.class);
		Assert.isTrue(registered.getId() == administrator.getId());
		
		return administrator;
	}
	
	public Administrator reglaNegocioDelete(Administrator administrator) {
		// Lo obtenemos de la BD y entonces lo pasamos como objeto a delete.
		Administrator saved = findOneForEdit(administrator.getId());
		
		return saved;
	}
	
	// Other business methods -------------------------------------------------

	public AdministratorForm construct(Administrator administrator) {
		AdministratorForm adminForm = new AdministratorForm();
		
		adminForm.setId(administrator.getId());
		adminForm.setName(administrator.getName());
		adminForm.setSurname(administrator.getSurname());
		adminForm.setPhone(administrator.getPhone());
		adminForm.setEmail(administrator.getEmail());
		
		return adminForm;
	}
	
	public Administrator reconstruct(AdministratorForm adminForm, BindingResult binding) {
		Administrator administrator = create();
		Administrator original = findOneForEdit(adminForm.getId());
		
		administrator.setId(original.getId());
		administrator.setVersion(original.getVersion());
		administrator.setUserAccount(original.getUserAccount());
		
		administrator.setName(adminForm.getName());
		administrator.setSurname(adminForm.getSurname());
		administrator.setPhone(adminForm.getPhone());
		administrator.setEmail(adminForm.getEmail());
		
		validator.validate(administrator,binding);
		
		return administrator;
	}
	
}