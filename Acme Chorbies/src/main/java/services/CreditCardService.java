package services;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CreditCardRepository;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Chorbi;
import domain.CreditCard;
import domain.Manager;
import forms.CreditCardForm;

@Service
@Transactional
public class CreditCardService extends GenericService<CreditCardRepository, CreditCard>{
	
	// Managed repository -------------------------------
	
	@Autowired
	private CreditCardRepository creditCardRepository;
	
	// Supporting services -------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	// Constructors --------------------------------------
	
	public CreditCardService(){
		super();
	}
		
	// Simple CRUD methods --------------------------------
	
	public CreditCard create(){
		return new CreditCard();
	}
	
	@Override
	public CreditCard reglaNegocioSave(CreditCard creditCard){
		Actor actor = null;
		Assert.notNull(creditCard);
		if(creditCard.getId()>0){
			actor = servicesUtils.checkUser();
			Assert.notNull(actor);
			if(actor instanceof Manager){
				Manager manager = (Manager) actor;
				creditCard.setManager(manager);
			}else{
				Chorbi chorbi = (Chorbi) actor;
				creditCard.setChorbi(chorbi);
			}
		}
		boolean res = validCreditCard(creditCard);
		if(creditCard.getValid()!=res){
			creditCard.setValid(res);
		}
		
		if(actor instanceof Manager) {
			Assert.isTrue(res,"businessRule.invalid.creditCard");
		}
		
		return creditCard;
	}
	
	@Override
	public CreditCard reglaNegocioDelete(CreditCard creditCard) {
		Assert.notNull(creditCard);
		Assert.isTrue(creditCard.getId() > 0);
		// Obtenemos el original y comprobamos que el usuario registrado sea el
		// propietario de la cuenta
		creditCard = creditCardRepository.findOne(creditCard.getId());
		Assert.notNull(creditCard);
		if(creditCard.getManager()!=null){
			Manager manager = servicesUtils.checkRegistered(Manager.class, servicesUtils.checkUser());
			Assert.notNull(manager);
			Assert.isTrue(manager.getId() == creditCard.getManager().getId());
		}else{
			Chorbi chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
			Assert.notNull(chorbi);
			Assert.isTrue(chorbi.getId() == creditCard.getChorbi().getId());
			chorbi.setCreditCard(null);
		}
		return creditCard;
	}
	
	@Override
	public CreditCard reglaNegocioEdit(CreditCard creditCard){
		Assert.notNull(creditCard);
		if(creditCard.getManager()!=null){
			Manager manager = servicesUtils.checkRegistered(Manager.class, servicesUtils.checkUser());
			Assert.notNull(manager);
			Assert.isTrue(manager.getId() == creditCard.getManager().getId());
		}else{
			Chorbi chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
			Assert.notNull(chorbi);
			Assert.isTrue(chorbi.getId() == creditCard.getChorbi().getId());
		}
		return creditCard;
	}
	
	// Other methods --------------------------------
	
	/**
	 * Comprueba la validez del n�mero con el algoritmo de Luhn.
	 * @param number El n�mero de la CreditCard.
	 * @return Boolean indicando si es v�lido o no.
	 */
	public boolean validNumber(String number) {
		Boolean res = false;
		if(number != null) {
			Integer sum = 0;
			int max = number.length()-1;
			Integer iter = 0;
			for(Integer i = max; i >= 0; i--) {
				int num = Character.getNumericValue(number.charAt(i));
				if(iter%2 != 0) {
					num = num*2;
					if(num > 9) {
						num = num - 9;
					}
				}
				iter++;
				sum += num;
			}
			res = sum%10 == 0;
		}
		return res;
	}
	
	/**
	 * Comprueba la validez de la fecha de caducidad de la CreditCard que en el dominio 
	 * de la empresa es posterior a la fecha actual en 7 d�as.
	 * @param month El mes de la tarjeta
	 * @param year El a�o de la tarjeta, se admite 2 o 4 cifras.
	 * @return Boolean que indica si es v�lido o no.
	 */
	public boolean validDate(int month, int year) {
		Calendar cardDate = Calendar.getInstance();
		cardDate.clear();
		if(year <= 100) {
			year = 2000 + year;
		}
		cardDate.set(year, month-1, 1);
		Calendar actualDate = Calendar.getInstance();
		actualDate.add(Calendar.DAY_OF_YEAR,7);
		return cardDate.after(actualDate);
	}
	
	/**
	 * Comprueba la validez de la brand de la creditCard
	 * @param brand El brand de la credit Card
	 * @return Boolean que indica si es v�lido o no.
	 */
	public boolean validBrand(String brand){
		return brand.matches("^(VISA|MASTERCARD|DISCOVER|DINNERS|AMEX)$");
	}
	
	/**
	 * Comprueba la validez de la creditCard
	 * @param creditCard El credit Card
	 * @return Boolean que indica si es v�lido o no.
	 */
	public boolean validCreditCard(CreditCard creditCard){
		boolean res = false;
		if(creditCard != null){
			res = validNumber(creditCard.getNumber())
					&& validDate(creditCard.getExpirationMonth(), creditCard.getExpirationYear())
					&& validBrand(creditCard.getBrand());
		}
		return res;
	}

	public CreditCard reconstruct(CreditCardForm creditCardForm, BindingResult binding) {
		CreditCard newCreditCard = create();
		if (creditCardForm.getId() > 0) {
			CreditCard original = findOneForEdit(creditCardForm.getId());
			
			newCreditCard.setId(original.getId());
			newCreditCard.setVersion(original.getVersion());
			
			newCreditCard.setValid(original.getValid());
			if(original.getManager()!=null){
				newCreditCard.setManager(original.getManager());
			}else{
				newCreditCard.setChorbi(original.getChorbi());
			}
		}
		else {
			newCreditCard.setValid(validCreditCard(newCreditCard));
//			Actor registered = servicesUtils.checkUser();
//			if (registered instanceof Manager) {
//				newCreditCard.setManager(servicesUtils.checkRegistered(
//						Manager.class, servicesUtils.checkUser()));
//			} else {
//				newCreditCard.setChorbi(servicesUtils.checkRegistered(
//						Chorbi.class, servicesUtils.checkUser()));
//			}
		}

		newCreditCard.setHolder(creditCardForm.getHolder());
		newCreditCard.setBrand(creditCardForm.getBrand());
		newCreditCard.setNumber(creditCardForm.getNumber());
		if(creditCardForm.getExpirationMonth() != null) {
			newCreditCard.setExpirationMonth(creditCardForm.getExpirationMonth());
		}
		if(creditCardForm.getExpirationYear() != null) {
			newCreditCard.setExpirationYear(creditCardForm.getExpirationYear());
		}
		if(creditCardForm.getCvv() != null) {
			newCreditCard.setCvv(creditCardForm.getCvv());
		}

		validator.validate(newCreditCard, binding);
		
		return newCreditCard;
	}
	
	public CreditCardForm construct(CreditCard creditCard) {
		CreditCardForm creditCardForm = new CreditCardForm();

		creditCardForm.setId(creditCard.getId());

		creditCardForm.setHolder(creditCard.getHolder());
		creditCardForm.setBrand(creditCard.getBrand());
		creditCardForm.setNumber(creditCard.getNumber());
		creditCardForm.setExpirationMonth(creditCard.getExpirationMonth());
		creditCardForm.setExpirationYear(creditCard.getExpirationYear());
		creditCardForm.setCvv(creditCard.getCvv());

		return creditCardForm;
	}
	
}
