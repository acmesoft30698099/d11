package services;

import java.util.ArrayList;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ChirpRepository;
import services.utils.ServicesUtils;
import domain.Actor;
import domain.Chirp;
import domain.Chorbi;
import domain.Manager;
import domain.Url;
import forms.ChirpForm;
import forms.ChirpFormEvent;

@Service
@Transactional
public class ChirpService extends GenericService<ChirpRepository, Chirp> {
	
	// Constructor ---------------------------------------
	
	public ChirpService() {
		super();
	}
	
	// Services ------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	// CRUD methods --------------------------------------
	
	public Chirp create() {
		Chirp chirp = new Chirp();
		chirp.setAttachments(new ArrayList<Url>());
		chirp.setSentDate(new Date());
		
		return chirp;
	}
	
	public Chirp reglaNegocioGeneral(Chirp chirp) {
		// Comprobamos los permisos, el usuario que lo envie o recibe tiene que estar registrado
		Actor actor = servicesUtils.checkUser();
		// El emisor debe ser o un Manager o un Chorbi.
		Assert.isTrue(actor instanceof Chorbi || actor instanceof Manager);
		Assert.isTrue(actor.getId() == chirp.getSender().getId() ||
				actor.getId() == chirp.getReceiver().getId());
		
		return chirp;
	}
	
	public Chirp reglaNegocioSave(Chirp chirp) {
		// Comprobamos que qui�n lo envia debe ser el usuario actual
		Actor actor = servicesUtils.checkUser();
		Assert.isTrue(actor.getId() == chirp.getSender().getId());
		
		return chirp;
	}
	
	public Chirp reglaNegocioDelete(Chirp chirp) {
		// Comprobamos los permisos, el usuario que lo envie o recibe tiene que estar registrado
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		// En este caso, teniendo en cuenta que Manager enviar� los mensajes sin su consentimiento,
		// no ser� necesario realizar una comprobaci�n de que el usuario es Manager.
		Assert.isTrue(chorbi.getId() == chirp.getSender().getId() ||
				chorbi.getId() == chirp.getReceiver().getId());
		
		return chirp;
	}
	
	// Other methods ------------------------------------------
	
	public ChirpForm construct(Chirp chirp) {
		ChirpForm chirpForm = new ChirpForm();
		
		chirpForm.setText(chirp.getText());
		chirpForm.setSubject(chirp.getSubject());
		chirpForm.setReceiver(chirp.getReceiver());
		chirpForm.setAttachments(new ArrayList<Url>(chirp.getAttachments()));
		
		return chirpForm;
	}
	
	public ChirpForm constructReply(Chirp chirp) {
		ChirpForm chirpForm = new ChirpForm();
		// S�lo los Chorbi tendr�n posiblidad de responder
		Chorbi user = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
		// Comprobamos que el emisor no es Manager
		Assert.isTrue(!(chirp.getSender() instanceof Manager));
		
		chirpForm.setText(chirp.getText());
		chirpForm.setSubject(chirp.getSubject());
		// Sabiendo que el emisor no es Manager, entonces ser� Chorbi
		chirpForm.setReceiver((Chorbi) chirp.getSender());
		chirpForm.setAttachments(new ArrayList<Url>(chirp.getAttachments()));
		
		Assert.isTrue(chirp.getReceiver().getId() == user.getId());
		
		return chirpForm;
	}
	
	public ChirpFormEvent constructBulk(Integer eventId) {
		ChirpFormEvent chirpForm = new ChirpFormEvent();
		
		chirpForm.setAttachments(new ArrayList<Url>());
		chirpForm.setEventId(eventId);
		
		return chirpForm;
	}
	
	public Chirp reconstruct(ChirpForm chirpForm, BindingResult binding) {
		Chirp chirp = create();
		
		// No hay edici�n, por lo tanto se crear� desde cero siempre
		chirp.setSubject(chirpForm.getSubject());
		chirp.setText(chirpForm.getText());
		chirp.setSentDate(new Date());
		chirp.setReceiver(chirpForm.getReceiver());
		chirp.setAttachments(chirpForm.getAttachments());
		// Obtenemos el usuario actual para el sender
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		chirp.setSender(chorbi);
		
		validator.validate(chirp, binding);
		
		return chirp;
	}
	
	public Chirp reconstructReply(ChirpForm original, BindingResult binding, String text) {
		Chirp chirp = create();
		Chorbi chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());

		Chorbi receiver = (Chorbi) original.getReceiver();
		
		chirp.setSubject("[RE] "+original.getSubject());
		chirp.setText(">"+original.getText()+"<"+System.lineSeparator()+text);
		chirp.setAttachments(original.getAttachments());
		chirp.setSentDate(new Date());
		chirp.setReceiver(receiver);
		chirp.setSender(chorbi);
		
		validator.validate(chirp, binding);
		
		return chirp;
	}
	
	public Chirp reconstructForward(Integer chirpId, Integer actorId) {
		Chirp result = create();
		Chorbi chorbi = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
		Chorbi receiver = servicesUtils.checkRegistered(Chorbi.class, actorService.findById(actorId));
		
		// Obtenemos el id del chirp que hace referencia
		Chirp chirp = findOne(chirpId);
		Assert.isTrue(chirp.getSender().getId() == chorbi.getId() ||
				chirp.getReceiver().getId() == chorbi.getId());
		
		result.setReceiver(receiver);
		result.setSender(chorbi);
		result.setAttachments(new ArrayList<Url>(chirp.getAttachments()));
		result.setSentDate(new Date());
		result.setText(chirp.getText());
		result.setSubject("[FWD] "+chirp.getSubject());
		
		return result;
	}
	
	public Chirp reconstructBulk(ChirpFormEvent chirpForm) {
		Chirp chirp = create();
		
		chirp.setAttachments(chirpForm.getAttachments());
		chirp.setText(chirpForm.getText());
		chirp.setSubject(chirpForm.getSubject());
		
		return chirp;
	}
}
