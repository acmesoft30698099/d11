package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.EventRepository;
import services.utils.ServicesUtils;
import domain.Chorbi;
import domain.Configuration;
import domain.Event;
import domain.Manager;
import forms.EventForm;

@Service
@Transactional
public class EventService extends GenericService<EventRepository, Event> {

	// Constructor ------------------------------------------------

	public EventService() {
		super();
	}

	// Services ---------------------------------------------------

	@Autowired
	private ServicesUtils servicesUtils;

	@Autowired
	private CreditCardService creditCardService;

	@Autowired
	private Validator validator;

	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private ConfigurationService configurationService;

	// CRUD Methods -----------------------------------------------

	public Event create() {
		Event event = new Event();
		event.setChorbies(new ArrayList<Chorbi>());
		event.setDate(new Date());
		event.setNumberSeats(1);

		return event;
	}

	public Event reglaNegocioSave(Event event) {
		// Primero, comprobamos los permisos del usuario
		Manager manager = servicesUtils.getRegisteredUser(Manager.class);
		// Despues, comprobamos la validez de su tarjeta de cr�dito
		Assert.isTrue(
				creditCardService.validCreditCard(manager.getCreditCard()),
				"businessRule.invalidCreditCard");
		// "Cargamos" el pago de la fee en su cuenta.
		if(event.getId() == 0) {
			Configuration configuration = configurationService.findConfiguration();
			manager.setFee(manager.getFee() + configuration.getManagerFee());
		}

		return event;
	}

	public Event reglaNegocioGeneral(Event event) {
		// Obtenemos el usuario
		Manager manager = servicesUtils.getRegisteredUser(Manager.class);
		// Comprobamos que posea el Event
		Assert.isTrue(event.getManager().getId() == manager.getId());

		return event;
	}

	// Other methods ----------------------------------------------

	public Event registerToEvent(Integer chorbiId, Integer eventId) {
		// Comprobamos que existe en la BD
		Assert.isTrue(eventId > 0);
		Event event = findOne(eventId);
		// Comprobamos que existen asientos libres
		Assert.isTrue(event.getAvailableSeats() > 0,"businessRule.not.available.seats");
		// Comprobamos la fecha del evento
		Assert.isTrue(event.getDate().after(new Date()));
		// Obtenemos el chorbi
		Chorbi chorbi = chorbiService.findOneForEdit(chorbiId);
		// Comprobamos que no se ha registrado ya
		Assert.isTrue(!event.getChorbies().contains(chorbi),
				"businessRule.already.registered");
		// Comprobamos que tenga tarjeta de cr�dito y que �sta es v�lida
		Assert.isTrue(chorbi.getCreditCard() != null, "businessRule.register.creditCard");
		Assert.isTrue(creditCardService.validCreditCard(chorbi.getCreditCard()), "businessRule.register.creditCard");
		// A�adimos el chorbi a los registrados
		event.getChorbies().add(chorbi);
		// Reducimos los asientos libres
		event.setAvailableSeats(event.getAvailableSeats() - 1);
		event = repository.save(event);
		// Actualizamos el contador de chorbi:
		Configuration configuration = configurationService.findConfiguration();
		chorbi.setFee(chorbi.getFee() + configuration.getChorbiFee());
		return event;
	}

	public Event unregisterToEvent(Integer chorbiId, Integer eventId) {
		// Comprobamos que existe en la BD
		Assert.isTrue(eventId > 0);
		Event event = findOne(eventId);
		// Obtenemos el chorbi
		Chorbi chorbi = chorbiService.findOneForEdit(chorbiId);
		// Comprobamos que se ha registrado ya
		Assert.isTrue(event.getChorbies().contains(chorbi),
				"businessRule.not.registered");
		// Quitamos el chorbi a los registrados
		event.getChorbies().remove(chorbi);
		// Incrementamos los asientos libres
		event.setAvailableSeats(event.getAvailableSeats() + 1);
		event = repository.save(event);
		return event;
	}

	public Collection<Event> getPastEvents() {
		return repository.getPastEvents();
	}

	public Collection<Event> getAvailableEventsOfOneMonth() {
		// Obtenemos la fecha actual
		Calendar calendar = Calendar.getInstance();
		// Restamos uno el mes
		calendar.add(Calendar.MONTH, 1);
		// Efectuamos la b�squeda con el m�todo de fecha
		return repository.getEventsBeforeDateAndWithAvailableSeats(calendar
				.getTime());
	}
	
	public Collection<Chorbi> getRegisteredChorbiesFromEvent(Event event) {
		return repository.getRegisteredChorbiesFromEvent(event.getId());
	}

	public EventForm construct(Event event) {
		EventForm eventForm = new EventForm();

		eventForm.setDate(event.getDate());
		eventForm.setDescription(event.getDescription());
		eventForm.setId(event.getId());
		eventForm.setImage(event.getImage());
		eventForm.setNumberSeats(event.getNumberSeats());
		eventForm.setTitle(event.getTitle());

		return eventForm;
	}

	public Event reconstruct(EventForm eventForm, BindingResult binding) {
		Event event = create();

		if (eventForm.getId() > 0) {
			Event original = findOneForEdit(eventForm.getId());

			event.setId(original.getId());
			event.setVersion(original.getVersion());
			event.setChorbies(original.getChorbies());
			int occupiedSeats = original.getChorbies().size();
			if (occupiedSeats > eventForm.getNumberSeats()) {
				binding.rejectValue("numberSeats",
						"businessRule.numberSeat.invalid");
			}
			event.setAvailableSeats(eventForm.getNumberSeats()-occupiedSeats);
			event.setManager(original.getManager());
		} else {
			event.setAvailableSeats(eventForm.getNumberSeats());
			// Obtenemos el manager actual
			Manager manager = servicesUtils.getRegisteredUser(Manager.class);
			event.setManager(manager);
		}

		event.setTitle(eventForm.getTitle());
		event.setDate(eventForm.getDate());
		if(event.getDate() != null) {
			if(!event.getDate().after(new Date())) {
				binding.rejectValue("date", "businessRule.event.invalid.date");
			}
		}
		event.setDescription(eventForm.getDescription());
		event.setImage(eventForm.getImage());
		event.setNumberSeats(eventForm.getNumberSeats());

		validator.validate(event, binding);

		return event;
	}
}
