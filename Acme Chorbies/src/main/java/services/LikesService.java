
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.LikesRepository;
import services.utils.ServicesUtils;
import domain.Administrator;
import domain.Chorbi;
import domain.Likes;
import forms.LikesForm;

@Service
@Transactional
public class LikesService extends GenericService<LikesRepository, Likes> {

	// Constructor -----------------------------------

	public LikesService() {
		super();
	}

	// Service ---------------------------------------

	@Autowired
	private ServicesUtils	servicesUtils;

	@Autowired
	private ChorbiService	chorbiService;

	@Autowired
	private Validator		validator;


	// CRUD methdos ----------------------------------

	public Likes create() {
		Likes likes = new Likes();

		return likes;
	}

	@Override
	public Likes reglaNegocioEdit(Likes likes) {
		// Comprobamos los permisos, tanto el creador como receptor podr�n editar los Likes, que 
		// se reduce a poder eliminarlos
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(likes.getSender().getId() == chorbi.getId() || likes.getReceiver().getId() == chorbi.getId());

		return likes;
	}

	@Override
	public Likes reglaNegocioSave(Likes likes) {
		// Comprobamos los permisos
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(likes.getSender().getId() == chorbi.getId());
		// Comprobamos que no est� realizando like a si mismo
		Assert.isTrue(likes.getSender().getId() != likes.getReceiver().getId());
		// Comprobamos que no realize un like a un usuario que ya ha hecho like
		Assert.isTrue(!hasLikedTheUser(likes.getSender(), likes.getReceiver()));
		// Actualizamos la fecha de envio
		likes.setSentDate(new Date());

		return likes;
	}

	@Override
	public Likes reglaNegocioDelete(Likes likes) {
		// Comprobamos los permisos, tanto el creador como receptor podr�n eliminarlos
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(likes.getSender().getId() == chorbi.getId() || likes.getReceiver().getId() == chorbi.getId());

		return likes;
	}

	// Other methods -----------------------------------

	public boolean hasLikedTheUser(Chorbi sender, Chorbi receiver) {
		return repository.hasLikedTheUser(sender.getId(), receiver.getId()) != null;
	}
	
	public Collection<Chorbi> getChorbiesThatLiked(Chorbi chorbi) {
		return repository.getChorbiesThatLiked(chorbi.getId());
	}

	public LikesForm construct(Likes likes) {
		LikesForm likesForm = new LikesForm();

		likesForm.setComments(likes.getComments());
		likesForm.setReceiver(likes.getReceiver());
		likesForm.setStars(likes.getStars());

		return likesForm;
	}

	public Likes reconstruct(LikesForm likesForm, BindingResult binding) {
		Likes likes = create();

		// S�lo se admite la creaci�n, por lo tanto siempre se definir�n los datos no mutables
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		likes.setSender(chorbi);
		likes.setSentDate(new Date());

		if (likesForm.getComments().isEmpty()) {
			likesForm.setComments(null);
		}

		likes.setComments(likesForm.getComments());
		likes.setReceiver(likesForm.getReceiver());
		likes.setStars(likesForm.getStars());

		validator.validate(likes, binding);

		return likes;
	}

	//QUERIES

	public Long getMinNumberOfLikesPerChorbi() {
		return (Long) (repository.getMinNumberOfLikesPerChorbi().toArray())[0];
	}

	public Long getMaxNumberOfLikesPerChorbi() {
		return (Long) (repository.getMaxNumberOfLikesPerChorbi().toArray())[0];
	}

	public Double getAvgNumberOfLikesPerChorbi() {
		return repository.getAvgNumberOfLikesPerChorbi();
	}
	
	public Collection<Chorbi> getChorbiesOrderedByAverageLikes(){
		servicesUtils.getRegisteredUser(Administrator.class);
		Collection<Chorbi> result = new ArrayList<Chorbi>();
		for (Integer i : repository.getChorbiesOrderedByAverageLikes()) {
			result.add(chorbiService.findOne(i));
		}
		return result;
	}

}
