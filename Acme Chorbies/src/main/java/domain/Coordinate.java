package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;

import org.hibernate.validator.constraints.NotBlank;

@Embeddable
@Access(AccessType.PROPERTY)
public class Coordinate {
	
	// Constructors ---------------------------------------------
	
	public Coordinate() {
		super();
	}
	
	// Attributes -----------------------------------------------
	
	private String country;
	private String state;
	private String province;
	private String city;
	
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
		
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	@NotBlank
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}

}