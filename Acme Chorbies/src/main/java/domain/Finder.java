package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Access(AccessType.PROPERTY)
public class Finder extends DomainEntity {
	
	// Constructors ---------------------------------------------
	
	public Finder() {
		super();
	}
	
	// Attributes -----------------------------------------------
	
	private String relation;
	private Integer age;
	private String gender;
	private Collection<Coordinate> coordinates;
	private String keyword;
	private Date cachedDate;
	
	@Pattern(regexp="(activities|friendship|love)?")
	public String getRelation() {
		return relation;
	}
	
	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	@Min(18)
	public Integer getAge() {
		return age;
	}
	
	public void setAge(Integer age) {
		this.age = age;
	}
	
	@Pattern(regexp="(man|woman)?")
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Valid
	@ElementCollection(fetch=FetchType.EAGER)
	public Collection<Coordinate> getCoordinates() {
		return coordinates;
	}
	
	public void setCoordinates(Collection<Coordinate> coordinates) {
		this.coordinates = coordinates;
	}
	
	public String getKeyword() {
		return keyword;
	}
	
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCachedDate() {
		return cachedDate;
	}
	
	public void setCachedDate(Date cachedDate) {
		this.cachedDate = cachedDate;
	}
	
	// Relationships --------------------------------------------
	
	private Chorbi chorbi;
	private Collection<Chorbi> cachedChorbies;

	@NotNull
	@Valid
	@OneToOne(optional = false, mappedBy="finder")
	public Chorbi getChorbi() {
		return chorbi;
	}
	
	public void setChorbi(Chorbi chorbi) {
		this.chorbi = chorbi;
	}
	
	@Valid
	@ManyToMany
	public Collection<Chorbi> getCachedChorbies() {
		return cachedChorbies;
	}
	
	public void setCachedChorbies(Collection<Chorbi> cachedChorbies) {
		this.cachedChorbies = cachedChorbies;
	}
	
}