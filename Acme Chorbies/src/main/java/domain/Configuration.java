
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Configuration extends DomainEntity {

	// Constructors ---------------------------------------------

	public Configuration() {
		super();
	}


	// Attributes -----------------------------------------------

	private Collection<Url>	banners;
	private Date			cacheTimer;
	private double			managerFee;
	private double			chorbiFee;


	@ElementCollection
	@NotNull
	@Valid
	public Collection<Url> getBanners() {
		return banners;
	}

	public void setBanners(Collection<Url> banners) {
		this.banners = banners;
	}

	@NotNull
	@DateTimeFormat(pattern = "HH:mm:ss")
	@Temporal(TemporalType.TIME)
	public Date getCacheTimer() {
		return cacheTimer;
	}

	public void setCacheTimer(Date timer) {
		this.cacheTimer = timer;
	}

	@Min(0)
	public double getManagerFee() {
		return managerFee;
	}

	public void setManagerFee(double managerFee) {
		this.managerFee = managerFee;
	}

	@Min(0)
	public double getChorbiFee() {
		return chorbiFee;
	}

	public void setChorbiFee(double chorbiFee) {
		this.chorbiFee = chorbiFee;
	}

	// Relationships --------------------------------------------

}
