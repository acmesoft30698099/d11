package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

@Entity
@Access(AccessType.PROPERTY)
public class CreditCard extends DomainEntity {
	
	// Constructors ---------------------------------------------

	public CreditCard() {
		super();
	}

	// Attributes -----------------------------------------------

	private String holder;
	private String brand;
	private String number;
	private int expirationMonth;
	private int expirationYear;
	private int cvv;
	private boolean valid;

	@NotBlank
	public String getHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	@NotBlank
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@NotBlank
	@Pattern(regexp = "^[\\d]{8,19}$")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Range(min = 1, max = 12)
	public int getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(Integer expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	@Min(2017) // Tarjetas de cr�dito menores a esta fecha son inv�lidas
	public int getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}

	@Range(min = 100, max = 999)
	public int getCvv() {
		return cvv;
	}

	public void setCvv(Integer cvv) {
		this.cvv = cvv;
	}
	
	public boolean getValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	// Relationships --------------------------------------------

	private Chorbi chorbi;
	private Manager manager;

	@Valid
	@OneToOne(optional = true, mappedBy = "creditCard", cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
	public Chorbi getChorbi() {
		return chorbi;
	}

	public void setChorbi(Chorbi chorbi) {
		this.chorbi = chorbi;
	}

	@Valid
	@OneToOne(optional = true, mappedBy = "creditCard", cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

}
