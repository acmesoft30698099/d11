package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {
	
	// Constructor ----------------------------------------
	
	public Manager() {
		super();
	}
	
	// Attributes -----------------------------------------
	
	private String companyName;
	private String VATNumber;
	private double fee;
	
	@NotBlank
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	@NotBlank
	public String getVATNumber() {
		return VATNumber;
	}
	public void setVATNumber(String vATNumber) {
		VATNumber = vATNumber;
	}
	
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	
	// Relationships --------------------------------------------

	private CreditCard creditCard;
	private Collection<Event> events;

	@NotNull
	@Valid
	@OneToOne(optional = false, cascade = { CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.REFRESH})
	public CreditCard getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	
	@Valid
	@OneToMany(mappedBy = "manager", cascade = CascadeType.REMOVE)
	public Collection<Event> getEvents() {
		return events;
	}
	public void setEvents(Collection<Event> events) {
		this.events = events;
	}

}
