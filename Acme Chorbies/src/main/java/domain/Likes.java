package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Likes extends DomainEntity {
	
	// Constructor -----------------------------------
	
	public Likes() {
		super();
	}
	
	// Attributes ------------------------------------
	
	private Date sentDate;
	private String comments;
	private int stars;
	
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSentDate() {
		return sentDate;
	}
	
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@Range(min=0,max=3)
	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}
	
	// Relationships --------------------------------------------

	private Chorbi sender;
	private Chorbi receiver;

	@NotNull
	@Valid
	@ManyToOne(targetEntity = Chorbi.class)
	public Chorbi getSender() {
		return sender;
	}
	
	public void setSender(Chorbi sender) {
		this.sender = sender;
	}
	
	@NotNull
	@Valid
	@ManyToOne(targetEntity=Actor.class)
	public Chorbi getReceiver() {
		return receiver;
	}
	
	public void setReceiver(Chorbi receiver) {
		this.receiver = receiver;
	}
	
}