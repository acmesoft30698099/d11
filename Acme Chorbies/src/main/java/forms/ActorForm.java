package forms;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class ActorForm {
	
	// Constructor ---------------------------------
	
	public ActorForm() {
		super();
	}
	
	// Attributes ----------------------------------
	
	private String name;
	private String surname;
	private String email;
	private String phone;
	private int id;
	private UserAccountForm userAccount;
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@Email
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Pattern(regexp="^(\\+[0-9]{1,3})?\\s?(\\([0-9]{3}\\)\\s)?([a-zA-Z0-9]){4,}(([\\s\\-])([a-zA-Z0-9]){4,})*?$")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public UserAccountForm getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(UserAccountForm userAccount) {
		this.userAccount = userAccount;
	}
}
