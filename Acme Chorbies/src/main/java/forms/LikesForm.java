
package forms;

import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import domain.Chorbi;

public class LikesForm {

	// Constructor -----------------------------------

	public LikesForm() {
		super();
	}


	// Attributes ------------------------------------

	private String	comments;
	private int		stars;


	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

	@Range(min = 0, max = 3)
	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}


	// Relationships ---------------------------------

	private Chorbi	receiver;


	@NotNull
	@Valid
	@ManyToOne
	public Chorbi getReceiver() {
		return receiver;
	}
	public void setReceiver(Chorbi receiver) {
		this.receiver = receiver;
	}

}
