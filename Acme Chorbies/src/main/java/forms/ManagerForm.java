package forms;


public class ManagerForm extends ActorForm{
	
	public ManagerForm() {
		super();
	}
	
	private String companyName;
	private String VATNumber;
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getVATNumber() {
		return VATNumber;
	}
	
	public void setVATNumber(String vATNumber) {
		VATNumber = vATNumber;
	}

	private CreditCardForm creditCard;
	
	public CreditCardForm getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCardForm creditCard) {
		this.creditCard = creditCard;
	}

}
