package forms;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import domain.Actor;
import domain.Url;

public class ChirpForm {
	
	// Constructor -------------------------------------
	
	public ChirpForm(){
		super();
	}
	
	// Attributes --------------------------------------

	private String subject;
	private String text;
	private List<Url> attachments;
	
	@NotBlank
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	@NotBlank
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	@Valid
	@ElementCollection
	public List<Url> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Url> attachments) {
		this.attachments = attachments;
	}
	
	// Relationships -------------------------------------------

	private Actor receiver;

	@NotNull
	@Valid
	@ManyToOne
	public Actor getReceiver() {
		return receiver;
	}
	public void setReceiver(Actor receiver) {
		this.receiver = receiver;
	}

}
