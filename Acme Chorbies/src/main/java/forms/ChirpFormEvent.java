package forms;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;

import domain.Url;

public class ChirpFormEvent {
	
	// Constructor --------------------------------------

	public ChirpFormEvent() {
		super();
	}

	// Attributes --------------------------------------

	private String subject;
	private String text;
	private List<Url> attachments;
	private Integer eventId;

	@NotBlank
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@NotBlank
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Valid
	@ElementCollection
	public List<Url> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Url> attachments) {
		this.attachments = attachments;
	}

	@Min(1)
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

}
