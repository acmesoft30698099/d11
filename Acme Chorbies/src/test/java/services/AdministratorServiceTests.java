package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Administrator;
import forms.AdministratorForm;

import utilities.AbstractTest;
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AdministratorServiceTests extends AbstractTest {
	
	// Constructor --------------------------------
	
	public AdministratorServiceTests() {
		super();
	}
	
	// Service ------------------------------------
	
	@Autowired
	private AdministratorService administratorService;
	
	// Tests --------------------------------------

	@Test
	public void findOneForEdit() {
		authenticate("admin");
		Administrator admin = administratorService.findOneForEdit(9);
		Assert.notNull(admin);
	}
	
	@Test
	public void editAdministrator() {
		authenticate("admin");
		Administrator admin = administratorService.findOneForEdit(9);
		AdministratorForm adminForm = administratorService.construct(admin);
		adminForm.setName("Something completely diferent"); // Completely
		Administrator changed = administratorService.reconstruct(adminForm, null);
		admin = administratorService.save(changed);
		administratorService.flush();
		Assert.isTrue(admin.getName().equals("Something completely diferent"));
	}
}
