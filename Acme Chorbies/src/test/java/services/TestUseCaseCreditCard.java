package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Chorbi;
import domain.CreditCard;
import forms.CreditCardForm;

import repositories.ChorbiRepository;
import repositories.CreditCardRepository;
import services.ChorbiService;
import services.CreditCardService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/*

Se prueba la funcionalidad implícita relativa CreditCard, aunque no aparezca en los requisitos. Esta funcionalidad
consiste en ver, editar, crear y eliminar una CreditCard.

*/

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseCreditCard extends AbstractTest {

	@Autowired
	private CreditCardService creditCardService;
	@Autowired
	private ChorbiService chorbiService;
	@Autowired
	private CreditCardRepository creditCardRepository;
	@Autowired
	private ChorbiRepository chorbiRepository;
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Test
	public void showValid() {
		template("chorbi1", null, null, null, null, null, null, null, "show");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void showWithoutAuthenticate() {
		template(null, null, null, null, null, null, null, null, "show");
	}
	
	@Test
	public void createValid() {
		template("chorbi2", "Caixa", "VISA", "12345678903", 12, 2020, 100, true, "create");
	}
	
	@Test
	public void createNotValidBecauseNumber() {
		template("chorbi2", "Caixa", "VISA", "12345678900", 12, 2020, 100, false, "create");
	}
	
	@Test
	public void createNotValidBecauseBrand() {
		template("chorbi2", "Caixa", "PISA", "12345678903", 12, 2020, 100, false, "create");
	}
	
	@Test
	public void createNotValidBecauseExpirationDate() {
		template("chorbi2", "Caixa", "VISA", "12345678903", 3, 2017, 100, false, "create");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createWithAdmin() {
		template("admin", "Caixa", "VISA", "12345678903", 5, 2020, 100, null, "create");
	}
	
	@Test
	public void editValid() {
		template("chorbi1", "Caixa", "MASTERCARD", "12345678903", 5, 2020, 100, true, "edit");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void editWithoutAuthenticate() {
		template(null, "Caixa", "MASTERCARD", "12345678903", 5, 2020, 100, null, "edit");
	}
	
	@Test
	public void deleteValid() {
		template("chorbi1", null, null, null, null, null, null, null, "delete");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void deleteWithoutAuthenticate() {
		template(null, null, null, null, null, null, null, null, "edit");
	}
	
	public void template(String username, String holder, String brand, String number, Integer expirationMonth,
		Integer expirationYear ,Integer cvv, Boolean valid, String action) {
		super.authenticate(username);
		// Al crear o editar
		if(action.equals("create") || action.equals("edit")) {
			CreditCard creditCard = null;
			if(action.equals("create")) {
				creditCard = creditCardService.create();
			}
			else if(action.equals("edit")) {
				creditCard = servicesUtils.getRegisteredUser(Chorbi.class).getCreditCard();
			}
			CreditCardForm form = creditCardService.construct(creditCard);
			form.setBrand(brand);
			form.setCvv(cvv);
			form.setExpirationMonth(expirationMonth);
			form.setExpirationYear(expirationYear);
			form.setHolder(holder);
			form.setNumber(number);
			CreditCard result = creditCardService.reconstruct(form, null);
			CreditCard saved = creditCardService.save(result);
			Chorbi chorbi = chorbiService.findOne(servicesUtils.getRegisteredUser(Chorbi.class).getId());
			creditCardRepository.flush();
			chorbiRepository.flush();
			Assert.notNull(saved.getValid());
			Assert.isTrue(saved.getBrand().equals(brand));
			Assert.isTrue(saved.getChorbi().getId()==chorbi.getId());
			Assert.isTrue(saved.getCvv()==cvv);
			Assert.isTrue(saved.getExpirationMonth()==expirationMonth);
			Assert.isTrue(saved.getExpirationYear()==expirationYear);
			Assert.isTrue(saved.getHolder().equals(holder));
			Assert.isTrue(saved.getId()!=0);
			Assert.isTrue(saved.getNumber().equals(number));
			Assert.notNull(chorbi.getId()==saved.getChorbi().getId());
			if(valid != null) {
				Assert.isTrue(saved.getValid()==valid);
			}
		}
		// Al eliminar
		else if(action.equals("delete")) {
			CreditCard creditCard = servicesUtils.getRegisteredUser(Chorbi.class).getCreditCard();
			creditCardService.delete(creditCard);
			Assert.isNull(creditCardService.findOne(creditCard.getId()));
		}
		// Al mostrar
		else if(action.equals("show")) {
			servicesUtils.getRegisteredUser(Chorbi.class).getCreditCard();
		}
		super.authenticate(null);
	}
	
}
