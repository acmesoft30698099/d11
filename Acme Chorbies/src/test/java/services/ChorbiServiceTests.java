package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chorbi;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ChorbiServiceTests extends AbstractTest {
	
	// Constructor -----------------------------------
	
	public ChorbiServiceTests() {
		super();
	}
	
	// Services --------------------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	// Tests -----------------------------------------
	
	@Test
	public void editChorbi() {
		authenticate("chorbi1");
		Chorbi chorbi = chorbiService.findOneForEdit(9);
		chorbi.setName("Hola2");
		Chorbi saved = chorbiService.save(chorbi);
		Assert.isTrue(saved.getName().equals("Hola2"));
		unauthenticate();
	}
	
	@SuppressWarnings("deprecation")
	@Test(expected=IllegalArgumentException.class)
	public void editIncorrectAgeChorbi() {
		authenticate("chorbi1");
		//Ya que se ejecutará el reconstruct y el reconstruct contiene findOneForEdit
		Chorbi chorbi = chorbiService.findOneForEdit(9);
		chorbi.setName("Hola2");
		Date now = new Date();
		now.setYear(2017);
		now.setMonth(2);
		now.setDate(1);
		chorbi.setBirthDate(now);
		chorbiService.save(chorbi);
		unauthenticate();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void editIncorrectUserChorbi() {
		unauthenticate();
		//Ya que se ejecutará el reconstruct y el reconstruct contiene findOneForEdit
		Chorbi chorbi = chorbiService.findOneForEdit(9);
		chorbi.setName("Hola2");
		chorbiService.save(chorbi);
		unauthenticate();
	}
	
}
