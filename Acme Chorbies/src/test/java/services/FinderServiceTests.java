package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Chorbi;
import domain.Finder;
import forms.FinderForm;

import utilities.AbstractTest;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class FinderServiceTests extends AbstractTest {
	
	// Constructor ------------------------------------
	
	public FinderServiceTests() {
		super();
	}
	
	// Service ----------------------------------------
	
	@Autowired
	private FinderService finderService;
	
	// Tests ------------------------------------------
	
	@Test
	public void searchWithFinder() {
		Finder finder = finderService.findOne(18);
		Collection<Chorbi> chorbies = finderService.searchWithFinder(finder);
		System.out.println("Chorbies : "+chorbies.toString());
	}
	
	@Test
	public void getResultsOfFinder() {
		authenticate("chorbi2");
		Finder finder = finderService.findOneForEdit(19);
		Collection<Chorbi> chorbies = finderService.getResultsOfFinder(finder);
		finderService.flush();
		System.out.println("Chorbies : " + chorbies.toString());
	}
	
	@Test
	public void editFinder() {
		authenticate("chorbi2");
		Finder finder = finderService.findOneForEdit(19);
		Collection<Chorbi> chorbies = finderService.getResultsOfFinder(finder);
		finderService.flush();
		FinderForm form = finderService.construct(finder);
		form.setAge(150);
		finder = finderService.reconstruct(form, null);
		finder = finderService.save(finder);
		finderService.flush();
		Assert.isTrue(finder.getCachedChorbies().size() != chorbies.size());
	}
}
