package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Chorbi;
import domain.Likes;
import forms.LikesForm;

import utilities.AbstractTest;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class LikesServiceTests extends AbstractTest {

	// Constructor ------------------------------------

	public LikesServiceTests() {
		super();
	}

	// Service ----------------------------------------

	@Autowired
	private LikesService likesService;

	@Autowired
	private ActorService actorService;

	// Tests ------------------------------------------

	@Test
	public void createLike() {
		authenticate("chorbi1");
		Likes likes = likesService.create();
		LikesForm likesForm = likesService.construct(likes);
		likesForm.setComments(null);
		likesForm.setReceiver((Chorbi) actorService.findOne(13));
		likes = likesService.reconstruct(likesForm, null);
		likes = likesService.save(likes);
		likesService.flush();
		Assert.notNull(likes);
		likesService.delete(likes);
		likesService.flush();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void deleteIncorrectLikes() {
		authenticate("chorbi1");
		Likes likes = likesService.create();
		LikesForm likesForm = likesService.construct(likes);
		likesForm.setComments(null);
		likesForm.setReceiver((Chorbi) actorService.findOne(13));
		likes = likesService.reconstruct(likesForm, null);
		likes = likesService.save(likes);
		likesService.flush();
		Assert.notNull(likes);
		authenticate("chorbi2");
		likesService.delete(likes);
		likesService.flush();
	}

}
