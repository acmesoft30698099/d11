package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Chirp;
import domain.Chorbi;
import forms.ChirpForm;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ChirpServiceTests extends AbstractTest {

	// Constructor ---------------------------------------

	public ChirpServiceTests() {
		super();
	}

	// Services ------------------------------------------

	@Autowired
	private ChirpService chirpService;

	@Autowired
	private ActorService actorService;

	// Tests ---------------------------------------------

	@Test
	public void sendAndReplyChirp() {
		authenticate("chorbi1");
		Chirp chirp = chirpService.create();
		ChirpForm chirpForm = chirpService.construct(chirp);
		Chorbi chorbiRec = (Chorbi) actorService.findOne(13);
		chirpForm.setSubject("Subject");
		chirpForm.setText("Text");
		chirpForm.setReceiver(chorbiRec);
		chirp = chirpService.reconstruct(chirpForm, null);
		chirp = chirpService.save(chirp);
		chirpService.flush();
		Assert.notNull(chirp);
		authenticate(chorbiRec.getUserAccount().getUsername());
		Chirp chirpResp = chirpService.create();
		Chirp chirpOriginal = chirpService.findOne(chirp.getId());
		ChirpForm form = chirpService.construct(chirpOriginal);
		chirpResp = chirpService.reconstructReply(form, null, "Response");
		chirpResp = chirpService.save(chirpResp);
		chirpService.flush();
		Assert.notNull(chirpResp);
	}
	
	@Test
	public void sendAndForward() {
		authenticate("chorbi1");
		Chirp chirp = chirpService.create();
		ChirpForm chirpForm = chirpService.construct(chirp);
		Chorbi chorbiRec = (Chorbi) actorService.findOne(13);
		chirpForm.setSubject("Subject");
		chirpForm.setText("Text");
		chirpForm.setReceiver(chorbiRec);
		chirp = chirpService.reconstruct(chirpForm, null);
		chirp = chirpService.save(chirp);
		chirpService.flush();
		Assert.notNull(chirp);
		Chirp chirpOriginal = chirpService.findOne(chirp.getId());
		chorbiRec = (Chorbi) actorService.findOne(12);
		Chirp chirpFwd = chirpService.reconstructForward(chirpOriginal.getId(), chorbiRec.getId());
		chirpService.save(chirpFwd);
		chirpService.flush();
	}

}
