package usecases;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChorbiService;
import utilities.AbstractTest;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
/**
 * Casos de prueba para el siguiente requisito funcional
 * 
- An actor who is authenticated as an administrator must be able to:
	Display a dashboard with the following information:
 		-A listing with the number of chorbies per country and city.
		-The minimum, the maximum, and the average ages of the chorbies.
		-The ratio of chorbies who have not registered a credit card or have regis-tered an invalid credit card.
		-The ratios of chorbies who search for "activities", "friendship", and "love".
 * @author Student
 *
 */
public class TestUseCaseDashboard extends AbstractTest {
	
	// Constructor ------------------------
	
	public TestUseCaseDashboard() {
		super();
	}
	
	// Services ---------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	// Templates --------------------------
	
	private void dashboardTemplate(String username, String testName, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			// El usuario se registra
			authenticate(username);
			Collection<Object[]> getNumberOfChorbiesByTheirCountryAndCityObject = chorbiService.getNumberOfChorbiesByTheirCountryAndCity();
			chorbiService.getMaxAgeOfChorbies();
			chorbiService.getMinAgeOfChorbies();
			chorbiService.getAvgAgeOfChorbies();
			chorbiService.getAmountOfChorbieswithInvalidOrNullCreditCard();
			chorbiService.getAmountOfChorbiesSearchingFor("activities");
			chorbiService.getAmountOfChorbiesSearchingFor("friendship");
			chorbiService.getAmountOfChorbiesSearchingFor("love");
			chorbiService.getChorbiesOrderedByNumberOfReceivedLikes();
			chorbiService.getMinNumberOfLikesPerChorbi();
			chorbiService.getMaxNumberOfLikesPerChorbi();
			chorbiService.getAvgNumberOfLikesPerChorbi();
			chorbiService.getMinNumberOfReceivedChirpsPerChorbi();
			chorbiService.getMaxNumberOfReceivedChirpsPerChorbi();
			chorbiService.getAvgNumberOfReceivedChirpsPerChorbi();
			chorbiService.getMinNumberOfSentChirpsPerChorbi();
			chorbiService.getMaxNumberOfSentChirpsPerChorbi();
			chorbiService.getAvgNumberOfSentChirpsPerChorbi();
			chorbiService.getChorbiesWithTheMostReceivedChirps();
			chorbiService.getChorbiesWithTheMostSentChirps();
			
			Map<String, Long> getNumberOfChorbiesByTheirCountryAndCity = new HashMap<String, Long>();
			for(Object[] array:getNumberOfChorbiesByTheirCountryAndCityObject){
				String key = (String) array[0];
				Long value = (Long) array[1];
				if(key!=null && key.startsWith(",")){
					key = key.substring(1,key.length());
				}
				getNumberOfChorbiesByTheirCountryAndCity.put(key,value); 
			}
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	
	
	// Tests ------------------------------
	
	// 1. Aplicación correcta a un Dashboard
	@Test
	public void testCaseCorrect() {
		dashboardTemplate("admin","ValidTest", null);
	}
	
	// 2. Usuario incorrecto
	@Test
	public void testCaseInvalidUser() {
		dashboardTemplate("chorbi1","InvalidTest", IllegalArgumentException.class);
	}

}
