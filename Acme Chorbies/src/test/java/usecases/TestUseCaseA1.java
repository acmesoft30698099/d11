package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ActorService;
import services.ChirpService;
import utilities.AbstractTest;
import domain.Chirp;
import domain.Chorbi;
import forms.ChirpForm;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Chirp to another chorbi.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseA1 extends AbstractTest {
	
	// Constructor -------------------------
	
	public TestUseCaseA1() {
		super();
	}
	
	// Services ----------------------------
	
	@Autowired
	private ChirpService chirpService;
	
	@Autowired
	private ActorService actorService;
	
	// Templates ---------------------------
	
	private void chirpSaveTemplate(String username, String title, ChirpForm chirpForm, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			authenticate(username);
			// El usuario accede al formulario de creaci�n de chirps y introduce los datos.
			// Le da click a guardar, lo que ejecuta el m�todo reconstruct
			Chirp chirp = chirpService.reconstruct(chirpForm, null);
			// Ejecuta el guardado
			chirpService.save(chirp);
			unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, title);
	}
	
	// Tests -------------------------------
	
	// 1. Creaci�n correcta de chirp.
	@Test
	public void testCaseSaveCorrect() {
		Chirp msg = chirpService.create();
		ChirpForm chirpForm = chirpService.construct(msg);
		Chorbi receiver = (Chorbi) actorService.findById(19);
		
		chirpForm.setSubject("T�tulo");
		chirpForm.setText("Texto");
		chirpForm.setReceiver(receiver);
		
		chirpSaveTemplate("chorbi1", "validTest", chirpForm, null);
	}
	
	// 2. Creaci�n incorrecta de chirp, usuario no registrado
	@Test
	public void testCaseSaveIncorrect() {
		Chirp msg = chirpService.create();
		ChirpForm chirpForm = chirpService.construct(msg);
		Chorbi receiver = (Chorbi) actorService.findById(19);
		
		chirpForm.setSubject("T�tulo");
		chirpForm.setText("Texto");
		chirpForm.setReceiver(receiver);
		
		chirpSaveTemplate(null, "invalidTest", chirpForm, IllegalArgumentException.class);
	}

}
