package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChirpService;
import utilities.AbstractTest;
import domain.Chirp;

/**
 * Caso de prueba del requisito funcional siguente:
 o Erase his or her messages, which requires previous confirmation.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseA4 extends AbstractTest {
	
	// Constructor --------------------------
	
	public TestUseCaseA4() {
		super();
	}
	
	// Services ----------------------------
	
	@Autowired
	private ChirpService chirpService;
	
	// Templates ---------------------------
	
	private void chirpDeleteTemplate(String username, String title, Integer chirpId, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			authenticate(username);
			// El usuario accede al formulario de creaci�n de chirps y introduce los datos.
			// Le da click a guardar, lo que ejecuta el m�todo reconstruct
			Chirp chirp = chirpService.findOne(chirpId);
			// Ejecuta el guardado
			chirpService.delete(chirp);
			unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, title);
	}
	
	// Tests -------------------------------
	
	// 1. Eliminaci�n correcta de chirp.
	@Test
	public void testCaseDeleteCorrect() {
		chirpDeleteTemplate("chorbi1", "validTest", 59, null);
	}
	
	// 2. Eliminaci�n incorrecta de chirp, usuario no emisor ni receptor
	@Test
	public void testCaseDeleteIncorrect() {
		chirpDeleteTemplate("chorbi3", "invalidTest", 59, IllegalArgumentException.class);
	}


}
