package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.LikesService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.Likes;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Browse the catalogue of chorbies who have liked him or her as long as he or she has
   registered a valid credit card. If he or she's not register a valid card, then the system
   must ask him or her to do so; the system must inform the chorbies that their credit
   cards will not be charged.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2A2 extends AbstractTest {
	
	// Constructor --------------------------------------

	public TestUseCase2A2() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private LikesService likesService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template -----------------------------------------
	
	public void templateListLikedChorbies(String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Comprobación del usuario
			Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
			// Obtención de los Chorbies
			Collection<Chorbi> chorbies = likesService.getChorbiesThatLiked(chorbi);
			for(Chorbi chorbiI : chorbies) {
				boolean hasLike = false;
				for(Likes likes : chorbiI.getSentLikes()) {
					if(likes.getReceiver().getId() == chorbi.getId()) {
						hasLike = true;
						break;
					}
				}
				Assert.isTrue(hasLike);
			}
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	// Test correcto ----------------------------------
	
	@Test
	public void testListLikesCorrect() {
		templateListLikedChorbies("chorbi1", null, "testListLikesCorrect");
	}
	
	@Test
	public void testsListLikesIncorrect() {
		templateListLikedChorbies("admin", IllegalArgumentException.class, "testListLikesCorrect");
	}

}
