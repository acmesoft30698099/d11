package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Administrator;
import domain.Configuration;

import forms.ConfigurationForm;

import services.ConfigurationService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Change the fee that is charged to managers and chorbies. (Note that they need not
   be the same.)
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C7 extends AbstractTest {
	
	// Constructor --------------------------------------

	public TestUseCase2C7() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ConfigurationService configurationService;
	// Templates ----------------------------------------
	
	public void templateChangeFees(ConfigurationForm configurationForm, String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Comprobamos los permisos del usuario
			servicesUtils.getRegisteredUser(Administrator.class);
			// Convertimos el objeto form
			Configuration configuration = configurationService.reconstruct(configurationForm, null);
			// Realizamos el guardado
			configurationService.save(configuration);
			configurationService.flush();
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------------
	
	// 1. Test correcto 
	
	@Test
	public void testChangeFeeCorrect() {
		ConfigurationForm configForm = configurationService.construct(configurationService.findConfiguration());
		configForm.setChorbiFee(60.5);
		
		templateChangeFees(configForm, "admin", null, "changeFeeCorrect");
	}
	
	// 2. Test incorrecto, usuario inv�lido
	
	@Test
	public void testChangeFeeIncorrect() {
		ConfigurationForm configForm = configurationService.construct(configurationService.findConfiguration());
		configForm.setChorbiFee(60.5);
		
		templateChangeFees(configForm, "manager3", IllegalArgumentException.class, "changeFeeIncorrect");
	}

}
