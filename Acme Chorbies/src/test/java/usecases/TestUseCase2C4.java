package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.EventService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Chorbi;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Register to an event as long as there are enough seats available.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C4 extends AbstractTest {
	
	// Constructor --------------------------------------

	public TestUseCase2C4() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template ------------------------------------------
	
	public void templateRegisterEvent(Integer eventId, String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Comprobamos los permisos
			Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
			// Ejecutamos el registro
			eventService.registerToEvent(chorbi.getId(), eventId);
			eventService.flush();
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Tests ----------------------------------------------
	
	// 1. Test correcto de registro de evento
	
	@Test
	public void testRegisterEventCorrect() {
		templateRegisterEvent(80, "chorbi3", null, "testRegisterEventCorrect");
	}
	
	// 2. Test incorrecto de registro de evento
	
	@Test
	public void testRegisterEventIncorrect() {
		templateRegisterEvent(80, null, IllegalArgumentException.class, "testRegisterEventIncorrect");
	}

}
