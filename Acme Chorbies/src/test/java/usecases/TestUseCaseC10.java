package usecases;

import javax.transaction.Transactional;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.joda.DateTimeFormatterFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Administrator;
import domain.Configuration;

import forms.ConfigurationForm;

import services.ConfigurationService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Test del caso de uso siguiente: 
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseC10 extends AbstractTest {
	
	// Constructor ----------------------------------
	
	public TestUseCaseC10() {
		super();
	}
	
	// Services -------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ServicesUtils servicesUtils;

	// Template -------------------------------------
	
	public void templateSaveConfiguration(String username, ConfigurationForm configurationForm,
			Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			// Comprobamos los permisos del usuario por el authority
			servicesUtils.getRegisteredUser(Administrator.class);
			// Reconstruimos el Configuration
			Configuration conf = configurationService.reconstruct(configurationForm, null);
			// Guardamos el objeto
			conf = configurationService.save(conf);
			configurationService.flush();
			// Comprobamos que existe
			conf = configurationService.findOne(conf.getId());
			Assert.notNull(conf);
			Assert.isTrue(conf.getCacheTimer().equals(configurationForm.getCacheTimer()));
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ------------------------------------------
	
	// 1. Test positivo -------------------------------
	
	@Test
	public void testCorrectEditConfiguration() {
		ConfigurationForm form = configurationService.construct(configurationService.findConfiguration());
		form.setCacheTimer(LocalDate.parse("00:01:00", new DateTimeFormatterFactory("HH:mm:ss").createDateTimeFormatter()).toDate());
		
		templateSaveConfiguration("admin", form, null, "testSaveConfigurationCorrect");
	}
	
	// 2. Test negativo, usuario no v�lido ------------
	
	@Test
	public void testIncorrectEditConfiguration() {
		ConfigurationForm form = configurationService.construct(configurationService.findConfiguration());
		form.setCacheTimer(LocalDate.parse("00:01:00", new DateTimeFormatterFactory("HH:mm:ss").createDateTimeFormatter()).toDate());
		
		templateSaveConfiguration("chorbi1", form, IllegalArgumentException.class, "testSaveConfigurationCorrect");
	}

}
