package usecases;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.FinderService;
import utilities.AbstractTest;
import domain.Coordinate;
import domain.Finder;
import forms.FinderForm;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
/**
 * Casos de prueba para el siguiente requisito funcional
 * 
 * - An actor who is authenticated as a chorbi must be able to:
 		Change his or her search template.
 * @author Student
 *
 */
public class TestUseCaseC6 extends AbstractTest {

	// Constructor ---------------------------

	public TestUseCaseC6() {
		super();
	}

	// Services ------------------------------

	@Autowired
	private FinderService finderService;
	
	// Templates -----------------------------
	
	private void finderSaveTemplate(String username, String testName, FinderForm finderForm, Class<?> expected) {
		Class<?> caught = null;

		try {
			// El usuario se autentifica como Chorbi
			authenticate(username);
			// Accede al formulario de edici�n de Finder, que le muestra
			// un
			// FinderForm. Rellena los datos y
			// Le da click a guardar, lo que hace que se ejecute el m�todo
			// reconstruct
			Finder finder = finderService.reconstruct(finderForm, null);
			// Y despues al m�todo save
			finder = finderService.save(finder);
			finderService.flush();
			// Finalmente, muestra en pantalla el perfil del Finder si todo ha
			// salido bien
			finder = finderService.findOne(finder.getId());
			// Comprobaciones
			if(finder.getAge()!= null){
				Assert.isTrue(finder.getAge().equals(finderForm.getAge()));
			}
			if(finder.getRelation() != null){
				Assert.isTrue(finder.getRelation().equals(finderForm.getRelation()));
			}
			if(finder.getGender() != null){
				Assert.isTrue(finder.getGender().equals(finderForm.getGender()));
			}
			if(finder.getKeyword() != null){
				Assert.isTrue(finder.getKeyword().equals(finderForm.getKeyword()));
			}
			if(finder.getCoordinates() != null) {
				for(Coordinate original : finderForm.getCoordinates()){
					for(Coordinate coordinate : finder.getCoordinates()){
						Assert.isTrue(original.equals(coordinate));
					}
				}
			}
			unauthenticate();
			
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------
	
	// 1. Edici�n correcta de Finder
	@Test
	public void testCaseSaveCorrect() {
		Finder finder = finderService.findOne(18);
		
		FinderForm finderForm = finderService.construct(finder);
		
		finderForm.setAge(34);
		finderForm.setRelation(null);
		finderForm.setKeyword("adsada");
		
		finderSaveTemplate("chorbi1", "ValidTest", finderForm, null);
	}
	
	// 1. Edici�n incorrecta de Finder (edad incorrecta)
	@Test
	public void testCaseSaveIncorrect() {
		Finder finder = finderService.findOne(18);

		FinderForm finderForm = finderService.construct(finder);

		finderForm.setAge(14);
		finderForm.setRelation(null);
		finderForm.setKeyword("adsada");

		finderSaveTemplate("chorbi1", "ValidTest", finderForm, ConstraintViolationException.class);
	}
}
