package usecases;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.EventService;
import utilities.AbstractTest;
import domain.Event;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Browse the  listing  of  events  that  are  going  to  be  organised in  less  than  one month
   and have seats available.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C1 extends AbstractTest {

	// Constructor -----------------------------------
	
	public TestUseCase2C1() {
		super();
	}
	
	// Services --------------------------------------
	
	@Autowired
	private EventService eventService;
	
	// Template --------------------------------------
	
	public void templateListAvailableEvents(String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Obtenemos la lista de eventos
			Collection<Event> events = eventService.getAvailableEventsOfOneMonth();
			// Comprobamos que es correcto
			Date now = new Date();
			Calendar oneMonth = Calendar.getInstance();
			oneMonth.add(Calendar.MONTH, 1);
			for(Event event : events) {
				Assert.isTrue(event.getAvailableSeats() > 0);
				Assert.isTrue(event.getDate().after(now) && event.getDate().before(oneMonth.getTime()));
			}
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	
	// Tests -----------------------------------------
	
	// 1. Test correcto de listado de eventos disponibles.
	
	@Test
	public void testListAvailableEvents() {
		templateListAvailableEvents(null, null, "listAvailableEvents");
	}
}
