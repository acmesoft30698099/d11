package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.EventService;
import utilities.AbstractTest;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Browse  a  listing  that  includes  every  event  that  was  registered  in  the  system.    Past 
   events  must  be  greyed out;  events  that  are  going  to  be  organised  in  less  than  one 
   month  and  have  seats  available  must  also  be  somewhat  highlighted;  the  rest  of 
   events must be displayed normally.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C2 extends AbstractTest {
	
	// Constructor --------------------------------------

	public TestUseCase2C2() {
		super();
	}

	// Services -----------------------------------------
	
	@Autowired
	private EventService eventService;
	
	// Template -----------------------------------------
	
	public void testListEvents(String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			eventService.findAll();
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Test ----------------------------------------------
	
	// 1. Test correcto de listado de eventos.
	
	@Test
	public void testListEvents() {
		testListEvents(null,null,"listEvents");
	}
}
