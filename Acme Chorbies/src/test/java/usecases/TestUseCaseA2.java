package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChirpService;
import utilities.AbstractTest;
import domain.Chirp;
import forms.ChirpForm;

/**
 * Caso de prueba para el requisito funcional siguiente:
 o Browse the list of chirps that he or she's got, and reply to any of them
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseA2 extends AbstractTest {
	
	// Constructor -------------------
	
	public TestUseCaseA2() {
		super();
	}
	
	// Services ----------------------------
	
	@Autowired
	private ChirpService chirpService;
	
	// Templates ---------------------------
	
	private void chirpReplyTemplate(String username, String title, Chirp chirp, String texto, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			authenticate(username);
			// El usuario accede al listado de chirps y le da click a responder en uno.
			ChirpForm chirpForm = chirpService.constructReply(chirp);
			// Rellena los datos y le da click a guardar, lo que ejecuta el m�todo reconstruct.
			Chirp res = chirpService.reconstructReply(chirpForm, null, texto);
			// Ejecuta el guardado
			chirpService.save(res);
			unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, title);
	}
	
	// Tests -------------------------------
	
	// 1. Respuesta correcta de chirp.
	@Test
	public void testCaseReplyCorrect() {
		Chirp chirp = chirpService.findOne(60);
		
		chirpReplyTemplate("chorbi1", "validTest", chirp, "Texto", null);
	}
	
	// 2. Respuesta incorrecta de chirp, chirp a responder no le pertenece
	@Test
	public void testCaseSaveIncorrect() {
		Chirp chirp = chirpService.findOne(60);
		
		
		chirpReplyTemplate("chorbi2", "validTest", chirp, "Texto", IllegalArgumentException.class);
	}


}
