package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Administrator;
import domain.Chorbi;

import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Test del caso de uso siguiente: Login to the system using his or her credentials.
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseC2 extends AbstractTest {
	
	// Constructor --------------------------------------
	
	public TestUseCaseC2() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template -----------------------------------------

	private void loginTemplate(String username, Class<? extends Actor> user, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			// Iniciamos sesi�n
			super.authenticate(username);
			// Comprobamos que el usuario tiene los permisos esperados
			Actor actor = servicesUtils.checkUser();
			Assert.isTrue(user.isAssignableFrom(actor.getClass()));
			servicesUtils.getRegisteredUser(user);
			
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------------
	
	// 1. Test correcto ----------------------------------
	
	@Test
	public void loginTestCorrect() {
		loginTemplate("chorbi1", Chorbi.class, null, "loginTestCorrect1");
		loginTemplate("chorbi3", Chorbi.class, null, "loginTestCorrect2");
		loginTemplate("admin", Administrator.class, null, "loginTestCorrect3");
	}
	
	// 2. Test incorrecto, usuarios no esperados ---------
	
	@Test
	public void loginTestIncorrect() {
		loginTemplate("admin", Chorbi.class, IllegalArgumentException.class, "loginTestCorrect2");
		loginTemplate("chorbi3", Administrator.class, IllegalArgumentException.class, "loginTestCorrect3");
	}
}
