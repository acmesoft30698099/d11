package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Chorbi;

import services.ChorbiService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Test del caso de uso siguiente: Browse the list of chorbies who have
 * registered to the system.
 * 
 * @author Student
 * 
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseC4 extends AbstractTest {

	// Constructor ------------------------------------

	public TestUseCaseC4() {
		super();
	}

	// Services ---------------------------------------

	@Autowired
	private ChorbiService chorbiService;

	@Autowired
	private ServicesUtils servicesUtils;

	// Template ---------------------------------------

	public void templateListChorbies(String username, Class<?> expected,
			String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Comprobamos el usuario ya que el security evitará que accedan
			// otros usuarios
			servicesUtils.checkUser();
			// Una vez realizado la comprobación, se obtienen los chorbies
			Collection<Chorbi> chorbies = chorbiService.findAll();
			// Comprobamos que tiene elementos
			Assert.isTrue(chorbies.size() >= 1);
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	// Tests ------------------------------------------

	// Tests positivos --------------------------------

	@Test
	public void testCorrectListChorbies() {
		templateListChorbies("chorbi2", null, "listChorbiesCorrect");
	}

	// Tests negativos --------------------------------

	@Test
	public void testIncorrectListChorbies() {
		templateListChorbies(null, IllegalArgumentException.class, "listChorbiesIncorrect");
	}
}
