package usecases;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.CreditCardService;
import services.FinderService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.CreditCard;
import domain.Finder;
import forms.CreditCardForm;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
/**
 * Casos de prueba para el siguiente requisito funcional
 * 
 * - Browse the results of his or her search template as long as he or she's registered
 		a valid credit card. Note that the validity of the credit card must be checked every
		time the results of the search template are displayed. The results of search templates
		must be cached for at least 12 hours.
 * @author Student
 *
 */
public class TestUseCaseC7 extends AbstractTest {

	// Constructor ---------------------------

	public TestUseCaseC7() {
		super();
	}

	// Services ------------------------------

	@Autowired
	private FinderService finderService;
	
	@Autowired
	private CreditCardService creditCardService;
	
	@Autowired
	private ServicesUtils serviceUtils;
	
	// Templates -----------------------------
	
	private void finderSaveTemplate(String username, String testName, Finder finder, Class<?> expected) {
		Class<?> caught = null;

		try {
			// El usuario se autentifica como Chorbi
			authenticate(username);
			// Obtenemos los resultados
			Collection<Chorbi> results = new ArrayList<Chorbi>();
			results = finderService.getResultsOfFinder(finder);
			// Finalmente, comprobamos que los datos son correctos
			for(Chorbi chorbi : results){
				if(finder.getAge() != null){
					Assert.isTrue(chorbi.getAge()<=finder.getAge()+5 && chorbi.getAge()>=finder.getAge()-5);
				}
				if(finder.getGender() != null){
					Assert.isTrue(chorbi.getGender().equals(finder.getGender()));
				}
				if(finder.getRelation() != null){
					Assert.isTrue(chorbi.getRelation().equals(finder.getRelation()));
				}
				if(finder.getKeyword() != null){
					Assert.isTrue(chorbi.getDescription().contains(finder.getKeyword()));
				}
				if(finder.getCoordinates() != null && !finder.getCoordinates().isEmpty()){
					Assert.isTrue(finder.getCoordinates().contains(chorbi.getCoordinate()));
				}
			}
			unauthenticate();
			
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	private void creditCardSaveTemplate(String username, CreditCardForm creditCardForm, boolean valid, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			// Comprobamos los permisos del usuario
			serviceUtils.getRegisteredUser(Chorbi.class);
			// Reconstruimos la CreditCard
			CreditCard creditCard = creditCardService.reconstruct(creditCardForm, null);
			// Realizamos el guardado
			creditCard = creditCardService.save(creditCard);
			creditCardService.flush();
			// Comprobamos que la CreditCard es correcta
			Assert.notNull(creditCard);
			Assert.isTrue(creditCard.getValid() == valid);
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void creditCardRemoveTemplate(String username, Integer creditCardId, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			// Obtenemos la creditCard
			CreditCard creditCard = creditCardService.findOne(creditCardId);
			// Ejecutamos el m�todo de eliminaci�n
			creditCardService.delete(creditCard);
			creditCardService.flush();
			// Comprobamos que ya no existe
			creditCard = creditCardService.findOne(creditCardId);
			Assert.isNull(creditCard);
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------
	
	// 1. Obtenci�n correcta de resultados
	@Test
	public void testCaseSaveCorrect() {
		Finder finder = finderService.findOne(18);
		
		finderSaveTemplate("chorbi1", "ValidTest", finder, null);
	}
	
	// 2. Obtenci�n incorrecta de resultados (credit card inv�lida)
	
	@Test
	public void testCaseSaveIncorrect() {
		Finder finder = finderService.findOne(20);

		finderSaveTemplate("chorbi2", "InvalidTest", finder, IllegalArgumentException.class);
	}
	
	// 3. Creaci�n correcta de CreditCard
	
	@Test
	public void testCreditCardSaveCorrect() {
		CreditCardForm creditCardFormCorrect = creditCardService.construct(creditCardService.create());
		creditCardFormCorrect.setBrand("VISA");
		creditCardFormCorrect.setCvv(100);
		creditCardFormCorrect.setExpirationMonth(12);
		creditCardFormCorrect.setExpirationYear(2020);
		creditCardFormCorrect.setHolder("Caixa");
		creditCardFormCorrect.setNumber("12345678903");
		
		creditCardSaveTemplate("chorbi2", creditCardFormCorrect, true, null, "creditCardCreateCorrect"); 
	}
	
	// 4. Creaci�n incorrecta de CreditCard, n�mero incorrecto
	
	@Test
	public void testCreditCardCreateIncorrectNumber() {
		CreditCardForm creditCardFormIncorrectNumber = creditCardService.construct(creditCardService.create());
		creditCardFormIncorrectNumber.setBrand("VISA");
		creditCardFormIncorrectNumber.setCvv(100);
		creditCardFormIncorrectNumber.setExpirationMonth(12);
		creditCardFormIncorrectNumber.setExpirationYear(2020);
		creditCardFormIncorrectNumber.setHolder("Caixa");
		creditCardFormIncorrectNumber.setNumber("12345678900");
		
		creditCardSaveTemplate("chorbi2", creditCardFormIncorrectNumber, false, null, "creditCardCreateIncorrectNumber");
	}
	
	// 5. Creaci�n incorrecta de CreditCard, compa�ia incorrecta
	
	@Test
	public void testCreditCardCreateIncorrectBrand() {
		CreditCardForm creditCardFormIncorrectBrand = creditCardService.construct(creditCardService.create());
		creditCardFormIncorrectBrand.setBrand("PISA");
		creditCardFormIncorrectBrand.setCvv(100);
		creditCardFormIncorrectBrand.setExpirationMonth(12);
		creditCardFormIncorrectBrand.setExpirationYear(2020);
		creditCardFormIncorrectBrand.setHolder("Caixa");
		creditCardFormIncorrectBrand.setNumber("12345678903");
		
		creditCardSaveTemplate("chorbi2", creditCardFormIncorrectBrand, false, null, "creditCardCreateIncorrectBrand");
	}

	// 6. Creaci�n incorrecta de CreditCard, fecha de caducidad incorrecta
	
	@Test
	public void testCreditCardCreateIncorrectDate() {
		CreditCardForm creditCardFormIncorrectExpirationDate = creditCardService.construct(creditCardService.create());
		creditCardFormIncorrectExpirationDate.setBrand("PISA");
		creditCardFormIncorrectExpirationDate.setCvv(100);
		creditCardFormIncorrectExpirationDate.setExpirationMonth(3);
		creditCardFormIncorrectExpirationDate.setExpirationYear(2017);
		creditCardFormIncorrectExpirationDate.setHolder("Caixa");
		creditCardFormIncorrectExpirationDate.setNumber("12345678900");
		
		creditCardSaveTemplate("chorbi2", creditCardFormIncorrectExpirationDate, false, null, "creditCardCreateIncorrectBrand");
	}
	
	// 7. Creaci�n incorrecta de CreditCard, usuario inv�lido
	
	@Test
	public void testCreditCardCreateIncorrectUser() {
		CreditCardForm creditCardFormCorrect = creditCardService.construct(creditCardService.create());
		creditCardFormCorrect.setBrand("VISA");
		creditCardFormCorrect.setCvv(100);
		creditCardFormCorrect.setExpirationMonth(12);
		creditCardFormCorrect.setExpirationYear(2020);
		creditCardFormCorrect.setHolder("Caixa");
		creditCardFormCorrect.setNumber("12345678903");
		
		creditCardSaveTemplate("admin", creditCardFormCorrect, true, IllegalArgumentException.class, "creditCardCreateIncorrectUser"); 
	}
	
	// 8. Edici�n correcta de CreditCard
	
	@Test
	public void testCreditCardEditCorrect() {
		CreditCardForm creditCardForm = creditCardService.construct(creditCardService.findOne(17));
		creditCardForm.setBrand("MASTERCARD");
		
		creditCardSaveTemplate("chorbi1", creditCardForm, true, null, "creditCardEditCorrect");
	}

	// 9. Edici�n incorrecta de CreditCard, usuario no v�lido
	
	@Test
	public void testCreditCardEditIncorrect() {
		CreditCardForm creditCardForm = creditCardService.construct(creditCardService.findOne(17));
		creditCardForm.setBrand("MASTERCARD");
		
		creditCardSaveTemplate("chorbi3", creditCardForm, true, IllegalArgumentException.class, "creditCardEditCorrect");
	}
	
	// 10. Eliminaci�n de CreditCard correcta
	
	@Test
	public void testCreditCardDeleteCorrect() {
		creditCardRemoveTemplate("chorbi1", 17, null, "creditCardDeleteCorrect");
	}
}
