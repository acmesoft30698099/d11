package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChorbiService;
import services.LikesService;
import services.ManagerService;
import utilities.AbstractTest;

@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
/**
 * Casos de prueba para el siguiente requisito funcional
 * 
- An actor who is authenticated as an administrator must be able to:
	Display a dashboard with the following information:
 		-A listing of managers sorted by the number of events that they organise.
		-A listing of managers that includes the amount that they due in fees.
		-A listing of chorbies sorted by the number of events to which they have registered.
		-A listing of chorbies that includes the amount that they due in fees.
		-The minimum, the maximum, and the average number of stars per chorbi.
		-The list of chorbies, sorted by the average number of stars that they've got.
 * @author Student
 *
 */
public class TestUseCase2Dashboard extends AbstractTest {
	
	// Constructor ------------------------
	
	public TestUseCase2Dashboard() {
		super();
	}
	
	// Services ---------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private ManagerService managerService;
	
	@Autowired
	private LikesService likesService;
	
	// Templates --------------------------
	
	private void dashboardTemplate(String username, String testName, Class<?> expected) {
		Class<?> caught = null;
		
		try {
			// El usuario se registra
			authenticate(username);
			managerService.getManagersOrderedByNumberOfEvents();
			managerService.findAll();
			chorbiService.getChorbiesOrderedByNumberOfEvents();
			chorbiService.findAll();
			likesService.getMinNumberOfLikesPerChorbi();
			likesService.getMaxNumberOfLikesPerChorbi();
			likesService.getAvgNumberOfLikesPerChorbi();
			likesService.getChorbiesOrderedByAverageLikes();
			
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	
	
	// Tests ------------------------------
	
	// 1. Aplicación correcta a un Dashboard
	@Test
	public void testCaseCorrect() {
		dashboardTemplate("admin","ValidTest", null);
	}
	
	// 2. Usuario incorrecto
	@Test
	public void testCaseInvalidUser() {
		dashboardTemplate("chorbi1","InvalidTest", IllegalArgumentException.class);
	}

}
