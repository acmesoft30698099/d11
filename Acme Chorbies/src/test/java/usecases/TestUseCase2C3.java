package usecases;

import javax.transaction.Transactional;

import org.joda.time.format.DateTimeFormat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.EventService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Chirp;
import domain.CreditCard;
import domain.Event;
import domain.Manager;
import forms.EventForm;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
- Manage the events that he or she organises, which includes listing, registering, 
  modifying, and deleting them. In order to register a new event, he must have registered 
  a valid credit card that must  not expire in less than one day.  Every time he or she 
  registers an event, the system will simulate that he or she's charged a 1.00 euros fee.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C3 extends AbstractTest {
	
	// Constructor --------------------------------------

	public TestUseCase2C3() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template -----------------------------------------
	
	public void templateSaveEvent(EventForm eventForm, String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Comprobamos los permisos del usuario como si fuera el security.xml
			servicesUtils.getRegisteredUser(Manager.class);
			// Reconstruimos el evento
			Event event = eventService.reconstruct(eventForm, null);
			// Realizamos el guardado
			eventService.save(event);
			eventService.flush();
			// Si se trata de una actualizaci�n, avisamos a los usuarios que est�n registrados en el evento.
			Chirp template = new Chirp();
			template.setSubject(event.getTitle()+" changed");
			template.setText("The event "+event.getTitle()+" in which you are registered has changed, to see the changes, follow the link in the attachments.");
			servicesUtils.sendBulkChirpsForEvent(event, template);
			
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	public void templateDeleteEvent(Integer eventId, String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Obtenemos el evento
			Event event = eventService.findOneForEdit(eventId);
			// Ejecutamos la eliminaci�n
			eventService.delete(event);
			eventService.flush();
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Tests ---------------------------------------------
	
	// 1. y 2. Test correcto de creaci�n de eventos.
	
	@Test
	public void testCreateEventsCorrect() {
		EventForm eventForm1 = eventService.construct(eventService.create());
		eventForm1.setDate(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
				.parseDateTime("10/09/2018 21:00").toDate());
		eventForm1.setDescription("Description 1");
		eventForm1.setImage("http://www.acme.com/db/image1338.jpg");
		eventForm1.setNumberSeats(50);
		eventForm1.setTitle("Title 1");
		
		EventForm eventForm2 = eventService.construct(eventService.create());
		eventForm2.setDate(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
				.parseDateTime("10/09/2019 12:00").toDate());
		eventForm2.setDescription("Description 2");
		eventForm2.setImage("http://www.acme.com/db/image331.png");
		eventForm2.setNumberSeats(250);
		eventForm2.setTitle("Title 2");
		
		Object [] [] objects = {
				{ eventForm1, "manager1", null, "testCreateEvent1" },
				{ eventForm2, "manager3", null, "testCreateEvent2" }
		};
		
		for(Object[] row : objects) {
			templateSaveEvent((EventForm) row[0], (String) row[1], (Class<?>) row[2], (String) row[3]); 
		}
	}
	
	// 3. y 4. Test de creaci�n de eventos incorrectos.
	
	@Test
	public void testCreateEventsIncorrect() {
		EventForm eventForm1 = eventService.construct(eventService.create());
		eventForm1.setDate(DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")
				.parseDateTime("10/09/2018 21:00").toDate());
		eventForm1.setDescription("Description 1");
		eventForm1.setImage("http://www.acme.com/db/image1338.jpg");
		eventForm1.setNumberSeats(50);
		eventForm1.setTitle("Title 1");
		
		super.authenticate("manager3");
		Manager manager3 = servicesUtils.getRegisteredUser(Manager.class);
		CreditCard manager3CreditCard = manager3.getCreditCard();
		manager3CreditCard.setBrand("SOMETHING NOT RIGHT");
		manager3.setCreditCard(manager3CreditCard);
		super.unauthenticate();
		
		Object [] [] objects = {
				{ eventForm1, "chorbi1", IllegalArgumentException.class, "testCreateEventIncorrectUser" },
				{ eventForm1, "manager3", IllegalArgumentException.class, "testCreateEventIncorrectCreditCard" }
				
		};
		
		for(Object[] row : objects) {
			templateSaveEvent((EventForm) row[0], (String) row[1], (Class<?>) row[2], (String) row[3]); 
		}
	}
	
	// 5. Edici�n correcta de un evento.
	
	@Test
	public void testEditEventCorrect() {
		EventForm eventForm1 = eventService.construct(eventService.findOne(75));
		eventForm1.setTitle("Something diferent");
		
		Object [][] objects = {
				{ eventForm1, "manager2", null, "testEditCorrectEvent" }
		};
		
		for(Object[] row : objects) {
			templateSaveEvent((EventForm) row[0], (String) row[1], (Class<?>) row[2], (String) row[3]); 
		}
	}
	
	// 6. y 7. Edici�n incorrecta de un evento, usuario incorrecto.
	
	@Test
	public void testEditEventInCorrect() {
		EventForm eventForm1 = eventService.construct(eventService.findOne(75));
		eventForm1.setTitle("Something diferent");
		
		EventForm eventForm2 = eventService.construct(eventService.findOne(77));
		eventForm2.setNumberSeats(2);
		
		Object [][] objects = {
				{ eventForm1, "manager3", IllegalArgumentException.class, "testEditIncorrectEvent1" },
				// Lanzar� NullPointerException ya interactua con el BindingResult para introducir el error del campo.
				{ eventForm2, "manager3", NullPointerException.class, "testEditIncorrectEvent2" }
		};
		
		for(Object[] row : objects) {
			templateSaveEvent((EventForm) row[0], (String) row[1], (Class<?>) row[2], (String) row[3]); 
		}
	}
	
	// 8. Eliminaci�n correcta de eventos 
	
	@Test
	public void testDeleteEventCorrect() {
		templateDeleteEvent(80, "manager3", null, "testDeleteCorrectEvent1");
	}
	
	// 9. y 10. Eliminaci�n incorrecta de eventos 
	
	@Test
	public void testDeleteEventInCorrect() {
		templateDeleteEvent(80, "chorbi3", IllegalArgumentException.class, "testDeleteInCorrectEvent1");
		templateDeleteEvent(80, "manager2", IllegalArgumentException.class, "testDeleteInCorrectEvent2");
	}	
}
