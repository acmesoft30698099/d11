package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.Event;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Browse the list of events to which he or she's registered.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C6 extends AbstractTest {

	// Constructor --------------------------------------

	public TestUseCase2C6() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template -----------------------------------------
	
	public void templateListRegistered(String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Obtenemos el usuario
			Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
			// Obtenemos la lista de eventos registrados
			Collection<Event> events = chorbi.getEvents();
			for(Event event : events) {
				Assert.isTrue(event.getChorbies().contains(chorbi));
			}
			
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Tests --------------------------------------------
	
	// 1. Test correcto de listado de eventos registrados.
	
	@Test
	public void testListRegistreredCorrect() {
		templateListRegistered("chorbi3", null, "testListRegisteredCorrect");
	}
	
	// 2. Test incorrecto de listado de eventos registrados, usuario incorrecto

	@Test
	public void testListRegisteredIncorrect() {
		templateListRegistered(null, IllegalArgumentException.class, "testListRegisteredCorrect");
	}
}
