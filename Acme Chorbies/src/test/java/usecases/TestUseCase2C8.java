package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Administrator;
import domain.Chorbi;

import services.ChorbiService;
import services.ConfigurationService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Run a process to update the total monthly fees that the chorbies would have to pay.
   Recall that chorbies must not be aware of the simulation.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C8 extends AbstractTest {
	
	// Constructor --------------------------------------

	public TestUseCase2C8() {
		super();
	}
	
	// Service ------------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private ChorbiService chorbiService;
	
	// Template -----------------------------------------
	
	public void templateListChorbiesWithFee(String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Comprobamos los permisos
			servicesUtils.getRegisteredUser(Administrator.class);
			// Solicitamos el pago mensual total
			Double monthlyTotalFee = configurationService.getMensualIncomeFromChorbisFee();
			// Comprobamos que el pago es correcto
			Double sum = 0d;
			for(Chorbi chorbi : chorbiService.findAll()) {
				sum += chorbi.getFee();
			}
			Assert.isTrue(monthlyTotalFee.equals(sum));
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Test ------------------------------------------
	
	// 1. Test correcto ------------------------------
	
	@Test
	public void testListFeeCorrect() {
		templateListChorbiesWithFee("admin", null, "testListFeeCorrect");
	}

	// 2. Test correcto ------------------------------
	
	@Test
	public void testListFeeIncorrect() {
		templateListChorbiesWithFee("chorbi2", IllegalArgumentException.class, "testListFeeIncorrect");
	}
}
