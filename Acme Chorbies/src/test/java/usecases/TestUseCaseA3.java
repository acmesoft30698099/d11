package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import services.ChirpService;
import utilities.AbstractTest;
import domain.Chirp;

/**
 * Caso de prueba del requisito funcional siguiente:
 o Browse the list of chirps that he or she's sent, and re-send any of them
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseA3 extends AbstractTest {

	// Constructor -------------------

	public TestUseCaseA3() {
		super();
	}

	// Services ----------------------------

	@Autowired
	private ChirpService chirpService;

	// Templates ---------------------------

	private void chirpForwardTemplate(String username, String title, Integer chirpId, Integer actorId, Class<?> expected) {
		Class<?> caught = null;

		try {
			authenticate(username);
			// El usuario accede a la lista de chirp y le da click a Reenviar.
			// Le saldr� un formulario para editar el chirp a reenviar, 
			// Le da click a guardar, lo que ejecuta el m�todo reconstruct
			Chirp chirp = chirpService.reconstructForward(chirpId,actorId);
			// Ejecuta el guardado
			chirpService.save(chirp);
			unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, title);
	}

	// Tests -------------------------------

	// 1. Reenvio correcto de chirp.
	@Test
	public void testCaseSaveCorrect() {
		chirpForwardTemplate("chorbi1", "validTest", 59, 30, null);
	}

	// 2. Creaci�n incorrecta de chirp, 
	@Test
	public void testCaseSaveIncorrect() {
		chirpForwardTemplate("chorbi4", "invalidTest", 59, 30, IllegalArgumentException.class);
	}

}
