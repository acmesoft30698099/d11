
package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ConfigurationService;
import utilities.AbstractTest;
import domain.Configuration;
import forms.ConfigurationForm;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
/**
 * Casos de prueba para el siguiente requisito funcional
 * 
 * - Browse the results of his or her search template as long as he or she's registered
 a valid credit card. Note that the validity of the credit card must be checked every
 time the results of the search template are displayed. The results of search templates
 must be cached for at least 12 hours.
 * @author Student
 *
 */
public class TestUseCaseC7ForD11 extends AbstractTest {

	// Constructor ---------------------------

	public TestUseCaseC7ForD11() {
		super();
	}


	// Services ------------------------------

	@Autowired
	private ConfigurationService	configurationService;


	// Tests

	@Test
	public void editChorbiFee() {
		template("admin", 0.5, configurationService.findConfiguration().getManagerFee(), null, "EditChorbiFee");
	}

	@Test
	public void editChorbiFeeNegative() {
		template("admin", -0.5, configurationService.findConfiguration().getManagerFee(), IllegalArgumentException.class, "EditChorbiFeeNegative");
	}

	@Test
	public void editManagerFee() {
		template("admin", configurationService.findConfiguration().getChorbiFee(), 0.5, null, "EditManagerFee");
	}

	@Test
	public void editManagerFeeWithChorbi() {
		template("chorbi1", configurationService.findConfiguration().getChorbiFee(), 4.65, IllegalArgumentException.class, "EditManagerFeeWithChorbi");
	}

	// Templates -----------------------------

	private void template(String username, double chorbiFee, double managerFee, Class<?> expected, String testName) {
		Class<?> caught = null;
		super.authenticate(username);
		try {
			ConfigurationForm form = configurationService.construct(configurationService.findConfiguration());
			form.setChorbiFee(chorbiFee);
			form.setManagerFee(managerFee);
			Configuration conf = configurationService.reconstruct(form, null);
			configurationService.save(conf);
			Assert.isTrue(conf.getChorbiFee() >= 0.);
			Assert.isTrue(conf.getManagerFee() >= 0.);
			Assert.isTrue(form.getChorbiFee() == chorbiFee);
			Assert.isTrue(conf.getManagerFee() == managerFee);
		} catch (Throwable t) {
			caught = t.getClass();
		}
		super.authenticate(null);
		checkExceptions(expected, caught, testName);
	}

}
