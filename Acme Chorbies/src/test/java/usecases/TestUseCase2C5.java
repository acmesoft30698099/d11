package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Chorbi;
import services.EventService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Un-register from an event to which he or she's registered.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2C5 extends AbstractTest {

	// Constructor --------------------------------------

	public TestUseCase2C5() {
		super();
	}

	// Services -----------------------------------------

	@Autowired
	private EventService eventService;

	@Autowired
	private ServicesUtils servicesUtils;

	// Template ------------------------------------------

	public void templateUnregisterEvent(Integer eventId, String username,
			Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Comprobamos los permisos
			Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
			// Ejecutamos el registro
			eventService.unregisterToEvent(chorbi.getId(), eventId);
			eventService.flush();
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	// Tests ----------------------------------------------

	// 1. Test correcto de deregistro de evento

	@Test
	public void testUnregisterEventCorrect() {
		templateUnregisterEvent(77, "chorbi3", null, "testUnregisterEventCorrect");
	}

	// 2. Test incorrecto de deregistro de evento, evento no registrado

	@Test
	public void testUnregisterEventIncorrect() {
		templateUnregisterEvent(80, "chorbi3", IllegalArgumentException.class, "testUnregisterEventCorrect");
	}

}
