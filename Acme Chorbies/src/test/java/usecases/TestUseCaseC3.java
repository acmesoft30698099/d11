package usecases;

import javax.transaction.Transactional;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.joda.DateTimeFormatterFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ChorbiService;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.Coordinate;
import forms.ChorbiForm;
import forms.UserAccountForm;

/**
 * Test del caso de uso siguiente: Register to the system as a chorbi. As of the
 * time of registering, a chorbi is not required to provide a credit card. No
 * person under 18 is allowed to register to the system.
 * 
 * @author Student
 * 
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseC3 extends AbstractTest {
	 
	// Constructor ------------------------------------------
	
	public TestUseCaseC3() {
		super();
	}
	
	// Services ---------------------------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	// Template ---------------------------------------------
	
	public void createChorbiTemplate(ChorbiForm form,Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.unauthenticate();
			// Reconstruimos el Chorbi
			Chorbi chorbi = chorbiService.reconstruct(form, null);
			// Realizamos el guardado.
			chorbi = chorbiService.save(chorbi);
			chorbiService.flush();
			// Comprobamos que el objeto queda guardado
			chorbi = chorbiService.findOne(chorbi.getId());
			Assert.notNull(chorbi);
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ------------------------------------------------
	
	// 1. Tests correctos --------------------------------------
	@Test
	public void testCorrectCreateChorbi() {
		ChorbiForm chorbi1 = chorbiService.construct(chorbiService.create());
		LocalDate localDate = LocalDate.parse("21/06/1978", new DateTimeFormatterFactory("dd/MM/yyyy").createDateTimeFormatter() );
		chorbi1.setBirthDate(localDate.toDate());
		chorbi1.setDescription("Description");
		chorbi1.setEmail("chorbi100@acme.com");
		chorbi1.setGender("man");
		chorbi1.setImage("http://www.acme.com/");
		chorbi1.setName("chorb100");
		chorbi1.setSurname("chorbi100");
		chorbi1.setRelation("activities");
		chorbi1.setPhone("+33 23948195");
		Coordinate coord = new Coordinate();
		coord.setCity("City");
		chorbi1.setCoordinate(coord);
		UserAccountForm userAccount1Form = new UserAccountForm();
		userAccount1Form.setPassword("chorbi100");
		userAccount1Form.setUsername("chorbi100");
		userAccount1Form.setConfirmPassword("chorbi100");
		chorbi1.setUserAccount(userAccount1Form);
		
		ChorbiForm chorbi2 = chorbiService.construct(chorbiService.create());
		localDate = LocalDate.parse("31/12/1995", new DateTimeFormatterFactory("dd/MM/yyyy").createDateTimeFormatter() );
		chorbi2.setBirthDate(localDate.toDate());
		chorbi2.setDescription("Description 2");
		chorbi2.setEmail("chorbi101@acme.com");
		chorbi2.setGender("woman");
		chorbi2.setImage("http://www.acme.com/");
		chorbi2.setName("chorb101");
		chorbi2.setSurname("chorbi101");
		chorbi2.setRelation("friendship");
		chorbi2.setPhone("83828159");
		coord = new Coordinate();
		coord.setCity("City2");
		coord.setState("State 2");
		chorbi2.setCoordinate(coord);
		UserAccountForm userAccount2Form = new UserAccountForm();
		userAccount2Form.setPassword("chorbi101");
		userAccount2Form.setUsername("chorbi101");
		userAccount2Form.setConfirmPassword("chorbi101");
		chorbi2.setUserAccount(userAccount2Form);
		
		Object[][] objects = { 
				{chorbi1,null,"testChorbi1"},
				{chorbi2,null,"testChorbi2"},
		};
		
		for(Object[] objs : objects) {
			createChorbiTemplate((ChorbiForm) objs[0], (Class<?>) objs[1], (String) objs[2]);
		}
	}
	
	// 2. Tests incorrectos, edad > 18.
	@Test
	public void testIncorrectCreateChorbi() {
		ChorbiForm chorbi1 = chorbiService.construct(chorbiService.create());
		LocalDate localDate = LocalDate.parse("21/06/1999", new DateTimeFormatterFactory("dd/MM/yyyy").createDateTimeFormatter() );
		chorbi1.setBirthDate(localDate.toDate());
		chorbi1.setDescription("Description");
		chorbi1.setEmail("chorbi100@acme.com");
		chorbi1.setGender("man");
		chorbi1.setImage("http://www.acme.com/");
		chorbi1.setName("chorb100");
		chorbi1.setSurname("chorbi100");
		chorbi1.setRelation("activities");
		chorbi1.setPhone("+33 23948195");
		Coordinate coord = new Coordinate();
		coord.setCity("City");
		chorbi1.setCoordinate(coord);
		UserAccountForm userAccount1Form = new UserAccountForm();
		userAccount1Form.setPassword("chorbi100");
		userAccount1Form.setUsername("chorbi100");
		userAccount1Form.setConfirmPassword("chorbi100");
		chorbi1.setUserAccount(userAccount1Form);
		
		//Se lanza Null pointer exception porque se debe a una restricci�n de dominio y como el binding est� a null, salta esta excepci�n
		Object[][] objects = { 
				{chorbi1,NullPointerException.class,"testChorbiIncorrectAge"}
		};
		
		for(Object[] objs : objects) {
			createChorbiTemplate((ChorbiForm) objs[0], (Class<?>) objs[1], (String) objs[2]);
		}
	}
}
