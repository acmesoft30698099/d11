package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ChorbiService;
import utilities.AbstractTest;
import domain.Chorbi;
import forms.ChorbiForm;

/**
 * Test del caso de uso siguiente: Change his or her profile.
 * 
 * @author Student
 * 
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseC5 extends AbstractTest {

	// Constructor ------------------------------------------

	public TestUseCaseC5() {
		super();
	}

	// Services ---------------------------------------------

	@Autowired
	private ChorbiService chorbiService;

	// Template ---------------------------------------------

	public void editChorbiTemplate(String username, ChorbiForm form, Class<?> expected,
			String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Reconstruimos el Chorbi
			Chorbi chorbi = chorbiService.reconstruct(form, null);
			// Realizamos el guardado.
			chorbi = chorbiService.save(chorbi);
			chorbiService.flush();
			// Comprobamos que el objeto queda guardado
			chorbi = chorbiService.findOne(chorbi.getId());
			Assert.notNull(chorbi);
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}

	// Tests ------------------------------------------------

	// 1. Tests correctos --------------------------------------
	@Test
	public void testCorrectSaveChorbi() {
		Chorbi chorbi = chorbiService.findOne(16);
		ChorbiForm chorbi1 = chorbiService.construct(chorbi);
		chorbi1.setDescription("Something else to test the editing of a profile");
		
		Object[][] objects = { { "chorbi1", chorbi1, null, "testChorbi1" } };

		for (Object[] objs : objects) {
			editChorbiTemplate((String) objs[0], (ChorbiForm) objs[1], (Class<?>) objs[2],
					(String) objs[3]);
		}
	}

	// 2. Tests incorrectos, usuario incorrecto
	@Test
	public void testIncorrectCreateChorbi() {
		Chorbi chorbi = chorbiService.findOne(16);
		ChorbiForm chorbi1 = chorbiService.construct(chorbi);
		chorbi1.setDescription("Something else to test the editing of a profile");
		
		Object[][] objects = { { "chorbi3", chorbi1, IllegalArgumentException.class, "testIncorrectChorbi1" } };

		for (Object[] objs : objects) {
			editChorbiTemplate((String) objs[0], (ChorbiForm) objs[1], (Class<?>) objs[2],
					(String) objs[3]);
		}
	}

}
