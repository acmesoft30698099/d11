package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Chorbi;
import domain.Likes;

import forms.LikesForm;

import services.ChorbiService;
import services.LikesService;
import utilities.AbstractTest;

/**
 * Test del caso de uso siguiente: Like another chorbi; a like may be cancelled
 * at any time.
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseB2 extends AbstractTest {
	
	// Constructor ----------------------------------
	
	public TestUseCaseB2() {
		super();
	}
	
	// Services -------------------------------------
	
	@Autowired
	private LikesService likesService;
	
	@Autowired
	private ChorbiService chorbiService;
	
	// Template -------------------------------------
	
	private void addLikeTemplate(String username, LikesForm likesForm, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			// Obtenemos el objeto reconstruido
			Likes likes = likesService.reconstruct(likesForm, null);
			// Guardamos el like
			likes = likesService.save(likes);
			likesService.flush();
			// Comprobamos que est� guardado
			Assert.notNull(likes);
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	private void removeLikeTemplate(String username, Integer likesId, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			Likes likes = likesService.findOne(likesId);
			// Guardamos el like
			likesService.delete(likes);
			likesService.flush();
			// Comprobamos que est� eliminado
			likes = likesService.findOne(likesId);
			Assert.notNull(likes);
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -------------------------------------------------
	
	// 1. Test positivo --------------------------------------
	
	@Test
	public void testSaveLikes() {
		LikesForm likesForm = likesService.construct(likesService.create());
		likesForm.setComments("");
		Chorbi chorbi = chorbiService.findOne(19);
		likesForm.setReceiver(chorbi);
		
		addLikeTemplate("chorbi1", likesForm, null, "addLikesCorrect1");
	}
	
	// 2. Test negativo, eliminaci�n con usuario no poseedor del like --------
	
	@Test
	public void testRemoveLikes() {
		removeLikeTemplate("chorbi2", 48, IllegalArgumentException.class, "removeLikesIncorrect1");
	}
}
