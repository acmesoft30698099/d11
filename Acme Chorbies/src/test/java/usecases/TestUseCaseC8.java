package usecases;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Administrator;
import domain.Chorbi;

import security.Authority;
import security.UserAccount;
import security.UserAccountService;
import services.ChorbiService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Test del caso de uso siguiente: Ban a chorbi, that is, to disable his or her account.
 * 
 * @author Student
 * 
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseC8 extends AbstractTest {
	
	// Constructor -----------------------------------------
	
	public TestUseCaseC8() {
		super();
	}
	
	// Services --------------------------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private UserAccountService userAccountService;
	
	// Template --------------------------------------------
	
	public void banChorbiTemplate(String username, Integer chorbiId, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			// Comprobamos los permisos del usuario
			servicesUtils.getRegisteredUser(Administrator.class);
			// Obtenemos el chorbi
			Chorbi chorbi = chorbiService.findOne(chorbiId);
			// Obtenemos la UserAccount del usuario
			UserAccount user = chorbi.getUserAccount();
			// Definimos el Authority de baneado
			Authority auth = new Authority();
			auth.setAuthority(Authority.BANNED_CHORBI);
			// Cambiamos la Authority
			Collection<Authority> auths = new ArrayList<Authority>();
			auths.add(auth);
			user.setAuthorities(auths);
			// Guardamos la UserAccount
			userAccountService.changeAuthorities(user);
			chorbiService.flush();
			chorbi = chorbiService.findOne(chorbiId);
			Assert.isTrue(chorbi.getUserAccount().getAuthorities().contains(auth));
			super.unauthenticate();
		} catch(Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -----------------------------------------------
	
	// 1. Test positivo ------------------------------------
	
	@Test
	public void testCorrectBanChorbi() {
		banChorbiTemplate("admin", 19, null, "testBanChorbi1");
	}
	
	// 2. Test negativo, usuario incorrecto ---------------
	
	@Test
	public void testIncorrectBanChorbi() {
		banChorbiTemplate(null, 19, IllegalArgumentException.class, "testBanChorbi1");
	}

}
