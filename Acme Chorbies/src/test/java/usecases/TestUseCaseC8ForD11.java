
package usecases;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ConfigurationService;
import utilities.AbstractTest;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
/**
 * Casos de prueba para el siguiente requisito funcional
 * 
 * - 
 * @author Student
 *
 */
public class TestUseCaseC8ForD11 extends AbstractTest {

	// Constructor ---------------------------

	public TestUseCaseC8ForD11() {
		super();
	}


	// Services ------------------------------

	@Autowired
	private ConfigurationService	configurationService;


	// Tests

	@Test
	public void showIncome() {
		template("admin", null, "ShowIncome");
	}

	@Test
	public void showIncomeAsChorbi() {
		template("chorbi1", IllegalArgumentException.class, "ShowIncomeAsChorbi");
	}

	// Template

	private void template(String username, Class<?> expected, String testName) {
		Class<?> caught = null;
		super.authenticate(username);
		try {
			double income = configurationService.getMensualIncomeFromChorbisFee();
			Assert.isTrue(income >= 0.);
		} catch (Throwable t) {
			caught = t.getClass();
		}
		super.authenticate(null);
		super.checkExceptions(expected, caught, testName);
	}
}
