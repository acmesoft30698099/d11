package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ChorbiService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;
import domain.Chorbi;
import domain.Likes;

/**
 * Test del caso de uso siguiente: Browse the list of chorbies who have
 * registered to the system and navigate to the chorbies who like them
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseB1 extends AbstractTest {
	
	// Constructor -------------------------------------
	
	public TestUseCaseB1() {
		super();
	}
	
	// Services ----------------------------------------
	
	@Autowired
	private ChorbiService chorbiService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Template ----------------------------------------
	
	private void findChorbiesLiked(String username, Integer chorbiId, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			// Comprobamos las authorities del usuario
			servicesUtils.checkUser();
			// Obtenemos el chorbi
			Chorbi chorbi = chorbiService.findOne(chorbiId);
			// Encontramos los likes que han hecho like
			Collection<Likes> likes = chorbi.getReceivedLikes();
			Assert.notNull(likes);
			// De esa forma se podr� obtener los chorbies que hicieron like
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests -------------------------------------------
	
	// 1. Test positivo --------------------------------
	
	@Test
	public void testFindChorbiesCorrect() {
		findChorbiesLiked("chorbi2", 16, null, "findLikesChorbi1");
		findChorbiesLiked("chorbi2", 21, null, "findLikesChorbi1");
	}
	
	// 2. Test negativo, usuario no v�lido -------------
	
	@Test
	public void testFindChorbiesInvalid() {
		findChorbiesLiked(null, 16, IllegalArgumentException.class, "findLikesChorbi1");
	}
}
