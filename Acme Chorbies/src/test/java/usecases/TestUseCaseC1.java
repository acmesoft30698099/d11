package usecases;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import services.ConfigurationService;
import utilities.AbstractTest;
import domain.Configuration;
import domain.Url;

/**
 * Test del caso de uso siguiente: See  a welcome  page  with  a  banner  that  advertises  Acme  projects,
 * including  Acme Pad Thai, Acme BnB, and Acme Car'n go! The banners must be selected randomly
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCaseC1 extends AbstractTest {
	
	// Constructor ------------------------------------
	
	public TestUseCaseC1() {
		super();
	}
	
	// Services ---------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	// Template ---------------------------------------
	
	private void selectBannerTemplate(String username, Class<?> expected, String testName) {
		Class<?> caught = null;
		
		try {
			super.authenticate(username);
			// Obtenemos los banners
			Configuration configuration = configurationService.findConfiguration();
			Collection<Url> banners = configuration.getBanners();
			// Ejecutamos el m�todo de selecci�n aleatoria
			Url bannerRand = configurationService.getRandomBanner();
			// Comprobamos que sea uno de los banners del sistema
			Assert.isTrue(banners.contains(bannerRand));
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}
		
		checkExceptions(expected, caught, testName);
	}
	
	// Tests ----------------------------------------------
	
	// 1. Test positivo -----------------------------------
	
	@Test
	public void testSelectBannerCorrect() {
		selectBannerTemplate(null, null, "selectBanner1");
		selectBannerTemplate("chorbi1", null, "selectBanner2");
		selectBannerTemplate("admin", null, "selectBanner3");
	}

}
