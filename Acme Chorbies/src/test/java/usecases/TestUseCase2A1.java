package usecases;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import domain.Chirp;
import domain.Event;
import domain.Manager;
import domain.Url;

import forms.ChirpFormEvent;

import services.ChirpService;
import services.EventService;
import services.utils.ServicesUtils;
import utilities.AbstractTest;

/**
 * Caso de prueba para el requisito funcional siguiente:
 * 
 o Broadcast a chirp to the chorbies who have registered to any of the events that
   he or she manages.
 * @author Student
 *
 */
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TestUseCase2A1 extends AbstractTest {
	
	// Constructor --------------------------------------

	public TestUseCase2A1() {
		super();
	}
	
	// Services -----------------------------------------
	
	@Autowired
	private ChirpService chirpService;
	
	@Autowired
	private EventService eventService;
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// Tempate ------------------------------------------

	public void templateSaveBulk(ChirpFormEvent chirpForm, String username, Class<?> expected, String testName) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			// Verificamos el usuario
			servicesUtils.getRegisteredUser(Manager.class);
			// Reconstruimos del objeto form
			Chirp template = chirpService.reconstructBulk(chirpForm);
			// Obtenemos los permisos del evento
			Event event = eventService.findOneForEdit(chirpForm.getEventId());
			// Enviamos el mensaje en bulk
			servicesUtils.sendBulkChirpsForEvent(event, template);
			chirpService.flush();
			super.unauthenticate();
		} catch (Throwable excp) {
			caught = excp.getClass();
		}

		checkExceptions(expected, caught, testName);
	}
	
	// Tests --------------------------------------------
	
	// 1. Test correcto ---------------------------------
	
	@Test
	public void testSendBulkCorrect() {
		ChirpFormEvent chirpForm = new ChirpFormEvent();
		chirpForm.setAttachments(new ArrayList<Url>());
		chirpForm.setEventId(76);
		chirpForm.setSubject("Something");
		chirpForm.setText("Text");
		
		templateSaveBulk(chirpForm, "manager3", null, "testSendBulk");
	}
	
	// 2. Test incorrect, usuario sin permisos ---------
	
	@Test
	public void testSendBulkIncorrect() {
		ChirpFormEvent chirpForm = new ChirpFormEvent();
		chirpForm.setAttachments(new ArrayList<Url>());
		chirpForm.setEventId(76);
		chirpForm.setSubject("Something");
		chirpForm.setText("Text");
		
		templateSaveBulk(chirpForm, "manager2", IllegalArgumentException.class, "testSendBulk");
	}
}
